﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EmployeeUpdates.aspx.vb" Inherits="CenterVisitEvaluation.EmployeeUpdates" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/jquery-ui.js" type="text/javascript"></script>
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
    </script>
    <script type="text/javascript">
        function openEditModal() {
            $('#modEditModal').modal({ backdrop: 'static', keyboard: false })
        }
    </script>
    <style type="text/css">
     .hidden
     {
         display:none;
     }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" >
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    EMPLOYEES
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="form-inline">

                                <div class="form-group" style="margin:10px">
                                    <label class="control-label">Search: </label>
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary" Font-Size="16px"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                </div> 
                                
                                <div class="form-group" style="margin:10px">
                                    
                                    <asp:Button ID="btnNew" Text ="Add New Employee" runat="server" CssClass="btn btn-success" Font-Size="14px"></asp:Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">


                            <asp:GridView ID="grdEmployee" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                                GridLines="None" CssClass="gridview" Width="100%" PageSize="25">
                                <Columns>
                                    
                                    <asp:TemplateField HeaderText="ID Number" SortExpression="emp_code">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmp1" runat="server" CausesValidation="False"
                                                CommandArgument="<%#Bind('emp_code') %>" CommandName="Employee"
                                                Text="<%#Bind('emp_code') %>">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="last_name" SortExpression="last_name" HeaderText="Last Name"  />
                                    <asp:BoundField DataField="first_name" SortExpression="first_name" HeaderText="First Name" />
                                    <asp:BoundField DataField="middle_name" SortExpression="middle_name" HeaderText="Middle Name" />
                                    <asp:BoundField DataField="position_code" SortExpression="position_code" HeaderText="Position" />
                                    <asp:BoundField DataField="unit_code" SortExpression="unit_code" HeaderText="Unit" />
                                    <asp:BoundField DataField="branch_desc" SortExpression="branch_desc" HeaderText="Branch" />
                                    <asp:BoundField DataField="area_desc" SortExpression="area_desc" HeaderText="Area" />
                                    <asp:BoundField DataField="region_desc" SortExpression="region_desc" HeaderText="Region" />
                                    <asp:BoundField DataField="region_code" SortExpression="region_code" HeaderText="Region Code" HeaderStyle-CssClass ="hidden" ItemStyle-CssClass="hidden"/>
                                    <asp:BoundField DataField="area_code" SortExpression="area_code" HeaderText="Area Code" HeaderStyle-CssClass ="hidden" ItemStyle-CssClass="hidden"/>
                                    <asp:BoundField DataField="branch_code" SortExpression="branch_code" HeaderText="Branch Code" HeaderStyle-CssClass ="hidden" ItemStyle-CssClass="hidden"/>
                                    <asp:BoundField DataField="users_name" SortExpression="users_name" HeaderText="User Name" />
                                </Columns>
                                <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />
                            </asp:GridView>


                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>




    <div class="modal fade" tabindex="-1" id="modMessageBox">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="modEditModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">
                        </h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateEmployee" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            
                            <div class="container-fluid">
                                <div class="row">
                                    
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    <asp:Label ID="lblTitleUpdate" runat="server" ></asp:Label> - <asp:Label ID="lblName" runat="server" ></asp:Label>
                                </div>
                            </div>
                        </div>
                    </nav>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">ID Number</div>
                                                        <div class="col-sm-9">

                                                            
                                                            <asp:TextBox ID="txtEmpCode" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                 </div> 
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">Last Name</div>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                 </div> 
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">First Name</div>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                 </div> 
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">Middle Name</div>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                 </div> 
                                                <div class="form-group">
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-3">Branch</div>
                                                        <div class="col-sm-9">

                                                            <asp:DropDownList ID="cboToBranch" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtToBranchCode" runat="server" Visible ="false" ></asp:TextBox>
                                                            <asp:TextBox ID="txtToRegCode" runat="server" Visible ="false" ></asp:TextBox>
                                                            <asp:TextBox ID="txtToAreaCode" runat="server" Visible ="false" ></asp:TextBox>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">Position</div>
                                                        <div class="col-sm-9">
                                                            <asp:DropDownList ID="cboPosition" runat="server" CssClass="form-control" AutoPostBack="True" required>

                                                                <asp:ListItem>Select Position</asp:ListItem>
                                                                <asp:ListItem>RD</asp:ListItem>
                                                                <asp:ListItem>AM</asp:ListItem>
                                                                <asp:ListItem>BM</asp:ListItem>
                                                                <asp:ListItem>BA</asp:ListItem>
                                                                <asp:ListItem>BAA</asp:ListItem>
                                                                <asp:ListItem>PUH</asp:ListItem>
                                                                <asp:ListItem>PO</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtPosCode" runat="server" Visible="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3">Unit</div>
                                                        <div class="col-sm-9">
                                                            <asp:DropDownList ID="cboUnit" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                                                <asp:ListItem>Select Unit</asp:ListItem>
                                                                <asp:ListItem>A</asp:ListItem>
                                                                <asp:ListItem>B</asp:ListItem>
                                                                <asp:ListItem>C</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <div class="btn-group" role="group">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CssClass="btn btn-success" />

                                            <asp:ConfirmButtonExtender ID="confirmExtender" runat="server" ConfirmText="Are you sure you want to submit?" TargetControlID="cmdSave">
                                            </asp:ConfirmButtonExtender>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!--SESSIONS-->
    <div>
        <asp:Label ID="lblfullname" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblposition" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchcode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchdesc" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblsearch" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Visible="False"></asp:Label>
    </div>

</asp:Content>
