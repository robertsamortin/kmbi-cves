﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Centers.aspx.vb" Inherits="CenterVisitEvaluation.Centers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    CENTER LIST
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group" style="margin:10px">
                                    <label for="drpBranch">Branch:</label>
                                    <asp:DropDownList ID="drpBranch" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
	                            <div class="form-group" style="margin:10px">
                	                <label for="lnkSearch">Center Name:</label>
                                    <asp:TextBox ID="txtCenter" runat="server" CssClass="form-control" placeholder="ALL"></asp:TextBox>   
                                    <asp:LinkButton ID="lnkSearch" runat="server" CssClass="btn btn-primary" Font-Size="16px"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>                                                                                       	          
       	                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="text-danger" style="font-weight:bold; font-size:large">                           
                            <asp:Label ID="lblNoRecords" runat="server"></asp:Label>
                        </div>
                        <asp:GridView ID="grdCenterList" runat="server" AllowPaging="True" 
                        AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                        GridLines="None" CssClass="gridview" Width="75%" PageSize="25">
                        <Columns>                           
                            <asp:TemplateField HeaderText="Center" SortExpression="center_code">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCenter" runat="server" CausesValidation="False"
                                            CommandArgument="<%#Bind('center_code') %>" CommandName="Center" 
                                            Text="<%#Bind('center_code') %>" >
                                    </asp:LinkButton>
                                </ItemTemplate>                    
                            </asp:TemplateField>
                            <asp:BoundField DataField="branch_name" SortExpression="branch_name" HeaderText="Branch" />
                        </Columns> 
                        <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade"  tabindex="-1" id="modMessageBox" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<!--SESSIONS-->
    <div> 
        <asp:Label ID="lblfullname" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblposition" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchcode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchdesc" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblsearch" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Visible="False"></asp:Label>
    </div>

</asp:Content>