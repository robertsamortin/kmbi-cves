﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class Evaluation
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Dim cryRpt As New ReportDocument

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                Me.txtdate.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
                BIND_Branch()
                GEt_score()
                RetrieveProvince()
            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub


    Private Sub BIND_Branch()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "OPS_list_branch '" & branchcode & "', '" & areacode & "', '" & accesslevel & "','" & pa & "', '" & pus & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.cbobranch.Items.Clear()
            While dr.Read
                Me.cbobranch.Items.Add(Trim(dr("branch_desc").ToString))
            End While
            BIND_Center()

            upPnlEval.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub


    Private Sub BIND_Center()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchdesc As String = Trim(Me.cbobranch.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "OPS_list_center '" & branchdesc & "', '" & areacode & "', '" & accesslevel & "','" & pa & "', '" & pus & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.cboCenter.Items.Clear()
            While dr.Read
                Me.cboCenter.Items.Add(Trim(dr("center_desc").ToString))
            End While
            Get_Center_Address()
            upPnlEval.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub


    Private Sub Get_Center_Address()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim centerdesc As String = Trim(Me.cboCenter.Text)
            Dim branchdesc As String = Trim(Me.cbobranch.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)


            Dim sqlQuery As String = "OPS_get_center_address '" & centerdesc & "', '" & branchdesc & "', '" & areacode & "', '" & accesslevel & "','" & pa & "', '" & pus & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            If dr.Read Then
                Me.txtaddress.Text = Trim(dr("ctr_address").ToString)
            End If

            If accesslevel = 5 Then
                Me.lnkAddress.Visible = True
            Else
                Me.lnkAddress.Visible = False
            End If

            txtProgramOfficer.Text = "PO" + Left(Right(cboCenter.Text, 3), 1)
            'Get_PA()
            upPnlEval.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub


    'Private Sub Get_PA()
    '    Try
    '        If con.State = 1 Then con.Close()
    '        con.Open()

    '        Dim branchdesc As String = Trim(Me.cbobranch.Text)
    '        Dim areacode As String = Trim(Me.lblareacode.Text)
    '        Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
    '        Dim pa As String = Trim(Me.lblcurrentuser.Text)
    '        Dim pus As String = Trim(Me.lblcurrentuser.Text)


    '        Dim sqlQuery As String = "OPS_get_pa '" & branchdesc & "', '" & areacode & "', '" & accesslevel & "','" & pa & "', '" & pus & "'"
    '        Dim cmd As New SqlCommand(sqlQuery, con)
    '        Dim dr As SqlDataReader
    '        dr = cmd.ExecuteReader()

    '        Me.cboProgramOfficer.Items.Clear()
    '        While dr.Read
    '            Me.cboProgramOfficer.Items.Add(Trim(dr("pa").ToString))
    '        End While
    '        Get_RecordData()
    '        upPnlEval.Update()
    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    Finally
    '        con.Close()
    '    End Try
    'End Sub


    Private Sub cbobranch_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbobranch.SelectedIndexChanged
        BIND_Center()
        ' Get_RecordData()
    End Sub


    Private Sub cboCenter_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboCenter.SelectedIndexChanged
        Get_Center_Address()
        'Get_PA()
        'Get_RecordData()
    End Sub

    Private Sub GEt_score()
        Dim totalscore As Integer
        totalscore = Val(Me.txtpo1.Text) + Val(Me.txtpo2.Text) + Val(Me.txtpo3.Text) _
            + Val(Me.txtc1.Text) + Val(Me.txtc2.Text) + Val(Me.txtc3.Text) + Val(Me.txtc4.Text) + Val(Me.txtc5.Text) + Val(Me.txtc6.Text) _
            + Val(Me.txtatt1.Text) + Val(Me.txtatt2.Text) _
            + Val(Me.txtcm1.Text) + Val(Me.txtcm2.Text) + Val(Me.txtcm3.Text) + Val(Me.txtcm4.Text) + Val(Me.txtcm5.Text) + Val(Me.txtcm6.Text) + Val(Me.txtcm7.Text) + Val(Me.txtcm8.Text) + Val(Me.txtcm9.Text) + Val(Me.txtcm10.Text) + Val(Me.txtcm11.Text) _
            + Val(Me.txtpay1.Text) + Val(Me.txtpay2.Text) + Val(Me.txtpay3.Text) + Val(Me.txtpay4.Text)

        Me.lblTotalScore.Text = totalscore

        Dim rating As Double
        Dim equivalent As String

        rating = FormatNumber((totalscore / 30) * 100, 2)
        'equivalent = rating & "%"
        If rating >= 90 Then
            equivalent = "VERY GOOD"
        ElseIf rating >= 80 And rating <= 89 Then
            equivalent = "GOOD"
        ElseIf rating >= 70 And rating <= 79 Then
            equivalent = "POOR"
        Else
            equivalent = "TOTALLY INEFFECTIVE"
        End If

        Me.lblRating.Text = rating
        Me.lblEquivalent.Text = equivalent
        upPnlEval.Update()
    End Sub

    Private Sub Get_RecordData()
        Try

            Dim conducteddate As Date = CDate(Me.txtdate.Text)
            Dim conductedbay As String = Trim(Me.lblcurrentuser.Text)
            Dim branch As String = Trim(Me.cbobranch.Text)
            Dim center As String = Trim(Me.cboCenter.Text)
            Dim pa As String = Trim(Me.txtProgramOfficer.Text)

            'MsgBox(conducteddate)
            'MsgBox(conductedbay)
            'MsgBox(branch)
            'MsgBox(center)
            'MsgBox(pa)

            If con.State = 1 Then con.Close()
            con.Open()
            Dim sqlQuery As String = "OPS_Get_Eval_record '" & conducteddate & "'," & _
                "'" & conductedbay & "'," & _
                "'" & branch & "'," & _
                "'" & center & "'," & _
                "'" & pa & "'"

            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader = cmd.ExecuteReader()

            If dr.Read() Then
                Me.txtpo1.Text = CInt(dr("pa_arrived"))
                If txtpo1.Text = 0 Then rd_po1_q1.Checked = False Else rd_po1_q1.Checked = True
                If txtpo1.Text = 0 Then rd_po1_q2.Checked = True Else rd_po1_q2.Checked = False

                Me.txtpo2.Text = CInt(dr("pa_deliquent"))
                If txtpo2.Text = 0 Then rd_po2_q1.Checked = False Else rd_po2_q1.Checked = True
                If txtpo2.Text = 0 Then rd_po2_q2.Checked = True Else rd_po2_q2.Checked = False

                Me.txtpo3.Text = CInt(dr("pa_discuss"))
                If txtpo3.Text = 0 Then rd_po3_q1.Checked = False Else rd_po3_q1.Checked = True
                If txtpo3.Text = 0 Then rd_po3_q2.Checked = True Else rd_po3_q2.Checked = False

                Me.txtc1.Text = CInt(dr("ctr_follow"))
                If txtc1.Text = 0 Then rd_c1_q1.Checked = False Else rd_c1_q1.Checked = True
                If txtc1.Text = 0 Then rd_c1_q2.Checked = True Else rd_c1_q2.Checked = False

                Me.txtc2.Text = CInt(dr("ctr_start"))
                If txtc2.Text = 0 Then rd_c2_q1.Checked = False Else rd_c2_q1.Checked = True
                If txtc2.Text = 0 Then rd_c2_q2.Checked = True Else rd_c2_q2.Checked = False

                Me.txtc3.Text = CInt(dr("ctr_provide"))
                If txtc3.Text = 0 Then rd_c3_q1.Checked = False Else rd_c3_q1.Checked = True
                If txtc3.Text = 0 Then rd_c3_q2.Checked = True Else rd_c3_q2.Checked = False

                Me.txtc4.Text = CInt(dr("ctr_trans"))
                If txtc4.Text = 0 Then rd_c4_q1.Checked = False Else rd_c4_q1.Checked = True
                If txtc4.Text = 0 Then rd_c4_q2.Checked = True Else rd_c4_q2.Checked = False

                Me.txtc5.Text = CInt(dr("ctr_visual"))
                If txtc5.Text = 0 Then rd_c5_q1.Checked = False Else rd_c5_q1.Checked = True
                If txtc5.Text = 0 Then rd_c5_q2.Checked = True Else rd_c5_q2.Checked = False

                Me.txtc6.Text = CInt(dr("ctr_minutes"))
                If txtc6.Text = 0 Then rd_c6_q1.Checked = False Else rd_c6_q1.Checked = True
                If txtc6.Text = 0 Then rd_c6_q2.Checked = True Else rd_c6_q2.Checked = False

                Me.txtatt1.Text = CInt(dr("att_min"))
                If txtatt1.Text = 0 Then rd_att1_q1.Checked = False Else rd_att1_q1.Checked = True
                If txtatt1.Text = 0 Then rd_att1_q2.Checked = True Else rd_att1_q2.Checked = False

                Me.txtatt2.Text = CInt(dr("att_pm"))
                If txtatt2.Text = 0 Then rd_att2_q1.Checked = False Else rd_att2_q1.Checked = True
                If txtatt2.Text = 0 Then rd_att2_q2.Checked = True Else rd_att2_q2.Checked = False

                Me.txtcm1.Text = CInt(dr("mat_house"))
                If txtcm1.Text = 0 Then rd_cm1_q1.Checked = False Else rd_cm1_q1.Checked = True
                If txtcm1.Text = 0 Then rd_cm1_q2.Checked = True Else rd_cm1_q2.Checked = False

                Me.txtcm2.Text = CInt(dr("mat_sign"))
                If txtcm2.Text = 0 Then rd_cm2_q1.Checked = False Else rd_cm2_q1.Checked = True
                If txtcm2.Text = 0 Then rd_cm2_q2.Checked = True Else rd_cm2_q2.Checked = False

                Me.txtcm3.Text = CInt(dr("mat_pitong"))
                If txtcm3.Text = 0 Then rd_cm3_q1.Checked = False Else rd_cm3_q1.Checked = True
                If txtcm3.Text = 0 Then rd_cm3_q2.Checked = True Else rd_cm3_q2.Checked = False

                Me.txtcm4.Text = CInt(dr("mat_pang"))
                If txtcm4.Text = 0 Then rd_cm4_q1.Checked = False Else rd_cm4_q1.Checked = True
                If txtcm4.Text = 0 Then rd_cm4_q2.Checked = True Else rd_cm4_q2.Checked = False

                Me.txtcm5.Text = CInt(dr("mat_org"))
                If txtcm5.Text = 0 Then rd_cm5_q1.Checked = False Else rd_cm5_q1.Checked = True
                If txtcm5.Text = 0 Then rd_cm5_q2.Checked = True Else rd_cm5_q2.Checked = False

                Me.txtcm6.Text = CInt(dr("mat_column"))
                If txtcm6.Text = 0 Then rd_cm6_q1.Checked = False Else rd_cm6_q1.Checked = True
                If txtcm6.Text = 0 Then rd_cm6_q2.Checked = True Else rd_cm6_q2.Checked = False

                Me.txtcm7.Text = CInt(dr("mat_black"))
                If txtcm7.Text = 0 Then rd_cm7_q1.Checked = False Else rd_cm7_q1.Checked = True
                If txtcm7.Text = 0 Then rd_cm7_q2.Checked = True Else rd_cm7_q2.Checked = False

                Me.txtcm8.Text = CInt(dr("mat_wall"))
                If txtcm8.Text = 0 Then rd_cm8_q1.Checked = False Else rd_cm8_q1.Checked = True
                If txtcm8.Text = 0 Then rd_cm8_q2.Checked = True Else rd_cm8_q2.Checked = False

                Me.txtcm9.Text = CInt(dr("mat_center"))
                If txtcm9.Text = 0 Then rd_cm9_q1.Checked = False Else rd_cm9_q1.Checked = True
                If txtcm9.Text = 0 Then rd_cm9_q2.Checked = True Else rd_cm9_q2.Checked = False

                Me.txtcm10.Text = CInt(dr("mat_attendance"))
                If txtcm10.Text = 0 Then rd_cm10_q1.Checked = False Else rd_cm10_q1.Checked = True
                If txtcm10.Text = 0 Then rd_cm10_q2.Checked = True Else rd_cm10_q2.Checked = False

                Me.txtcm11.Text = CInt(dr("mat_weekly"))
                If txtcm11.Text = 0 Then rd_cm11_q1.Checked = False Else rd_cm11_q1.Checked = True
                If txtcm11.Text = 0 Then rd_cm11_q2.Checked = True Else rd_cm11_q2.Checked = False

                Me.txtpay1.Text = CInt(dr("pay_weekly"))
                If txtpay1.Text = 0 Then rd_pay1_q1.Checked = False Else rd_pay1_q1.Checked = True
                If txtpay1.Text = 0 Then rd_pay1_q2.Checked = True Else rd_pay1_q2.Checked = False

                Me.txtpay2.Text = CInt(dr("pay_center"))
                If txtpay2.Text = 0 Then rd_pay2_q1.Checked = False Else rd_pay2_q1.Checked = True
                If txtpay2.Text = 0 Then rd_pay2_q2.Checked = True Else rd_pay2_q2.Checked = False

                Me.txtpay3.Text = CInt(dr("pay_indiv"))
                If txtpay3.Text = 0 Then rd_pay3_q1.Checked = False Else rd_pay3_q1.Checked = True
                If txtpay3.Text = 0 Then rd_pay3_q2.Checked = True Else rd_pay3_q2.Checked = False

                Me.txtpay4.Text = CInt(dr("pay_pm"))
                If txtpay4.Text = 0 Then rd_pay4_q1.Checked = False Else rd_pay4_q1.Checked = True
                If txtpay4.Text = 0 Then rd_pay4_q2.Checked = True Else rd_pay4_q2.Checked = False

                Me.lblTotalScore.Text = CInt(dr("total_score"))
                Me.lblRating.Text = CDbl(dr("compliance"))
                Me.lblEquivalent.Text = Trim(dr("equivalent_rating"))
            End If

            dr.Close()
            cmd.Dispose()
            upPnlEval.Update()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub cmdsave_Click(sender As Object, e As System.EventArgs) Handles cmdsave.Click
        If txtActual.Text = "" Or txtMax.Text = "" Or txtMax.Text = 0 Then
            Prompt("Please input number of members. Total active members in center must not be zero.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            txtMax.Focus()
        ElseIf CInt(Me.txtMax.Text) < CInt(Me.txtActual.Text) Then
            Prompt("Number of clients present must not be less than total active members.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            txtActual.Focus()
        Else

            Try
                GEt_score()

                Dim center As String = Trim(Me.cboCenter.Text)
                Dim centeraddress As String = Trim(Me.txtaddress.Text)
                Dim branch As String = Trim(Me.cbobranch.Text)
                Dim pa As String = Trim(Me.txtProgramOfficer.Text)
                Dim conducteddate As Date = CDate(Me.txtdate.Text)
                Dim conductedbay As String = Trim(Me.lblcurrentuser.Text)
                Dim paarrived As Integer = CInt(Me.txtpo1.Text)
                Dim padeliquent As Integer = CInt(Me.txtpo2.Text)
                Dim padiscuss As Integer = CInt(Me.txtpo3.Text)
                Dim ctrfollow As Integer = CInt(Me.txtc1.Text)
                Dim ctrstart As Integer = CInt(Me.txtc2.Text)
                Dim ctrprovide As Integer = CInt(Me.txtc3.Text)
                Dim ctrtrans As Integer = CInt(Me.txtc4.Text)
                Dim ctrisual As Integer = CInt(Me.txtc5.Text)
                Dim ctrminutes As Integer = CInt(Me.txtc6.Text)
                Dim attmax As Integer = CInt(Me.txtMax.Text)
                Dim attactual As Integer = CInt(Me.txtActual.Text)
                Dim attmin As Integer = CInt(Me.txtatt1.Text)
                Dim attpm As Integer = CInt(Me.txtatt2.Text)
                Dim mathouse As Integer = CInt(Me.txtcm1.Text)
                Dim matsign As Integer = CInt(Me.txtcm2.Text)
                Dim matpitong As Integer = CInt(Me.txtcm3.Text)
                Dim matpang As Integer = CInt(Me.txtcm4.Text)
                Dim matorg As Integer = CInt(Me.txtcm5.Text)
                Dim matcolumn As Integer = CInt(Me.txtcm6.Text)
                Dim matblack As Integer = CInt(Me.txtcm7.Text)
                Dim matwall As Integer = CInt(Me.txtcm8.Text)
                Dim matcenter As Integer = CInt(Me.txtcm9.Text)
                Dim matattendance As Integer = CInt(Me.txtcm10.Text)
                Dim matweekly As Integer = CInt(Me.txtcm11.Text)
                Dim payweekly As Integer = CInt(Me.txtpay1.Text)
                Dim paycenter As Integer = CInt(Me.txtpay2.Text)
                Dim payindiv As Integer = CInt(Me.txtpay3.Text)
                Dim paypm As Integer = CInt(Me.txtpay4.Text)
                Dim comments As String = Me.txtComments.Text.Replace("'", "''")
                Dim totalscore As Integer = CInt(Me.lblTotalScore.Text)
                Dim compliance As Decimal = CDbl(Me.lblRating.Text)
                Dim rating As String = Trim(Me.lblEquivalent.Text)

                If con.State = 1 Then con.Close()
                con.Open()
                Dim sqlQuery As String = ""


                sqlQuery = "OPS_insert_eval " & _
                                    "'" & center & "', " & _
                                    "'" & centeraddress & "', " & _
                                    "'" & branch & "', " & _
                                    "'" & pa & "', " & _
                                    "'" & conducteddate & "', " & _
                                    "'" & conductedbay & "'," & _
                                    "'" & paarrived & "'," & _
                                    "'" & padeliquent & "'," & _
                                    "'" & padiscuss & "'," & _
                                    "'" & ctrfollow & "'," & _
                                    "'" & ctrstart & "'," & _
                                    "'" & ctrprovide & "'," & _
                                    "'" & ctrtrans & "'," & _
                                    "'" & ctrisual & "'," & _
                                    "'" & ctrminutes & "'," & _
                                    "'" & attmax & "'," & _
                                    "'" & attactual & "'," & _
                                    "'" & attmin & "'," & _
                                    "'" & attpm & "'," & _
                                    "'" & mathouse & "'," & _
                                    "'" & matsign & "'," & _
                                    "'" & matpitong & "'," & _
                                    "'" & matpang & "'," & _
                                    "'" & matorg & "'," & _
                                    "'" & matcolumn & "'," & _
                                    "'" & matblack & "'," & _
                                    "'" & matwall & "'," & _
                                    "'" & matcenter & "'," & _
                                    "'" & matattendance & "'," & _
                                    "'" & matweekly & "'," & _
                                    "'" & payweekly & "'," & _
                                    "'" & paycenter & "'," & _
                                    "'" & payindiv & "'," & _
                                    "'" & paypm & "'," & _
                                    "'" & comments & "'," & _
                                    "'" & totalscore & "'," & _
                                    "'" & compliance & "'," & _
                                    "'" & rating & "'"

                Dim cmd As New SqlCommand(sqlQuery, con)
                cmd.ExecuteNonQuery()
                Prompt("Successfully Submitted", Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
                upPnlEval.Update()

            Catch ex As Exception
                Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            Finally
                con.Close()
            End Try
        End If

    End Sub

    Protected Sub rd_po1_q1_CheckedChanged(sender As Object, e As EventArgs) Handles rd_po1_q1.CheckedChanged
        If Me.rd_po1_q1.Checked = True Then Me.txtpo1.Text = "1" Else Me.txtpo1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po1_q2.CheckedChanged
        If Me.rd_po1_q2.Checked = True Then Me.txtpo1.Text = "0" Else Me.txtpo1.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po2_q1.CheckedChanged
        If Me.rd_po2_q1.Checked = True Then Me.txtpo2.Text = "1" Else Me.txtpo2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po2_q2.CheckedChanged
        If Me.rd_po2_q2.Checked = True Then Me.txtpo2.Text = "0" Else Me.txtpo2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po3_q1.CheckedChanged
        If Me.rd_po3_q1.Checked = True Then Me.txtpo3.Text = "1" Else Me.txtpo3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po3_q2.CheckedChanged
        If Me.rd_po3_q2.Checked = True Then Me.txtpo3.Text = "0" Else Me.txtpo3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c1_q1.CheckedChanged
        If Me.rd_c1_q1.Checked = True Then Me.txtc1.Text = "1" Else Me.txtc1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c1_q2.CheckedChanged
        If Me.rd_c1_q2.Checked = True Then Me.txtc1.Text = "0" Else Me.txtc1.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c2_q1.CheckedChanged
        If Me.rd_c2_q1.Checked = True Then Me.txtc2.Text = "1" Else Me.txtc2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c2_q2.CheckedChanged
        If Me.rd_c2_q2.Checked = True Then Me.txtc2.Text = "0" Else Me.txtc2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c3_q1.CheckedChanged
        If Me.rd_c3_q1.Checked = True Then Me.txtc3.Text = "1" Else Me.txtc3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c3_q2.CheckedChanged
        If Me.rd_c3_q2.Checked = True Then Me.txtc3.Text = "0" Else Me.txtc3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c4_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c4_q1.CheckedChanged
        If Me.rd_c4_q1.Checked = True Then Me.txtc4.Text = "2" Else Me.txtc4.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c4_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c4_q2.CheckedChanged
        If Me.rd_c4_q2.Checked = True Then Me.txtc4.Text = "0" Else Me.txtc4.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c5_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c5_q1.CheckedChanged
        If Me.rd_c5_q1.Checked = True Then Me.txtc5.Text = "1" Else Me.txtc5.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c5_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c5_q2.CheckedChanged
        If Me.rd_c5_q2.Checked = True Then Me.txtc5.Text = "0" Else Me.txtc5.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c6_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c6_q1.CheckedChanged
        If Me.rd_c6_q1.Checked = True Then Me.txtc6.Text = "1" Else Me.txtc6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c6_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c6_q2.CheckedChanged
        If Me.rd_c6_q2.Checked = True Then Me.txtc6.Text = "0" Else Me.txtc6.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att1_q1.CheckedChanged
        If Me.rd_att1_q1.Checked = True Then Me.txtatt1.Text = "2" Else Me.txtc6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att1_q2.CheckedChanged
        If Me.rd_att1_q2.Checked = True Then Me.txtatt1.Text = "0" Else Me.txtatt1.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att2_q1.CheckedChanged
        If Me.rd_att2_q1.Checked = True Then Me.txtatt2.Text = "1" Else Me.txtatt2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att2_q2.CheckedChanged
        If Me.rd_att2_q2.Checked = True Then Me.txtatt2.Text = "0" Else Me.txtatt2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm1_q1.CheckedChanged
        If Me.rd_cm1_q1.Checked = True Then Me.txtcm1.Text = "1" Else Me.txtcm1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm1_q2.CheckedChanged
        If Me.rd_cm1_q2.Checked = True Then Me.txtcm1.Text = "0" Else Me.txtcm1.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm2_q1.CheckedChanged
        If Me.rd_cm2_q1.Checked = True Then Me.txtcm2.Text = "1" Else Me.txtcm2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm2_q2.CheckedChanged
        If Me.rd_cm2_q2.Checked = True Then Me.txtcm2.Text = "0" Else Me.txtcm2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm3_q1.CheckedChanged
        If Me.rd_cm3_q1.Checked = True Then Me.txtcm3.Text = "1" Else Me.txtcm3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm3_q2.CheckedChanged
        If Me.rd_cm3_q2.Checked = True Then Me.txtcm3.Text = "0" Else Me.txtcm3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm4_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm4_q1.CheckedChanged
        If Me.rd_cm4_q1.Checked = True Then Me.txtcm4.Text = "1" Else Me.txtcm4.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm4_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm4_q2.CheckedChanged
        If Me.rd_cm4_q2.Checked = True Then Me.txtcm4.Text = "0" Else Me.txtcm4.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm5_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm5_q1.CheckedChanged
        If Me.rd_cm5_q1.Checked = True Then Me.txtcm5.Text = "1" Else Me.txtcm5.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm5_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm5_q2.CheckedChanged
        If Me.rd_cm5_q2.Checked = True Then Me.txtcm5.Text = "0" Else Me.txtcm5.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm6_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm6_q1.CheckedChanged
        If Me.rd_cm6_q1.Checked = True Then Me.txtcm6.Text = "1" Else Me.txtcm6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm6_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm6_q2.CheckedChanged
        If Me.rd_cm6_q2.Checked = True Then Me.txtcm6.Text = "0" Else Me.txtcm6.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm7_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm7_q1.CheckedChanged
        If Me.rd_cm7_q1.Checked = True Then Me.txtcm7.Text = "1" Else Me.txtcm6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm7_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm7_q2.CheckedChanged
        If Me.rd_cm7_q2.Checked = True Then Me.txtcm7.Text = "0" Else Me.txtcm7.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm8_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm8_q1.CheckedChanged
        If Me.rd_cm8_q1.Checked = True Then Me.txtcm8.Text = "1" Else Me.txtcm8.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm8_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm8_q2.CheckedChanged
        If Me.rd_cm8_q2.Checked = True Then Me.txtcm8.Text = "0" Else Me.txtcm8.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm9_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm9_q1.CheckedChanged
        If Me.rd_cm9_q1.Checked = True Then Me.txtcm9.Text = "1" Else Me.txtcm9.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm9_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm9_q2.CheckedChanged
        If Me.rd_cm9_q2.Checked = True Then Me.txtcm9.Text = "0" Else Me.txtcm9.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm10_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm10_q1.CheckedChanged
        If Me.rd_cm10_q1.Checked = True Then Me.txtcm10.Text = "2" Else Me.txtcm10.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm10_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm10_q2.CheckedChanged
        If Me.rd_cm10_q2.Checked = True Then Me.txtcm10.Text = "0" Else Me.txtcm10.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm11_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm11_q1.CheckedChanged
        If Me.rd_cm11_q1.Checked = True Then Me.txtcm11.Text = "1" Else Me.txtcm11.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm11_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm11_q2.CheckedChanged
        If Me.rd_cm11_q2.Checked = True Then Me.txtcm11.Text = "0" Else Me.txtcm11.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay1_q1.CheckedChanged
        If Me.rd_pay1_q1.Checked = True Then Me.txtpay1.Text = "2" Else Me.txtpay1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay1_q2.CheckedChanged
        If Me.rd_pay1_q2.Checked = True Then Me.txtpay1.Text = "0" Else Me.txtpay1.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay2_q1.CheckedChanged
        If Me.rd_pay2_q1.Checked = True Then Me.txtpay2.Text = "1" Else Me.txtpay2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay2_q2.CheckedChanged
        If Me.rd_pay2_q2.Checked = True Then Me.txtpay2.Text = "0" Else Me.txtpay2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay3_q1.CheckedChanged
        If Me.rd_pay3_q1.Checked = True Then Me.txtpay3.Text = "1" Else Me.txtpay3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay3_q2.CheckedChanged
        If Me.rd_pay3_q2.Checked = True Then Me.txtpay3.Text = "0" Else Me.txtpay3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay4_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay4_q1.CheckedChanged
        If Me.rd_pay4_q1.Checked = True Then Me.txtpay4.Text = "1" Else Me.txtpay4.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay4_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay4_q2.CheckedChanged
        If Me.rd_pay4_q2.Checked = True Then Me.txtpay4.Text = "0" Else Me.txtpay4.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub


    Private Sub btnViewScore_Click(sender As Object, e As System.EventArgs) Handles btnViewScore.Click
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub cmdReport_Click(sender As Object, e As System.EventArgs) Handles cmdReport.Click
        ' MsgBox("")
        Try

            Dim PA As String = Trim(Me.txtProgramOfficer.Text)
            Dim BRANCH As String = Trim(Me.cbobranch.Text)
            Dim CENTER As String = Trim(Me.cboCenter.Text)
            Dim cmd As New SqlCommand
            cmd.Connection = con
            cmd.CommandText = "OPS_cves_form"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("pa", PA)
            cmd.Parameters.AddWithValue("branch", BRANCH)
            cmd.Parameters.AddWithValue("center_code", CENTER)

            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            'MsgBox(Server.MapPath("reports/cvef_revised.rpt"))
            Dim dt As New DataTable
            da.Fill(dt)
            cryRpt.Load(Server.MapPath("report/cvef_revised.rpt"))
            cryRpt.SetDataSource(dt)

            Me.CRV.ReportSource = cryRpt
            'Me.crViewer.DataBind()

            Dim Filename As String = "report/CVEF" & BRANCH & CENTER & Replace(CStr(FormatDateTime(Date.Now, DateFormat.ShortDate)), "/", "") & ".pdf"
            If System.IO.File.Exists(Server.MapPath(Filename)) = True Then
                System.IO.File.Delete(Server.MapPath(Filename))
            End If
            Dim oDiskOpts As New DiskFileDestinationOptions
            cryRpt.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
            cryRpt.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat
            oDiskOpts.DiskFileName = Server.MapPath(Filename)
            cryRpt.ExportOptions.DestinationOptions = oDiskOpts
            cryRpt.Export()

            ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '" & Filename & "', target='_blank' );", True)
        Catch EX As Exception
            Prompt("ERROR: " & EX.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            cryRpt.Close()
            cryRpt.Dispose()
        End Try
    End Sub

    Private Sub txtdate_TextChanged(sender As Object, e As System.EventArgs) Handles txtdate.TextChanged
        Get_RecordData()
    End Sub

    Protected Sub lnkAddress_Click(sender As Object, e As EventArgs) Handles lnkAddress.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openChangeAddress();", True)
    End Sub

    Private Sub RetrieveProvince()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select province_desc from province order by province_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.drpProvince.Items.Clear()
            While dr.Read
                Me.drpProvince.Items.Add(Trim(dr("province_desc").ToString))
            End While
            BIND_Province()
            RetrieveMunicipality()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub RetrieveMunicipality()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select municipality_desc from municipality where province_code = (select province_code from province where province_desc = '" + drpProvince.SelectedItem.Text + "') order by municipality_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.drpMunicipality.Items.Clear()
            While dr.Read
                Me.drpMunicipality.Items.Add(Trim(dr("municipality_desc").ToString))
            End While
            BIND_Municipality()
            RetrieveBarangay()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub RetrieveBarangay()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select barangay_desc from barangay where municipality_code = (select municipality_code from municipality where municipality_desc = '" + drpMunicipality.SelectedItem.Text + "' and province_code = '" + hdnProvince.Value.ToString() + "') order by barangay_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.drpBarangay.Items.Clear()
            While dr.Read
                Me.drpBarangay.Items.Add(Trim(dr("barangay_desc").ToString))
            End While
            BIND_Barangay()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Province()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select province_code from province where province_desc = '" + drpProvince.SelectedItem.Text + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            dr.Read()
            Me.hdnProvince.Value = dr("province_code").ToString()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Municipality()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select municipality_code from municipality where municipality_desc = '" + drpMunicipality.SelectedItem.Text + "' and province_code = '" + hdnProvince.Value.ToString() + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            dr.Read()
            Me.hdnMunicipality.Value = dr("municipality_code").ToString()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Barangay()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select barangay_code from barangay where barangay_desc = '" + drpBarangay.SelectedItem.Text + "' and municipality_code = '" + hdnMunicipality.Value.ToString() + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            dr.Read()
            Me.hdnBarangay.Value = dr("barangay_code").ToString()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Protected Sub drpProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProvince.SelectedIndexChanged
        BIND_Province()
        RetrieveMunicipality()
    End Sub

    Protected Sub drpMunicipality_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpMunicipality.SelectedIndexChanged             
        BIND_Municipality()
        RetrieveBarangay()
    End Sub

    Protected Sub drpBarangay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpBarangay.SelectedIndexChanged
        BIND_Barangay()
        upPnlCAddress.Update()
    End Sub

    Protected Sub cmdOkCAddress_Click(sender As Object, e As EventArgs) Handles cmdOkCAddress.Click
        Try
            If con.State = 1 Then con.Close()
            con.Open()
            Dim sqlQuery As String = "Update center set barangay_code = '" + hdnBarangay.Value.ToString() + "', municipality_code = '" + hdnMunicipality.Value.ToString() + "', province_code = '" + hdnProvince.Value.ToString() + "' where center_desc = '" + cboCenter.SelectedItem.Text + "'"


            Dim cmd As New SqlCommand(sqlQuery, con)
            cmd.ExecuteNonQuery()
            
            Prompt("Successfully Saved", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            Get_Center_Address()
            upPnlEval.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBoxSite();", True)
        Finally
            con.Close()
        End Try
    End Sub
End Class