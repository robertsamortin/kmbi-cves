﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="CenterVisitEvaluation.Login" %>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="UTF-8">
  <title>Document</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    label {
      font-weight: 400;
    }
    .panel.panel-default {
      margin-top: 150px;
      max-width: 800px;
      margin-left: auto;
      margin-right: auto;
    }
    .panel-body {
      padding: 37px;
      box-shadow: 1px 1px 1px #009900;
    }
    .form-title {
      margin-top: 20px;
      margin-bottom: 20px;
      font-size: 20px;
      font-weight: 500;
    }
    .btn-danger {
      width: 100px;
      border-radius: 0;
      margin-top: 20px;
      margin-bottom: 20px;
    }
    .form-control {
      border-radius: 0;
    }
    img {
      width: 146px;
    }
    .col-sm-8.divider {
      border-left: 1px solid #ccc;
      padding-left: 37px;
    }
  </style>
  </head>
  <body>
    <div class="container">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-4">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <img src="aras/img/kmbi.png" alt="">
                  <div class="text-logo">
                    <small>Kabalikat para sa Maunlad na Buhay Inc.</small>
                    <small><em>(A Microfinance NGO)</em></small>
                  </div>
                </div>
                <div class="col-sm-12 text-center">
                  <%--<img src="./img/kmbi.png" alt="">--%>
                  <div class="text-logo">
                    <H4><B>CREDIT OPERATIONS GROUP</B></H4>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-8 divider">
              <div class="form-title">Login</div>
              <form runat="server">
                <div class="form-group">
                  <label>Username:</label>
                  <asp:TextBox ID="txtusername" runat="server" class="form-control" placeholder="username" required autofocus></asp:TextBox>
                </div>
                <div class="form-group">
                  <label>Password:</label>
                  <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" class="form-control" placeholder="password" required></asp:TextBox>
                </div>
                <div class="checkbox">
                  <%--<label><input type="checkbox"> Remember me</label>--%>
                    <asp:Label ID="lblInvalidlogin" runat="server" Text="" Visible="false" Cssclass="checkbox pull-left"></asp:Label>
                </div>
                   <asp:Button type="submit" ID="cmdLogin" runat="server" Text="Login" CssClass="btn btn-danger btn-lg"></asp:Button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
