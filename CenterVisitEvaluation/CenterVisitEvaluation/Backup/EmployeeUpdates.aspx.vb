﻿Imports System.Data.SqlClient

Public Class EmployeeUpdates
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Form.DefaultButton = Me.LinkButton1.UniqueID

        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then

                BIND_ALL_Employees()
                BIND_Branch()

            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub BIND_ALL_Employees()
        Try

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim emp As String = Trim(Me.lblcurrentuser.Text)
            Dim search As String = Trim(Me.txtSearch.Text)

            Dim dv As New DataView(TryCast(GetDocument("ops_employeeUpdates '" & search & "', '" & search & "'").Tables(0), DataTable))

            Session("Emp") = dv
            grdEmployee.DataSource = dv
            grdEmployee.DataBind()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub BIND_Branch()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "Select * from branch order by branch_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()


            Me.cboToBranch.Items.Clear()
            Me.cboToBranch.Items.Add("Select Branch")
            While dr.Read
                Me.cboToBranch.Items.Add(Trim(dr("branch_desc").ToString))
            End While

            UpdatePanel1.Update()
            dr.Close()
            cmd.Dispose()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub cboToBranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboToBranch.SelectedIndexChanged
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchdesc As String = Trim(Me.cboToBranch.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "Select a.branch_code, b.area_code, c.region_code from branch a inner join area b on a.area_code = b.area_code inner join region c on b.region_code = c.region_code where a.branch_desc  = '" & branchdesc & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            If dr.Read Then
                Me.txtToBranchCode.Text = dr("branch_code").ToString
                Me.txtToAreaCode.Text = dr("area_code").ToString
                Me.txtToRegCode.Text = dr("region_code").ToString
            End If

            dr.Close()
            cmd.Dispose()

            UpdatePanel1.Update()
            pnlUpdateEmployee.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub cboPosition_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPosition.SelectedIndexChanged
        If cboPosition.Text = "PO" Then
            txtPosCode.Text = "PA"
        ElseIf cboPosition.Text = "PUH" Then
            txtPosCode.Text = "PUS"
        ElseIf cboPosition.Text = "RD" Then
            txtPosCode.Text = "RM"
        Else
            txtPosCode.Text = cboPosition.Text
        End If
    End Sub

    Protected Sub grdEmployee_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdEmployee.PageIndexChanging
        grdEmployee.PageIndex = e.NewPageIndex
        grdEmployee.DataSource = Session("Emp")
        grdEmployee.DataBind()
    End Sub

    Protected Sub grdEmployee_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdEmployee.RowCommand
        Me.lblTitleUpdate.Text = "Update Employee"

        Dim empcode As String = e.CommandArgument

        Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

        Dim lastname As String = CType(row.Cells(1), DataControlFieldCell).Text.Trim()
        Dim firstname As String = CType(row.Cells(2), DataControlFieldCell).Text.Trim()
        Dim middlename As String = CType(row.Cells(3), DataControlFieldCell).Text.Trim()
        Dim position As String = CType(row.Cells(4), DataControlFieldCell).Text.Trim()
        Dim unit = CType(row.Cells(5), DataControlFieldCell).Text.Trim()
        Dim branch = CType(row.Cells(6), DataControlFieldCell).Text.Trim()
        Dim area = CType(row.Cells(7), DataControlFieldCell).Text.Trim()
        Dim region = CType(row.Cells(8), DataControlFieldCell).Text.Trim()
        Dim regioncode = CType(row.Cells(9), DataControlFieldCell).Text.Trim()
        Dim areacode = CType(row.Cells(10), DataControlFieldCell).Text.Trim()
        Dim branchcode = CType(row.Cells(11), DataControlFieldCell).Text.Trim()

        Me.lblName.Text = lastname & ", " & firstname
        Try
            If e.CommandName = "Employee" Then
                Session("employeeid") = empcode

                'ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)
                Me.txtEmpCode.Text = empcode
                Me.txtLastName.Text = lastname
                Me.txtFirstName.Text = firstname
                Me.txtMiddleName.Text = middlename
                Me.cboToBranch.Text = branch
                Me.txtToAreaCode.Text = areacode
                Me.txtToRegCode.Text = regioncode
                Me.txtToBranchCode.Text = branchcode
                Me.cboUnit.Text = unit
                If position = "PO" Then
                    cboPosition.Text = position
                    txtPosCode.Text = "PA"
                ElseIf position = "PUH" Then
                    cboPosition.Text = position
                    txtPosCode.Text = "PUS"
                ElseIf position = "RD" Then
                    cboPosition.Text = position
                    txtPosCode.Text = "RM"
                Else
                    txtPosCode.Text = position
                    cboPosition.Text = position
                End If
                UpdatePanel1.Update()

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openEditModal();", True)
                pnlUpdateEmployee.Update()

            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub grdEmployee_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdEmployee.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.Ascending Then
            direction = SortDirection.Descending
            sortingDirection = "Desc"
        Else
            direction = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        grdEmployee.SetPageIndex(0)

        Dim dv As DataView = Session("Emp")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("Emp") = dv
        grdEmployee.DataSource = dv
        grdEmployee.DataBind()
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtToBranchCode.Text = "" Or txtToAreaCode.Text = "" Or txtToRegCode.Text = "" Then
            Prompt("Please select To Branch.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboToBranch.Focus()
        ElseIf txtPosCode.Text = "" Then
            Prompt("Please select Position.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboToBranch.Focus()
        ElseIf txtPosCode.Text = "PUS" And cboUnit.Text = "Select Unit" Then
            Prompt("Please select Unit.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboToBranch.Focus()
        ElseIf txtLastName.Text = "" Then
            Prompt("Last Name must not be empty", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            txtLastName.Focus()
        ElseIf txtFirstName.Text = "" Then
            Prompt("First Name must not be empty", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            txtFirstName.Focus()
        ElseIf txtMiddleName.Text = "" Then
            Prompt("Middle Name must not be empty", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            txtMiddleName.Focus()
        ElseIf txtEmpCode.Text = "" Then
            Prompt("ID Number must not be empty", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            txtEmpCode.Focus()
        Else


            Try
                Dim empcode As String = Trim(Me.txtEmpCode.Text)
                Dim regcode As String = Trim(Me.txtToRegCode.Text)
                Dim areacode As String = Trim(Me.txtToAreaCode.Text)
                Dim bracode As String = Trim(Me.txtToBranchCode.Text)
                Dim unitcode As String
                If cboUnit.Text = "Select Unit" Then
                    unitcode = "A"
                Else
                    unitcode = Trim(Me.cboUnit.Text)
                End If
                Dim poscode As String = Trim(Me.txtPosCode.Text)
                Dim username = Trim(Me.txtFirstName.Text.ToLower()) & Trim(Me.txtLastName.Text.ToLower())
                Dim active = Trim(Me.lblTitleUpdate.Text)
                Dim lastname = Trim(Me.txtLastName.Text.ToUpper())
                Dim firstname = Trim(Me.txtFirstName.Text.ToUpper())
                Dim middlename = Trim(Me.txtMiddleName.Text.ToUpper())

                If con.State = 1 Then con.Close()
                con.Open()
                Dim sqlQuery As String = ""


                sqlQuery = "sp_updateEmployeeUsers " &
                                "'" & empcode & "', " &
                                "'" & regcode & "', " &
                                "'" & areacode & "', " &
                                "'" & bracode & "', " &
                                "'" & unitcode & "', " &
                                "'" & poscode & "'," &
                                "'" & active & "'," &
                                "'" & username & "'," &
                                "'" & lastname & "'," &
                                "'" & firstname & "'," &
                                "'" & middlename & "'"




                Dim cmd As New SqlCommand(sqlQuery, con)
                cmd.ExecuteNonQuery()
                Prompt("Successfully Submitted", Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
                UpdatePanel1.Update()
                pnlUpdateEmployee.Update()
            Catch ex As Exception
                Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            Finally
                con.Close()
            End Try
        End If
    End Sub

    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SORTING AND PAGING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    Public Function GetDocument(ByVal query As String) As DataSet

        If con.State = ConnectionState.Open Or con.State = 16 Or con.State = 2 Or con.State = ConnectionState.Fetching Or con.State = ConnectionState.Executing Then con.Close()
        Dim cmd As New SqlCommand(query, con)
        con.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        con.Close()
        Return ds
    End Function

    Public Property direction() As SortDirection
        Get
            If ViewState("directionState") Is Nothing Then
                ViewState("directionState") = SortDirection.Ascending
            End If
            Return DirectCast(ViewState("directionState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("directionState") = value
        End Set
    End Property

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Try
            Dim search As String = Trim(Me.txtSearch.Text)

            Dim dv As New DataView(TryCast(GetDocument("ops_employeeUpdates '" & search & "', '" & search & "'").Tables(0), DataTable))
            Session("Emp") = dv
            grdEmployee.DataSource = dv
            grdEmployee.DataBind()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Me.lblTitleUpdate.Text = "Add New Employee"
        Try

            'ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)
            Me.txtEmpCode.Text = ""
            Me.txtLastName.Text = ""
            Me.txtFirstName.Text = ""
            Me.txtMiddleName.Text = ""
            Me.cboToBranch.Text = "Select Branch"
            Me.txtToAreaCode.Text = ""
            Me.txtToRegCode.Text = ""
            Me.txtToBranchCode.Text = ""
            Me.cboUnit.Text = "Select Unit"

            cboPosition.Text = "Select Position"
            txtPosCode.Text = ""

            UpdatePanel1.Update()

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openEditModal();", True)
            pnlUpdateEmployee.Update()


        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub
End Class