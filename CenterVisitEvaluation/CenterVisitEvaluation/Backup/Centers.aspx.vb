﻿Imports System.Data.SqlClient

Public Class Centers
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Form.DefaultButton = Me.lnkSearch.UniqueID

        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                BIND_Branch()
                lblNoRecords.Visible = False
                grdCenterList.Visible = False
            End If
        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Protected Sub lnkSearch_Click(sender As Object, e As EventArgs) Handles lnkSearch.Click
        Try
            
            Dim dv As New DataView(TryCast(GetDocument("OPS_center_search '" + drpBranch.SelectedItem.Text + "', '" + txtCenter.Text + "', '" + lblaccesslevel.Text + "', '" + lblareacode.Text + "', '" + lblcurrentuser.Text + "'").Tables(0), DataTable))

            dv.Sort = "branch_name ASC, center_code ASC"
            Session("Center") = dv
            grdCenterList.DataSource = dv
            grdCenterList.DataBind()
            grdCenterList.Visible = True

            If grdCenterList.Rows.Count = 0 Then
                lblNoRecords.Visible = True
                lblNoRecords.Text = "NO RECORDS FOUND!"
            Else
                lblNoRecords.Visible = False
            End If

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub BIND_Branch()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "OPS_list_branch '" & branchcode & "', '" & areacode & "', '" & accesslevel & "','" & pa & "', '" & pus & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.drpBranch.Items.Clear()
            While dr.Read
                Me.drpBranch.Items.Add(Trim(dr("branch_desc").ToString))
            End While

            If drpBranch.Items.Count > 1 Then
                drpBranch.Items.Insert(0, "ALL")
            End If

            UpdatePanel1.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub grdCenterList_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCenterList.PageIndexChanging
        grdCenterList.PageIndex = e.NewPageIndex
        grdCenterList.DataSource = Session("Center")
        grdCenterList.DataBind()
    End Sub

    Private Sub grdCenterList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCenterList.RowCommand, grdCenterList.RowCommand
        Dim centercode As String = e.CommandArgument
        Try
            If e.CommandName = "Center" Then
                Session("centercode") = centercode
                UpdatePanel1.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/CenterDetails.aspx?id=" + centercode + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub grdCenterList_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCenterList.Sorting, grdCenterList.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.Ascending Then
            direction = SortDirection.Descending
            sortingDirection = "Desc"
        Else
            direction = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        grdCenterList.SetPageIndex(0)

        Dim dv As DataView = Session("Center")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("Center") = dv
        grdCenterList.DataSource = dv
        grdCenterList.DataBind()
    End Sub

    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SORTING AND PAGING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    Public Function GetDocument(ByVal query As String) As DataSet

        If con.State = ConnectionState.Open Or con.State = 16 Or con.State = 2 Or con.State = ConnectionState.Fetching Or con.State = ConnectionState.Executing Then con.Close()
        Dim cmd As New SqlCommand(query, con)
        con.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        con.Close()
        Return ds
    End Function

    Public Property direction() As SortDirection
        Get
            If ViewState("directionState") Is Nothing Then
                ViewState("directionState") = SortDirection.Ascending
            End If
            Return DirectCast(ViewState("directionState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("directionState") = value
        End Set
    End Property

End Class