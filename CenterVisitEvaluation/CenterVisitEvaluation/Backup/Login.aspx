﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="CenterVisitEvaluation.Login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head runat="server">
	<meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>:::Center Visit Evaluation (Login)</title>
	<!-- CSS -->
  
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container">
        <div class="panel-title" align="center" style="margin-top:50px;">
            <img src="assets/img/logo.png" alt="" class="img-responsive" />
        </div>
       <div id="loginbox" style="margin-top:10px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title">Login</div>
                </div>  
                    <div style="padding-top:30px" class="panel-body" >
                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            <form id="loginform" class="form-horizontal" role="form" runat="server">

                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <asp:TextBox ID="txtusername" runat="server" class="form-control" placeholder="username" required autofocus></asp:TextBox>
                                </div>
                                
                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" class="form-control" placeholder="password" required></asp:TextBox>
                                </div>

                                <div class="input-group">
                                    <asp:Label ID="lblInvalidlogin" runat="server" Text="" Visible="false" Cssclass="checkbox pull-left"></asp:Label>
                                </div>
                                
                                <div style="margin-top:10px" class="form-group">
                                    <div class="col-sm-12 controls">
                                        <asp:Button ID="cmdLogin" runat="server" Text="Login" CssClass="btn btn-success"></asp:Button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                                        </div>
                                    </div>
                                </div>    
                           </form> 
                    </div>                     
            </div>  
        </div>
    </div>



   
    <!-- Javascript -->
<%--    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>--%>

   
</body>
</html>