﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Profile.aspx.vb" Inherits="CenterVisitEvaluation.Profile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/path/to/jquery.bs_grid.min.css">
<script type="text/javascript" src="/path/to/jquery.bs_grid.min.js"></script>
<script type="text/javascript" src="/path/to/bs_grid/localization/en.min.js"></script>
<link rel="stylesheet" type="text/css" href="/path/to/jquery.bs_pagination.min.css">
<script type="text/javascript" src="/path/to/jquery.bs_pagination.min.js"></script>
<script type="text/javascript" src="/path/to/bs_pagination/localization/en.min.js"></script>
    
    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="container-fluid">              
                    <div style="font-size:16px;">
                        <asp:Label ID="lblEvaluatorName" runat="server" ></asp:Label>
                    </div>
                    <br />
                    <div id="cnt_" class="panel panel-default">                       
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <div class="navbar-brand">
                                        <span class="glyphicon glyphicon-list"></span>
                                        LIST OF CENTERS EVALUATED
                                    </div>
                                </div>
                            </div>
                        </nav>    
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="form-inline">
                                    <div class="form-group" style="margin:10px">
                                        <label class="control-label">Date From:</label>                                                                                       
                                        <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                    </div>
                                    <div class="form-group" style="margin:10px">
                                        <label class="control-label">Date To:</label>                           
                                        <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary" Font-Size="16px"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                        <div class="panel-body" style="font-size:14px;">
                            <div class="text-danger" style="font-weight:bold; font-size:large">                           
                                <asp:Label ID="lblNoRecords" runat="server"></asp:Label>
                            </div>
                            <div>                           
                                <asp:Label ID="lblCount" runat="server"></asp:Label>
                            </div>
                            <asp:GridView ID="grdCenterList" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                            GridLines="None" CssClass="gridview text-center" Width="100%" PageSize="25">
                            <Columns>
                            
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEvalID" runat="server" CausesValidation="False" cssclass="btn btn-primary" 
                                                CommandArgument="<%#Bind('eval_id') %>" CommandName="Centers" 
                                                Text='<%# If(Eval("conducted_by").ToString() = lblcurrentuser.Text, "Edit", "View") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>                    
                                </asp:TemplateField>
                           
                            
                                <asp:BoundField DataField="conducted_date" SortExpression="conducted_date" HeaderText="Date Conducted" ItemStyle-HorizontalAlign = "Center" DataFormatString="{0:D}"/>
                                <asp:BoundField DataField="branch_name" SortExpression="branch_name" HeaderText="Branch" />                                
                                <asp:BoundField DataField="center_code" SortExpression="center_code" HeaderText="Center" />
                                <asp:BoundField DataField="total_score" SortExpression="total_score" HeaderText="Total Score" ItemStyle-HorizontalAlign = "Center" DataFormatString="{0:N0}"/>
                                <asp:BoundField DataField="compliance" SortExpression="compliance" HeaderText="Rating" ItemStyle-HorizontalAlign = "Center" DataFormatString="{0:p}" />
                                <asp:BoundField DataField="equivalent_rating" HeaderText="Equivalent" />
                                <asp:TemplateField HeaderText="Has Notes/Comments?">
                                   <ItemTemplate>
                                     <asp:Image ID="ImageDetailItem" width="22" height="22" ImageUrl='<%# If(Eval("comments").ToString() = "", "assets/img/cross.png", "assets/img/check.png") %>' runat="server" />                                        
                                   </ItemTemplate>
                                </asp:TemplateField>
                            </Columns> 
                            <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />
                        </asp:GridView>
                        </div>
                    </div>

                </div>

            </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Label ID="lblEvaluatorID" runat="server" Text="Label" Visible="False"></asp:Label>
    <div class="modal fade"  tabindex="-1" id="modMessageBox" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<asp:UpdateProgress ID="pnlUpdateProgress" runat="server">
        <ProgressTemplate>
            <div style="background-color: Gray; 
                filter:alpha(opacity=60); 
                opacity:0.60; 
                width: 100%; 
                top: 0; 
                left: 0; 
                position: fixed; 
                height: 100%; 
                z-index: 100001">
            </div>
            <div style="filter: alpha(opacity=100);
                opacity: 1;
                position:  fixed;
                top: 50%;
                left: 50%;
                margin: -50px 0px 0px -25px;
                z-index: 100001">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            jQuery(function ($) {
                $("#txtDateFrom").datepicker();
                $("#txtDateTo").datepicker();
            });

        });

        jQuery(function ($) {
            $("#txtDateFrom").datepicker();
            $("#txtDateTo").datepicker();
        });

    </script>

<!--SESSIONS-->
    <div> 
        <asp:Label ID="lblfullname" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblposition" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchcode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchdesc" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblsearch" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Visible="False"></asp:Label>
    </div>
</asp:Content>
