﻿Imports AjaxControlToolkit

Public Module modPrompt
    Public Sub Prompt(ByVal Message As String, _
                      ByVal LMessage As Label, _
                      ByVal UPanel As UpdatePanel)
        LMessage.Text = Message
        UPanel.Update()
    End Sub
End Module
