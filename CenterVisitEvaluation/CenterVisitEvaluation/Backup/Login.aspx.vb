﻿Imports System.Data.SqlClient

Public Class Login
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.txtusername.Focus()
    End Sub

    Private Sub LOGIN()
        Try
            If con.State = 1 Then con.Close()
            con.Open()
            Dim usersname As String = Trim(Me.txtusername.Text)
            Dim password As String = Trim(Me.txtpassword.Text)


            Dim sqlQuery As String = "MF_login '" & Trim(usersname) & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            If dr.Read Then
                If PasswordHash.ValidatePassword(password, ":1000:" & dr("user_salt").ToString & ":" & dr("user_hash").ToString) Then
                    Session("fullname") = Trim(dr("full_name").ToString)
                    Session("position") = Trim(dr("position_desc").ToString)
                    Session("branchcode") = Trim(dr("branch_code").ToString)
                    Session("branchdesc") = dr("branch_desc").ToString & " BRANCH"
                    Session("currentuser") = Trim(dr("emp_code").ToString)
                    Session("accesslevel") = CInt(dr("access_level"))
                    Session("areacode") = Trim(dr("area_code"))

                    If Session("accesslevel") = 1 Or Session("accesslevel") = 3 Or Session("accesslevel") = 4 Then
                        Me.lblInvalidlogin.Visible = True
                        Me.lblInvalidlogin.Text = "Access Denied"
                    Else
                        Me.lblInvalidlogin.Visible = False
                        Me.lblInvalidlogin.Text = ""
                        FormsAuthentication.RedirectFromLoginPage(usersname, True)
                        'Response.Redirect("~/Default.aspx?userid=" & Trim(Session("currentuser")) & Trim(Session("branch_code")), False)
                    End If
                Else
                    Me.lblInvalidlogin.Visible = True
                    Me.lblInvalidlogin.Text = "Invalid Password"
                    Me.txtpassword.Focus()
                End If
            Else
                Me.lblInvalidlogin.Visible = True
                Me.lblInvalidlogin.Text = "Invalid Username"
                Me.txtusername.Focus()
            End If
        Catch ex As Exception
            Me.lblInvalidlogin.Visible = True
            Me.lblInvalidlogin.Text = "ERROR:" & ex.Message
            'MsgBox("ERROR:" & ex.Message, MsgBoxStyle.Critical, "ERROR")
        Finally
            con.Close()
        End Try
    End Sub

    Protected Sub cmdLogin_Click(sender As Object, e As EventArgs) Handles cmdLogin.Click
        LOGIN()
    End Sub
End Class