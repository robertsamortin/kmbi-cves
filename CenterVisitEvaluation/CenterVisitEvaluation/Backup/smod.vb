﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO

Namespace JDO

    Public Class Smod

        Private Shared conn As SqlConnection
        Private Shared sqlCommand As SqlCommand
        Private Shared DB As SqlDataAdapter
        Private Shared DS As New DataSet()

        Public Sub Smod()
            conn = New SqlConnection("Server=198.143.141.249,2433;Database=KIIS;UID=james;Password=J@mes4")
        End Sub

        Public Shared Function Openconn() As SqlConnection

            conn = New SqlConnection("Server=198.143.141.249,2433;Database=KIIS;UID=james;Password=J@mes4")

            If conn.State = ConnectionState.Closed OrElse conn.State = ConnectionState.Broken Then
                conn.Open()
            End If
            Return conn

        End Function


        Public Shared Function Closeconn() As SqlConnection

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            Return conn
        End Function

        Public Shared Sub ExecuteQuery(txtQuery As String)

            Dim cmd As New SqlCommand()
            Try
                cmd.Connection = Openconn()
                cmd.CommandText = txtQuery
                cmd.ExecuteNonQuery()
            Catch Ex As Exception
                Throw Ex
            Finally
                cmd = Nothing
                Closeconn()
            End Try
        End Sub

        Public Shared Function returnDataSet(txtQuery As String) As DataSet
            conn.Open()
            sqlCommand = conn.CreateCommand()
            DB = New SqlDataAdapter(txtQuery, conn)
            DS.Reset()
            DB.Fill(DS)
            conn.Close()
            Return (DS)
        End Function

        Public Shared Function returnDataTable(con As SqlConnection, txtQuery As String) As DataTable
            Dim cmd As New SqlCommand()
            Dim adap As SqlDataAdapter
            Try
                cmd.Connection = con
                cmd.CommandText = txtQuery
                adap = New SqlDataAdapter(cmd)
                Dim dt As New DataTable()
                adap.Fill(dt)

                Return dt
            Catch Ex As Exception
                Throw Ex
            Finally
                cmd = Nothing
                Closeconn()
            End Try
        End Function

        Public Shared Function returnDataReader(con As SqlConnection, txtQuery As String) As SqlDataReader
            Dim cmd As New SqlCommand()

            Try
                cmd.Connection = con
                cmd.CommandText = txtQuery
                Dim rd As SqlDataReader
                rd = cmd.ExecuteReader()
                rd.Read()
                Return rd
            Catch Ex As Exception
                Throw Ex
            Finally
                cmd = Nothing
                Closeconn()
            End Try
        End Function

    End Class

End Namespace

