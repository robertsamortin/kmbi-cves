﻿Imports System.Data.SqlClient
Imports CenterVisitEvaluation.JDO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared


Public Class _Default
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                BIND_Region()
                BIND_Area()
                BIND_Branch()
                BIND_Unit()
                BIND_PO()
                Me.txtDateFrom.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
                Me.txtDateTo.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub BIND_Region()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim sqlQuery As String

            Me.drpRegion.Items.Clear()

            If accesslevel >= 8 Then
                sqlQuery = "Select region_desc from region"
                Me.drpRegion.Items.Insert(0, "All")
            Else
                sqlQuery = "Select region_desc from region where region_code = (select region_code from area where area_code = '" + areacode + "')"
            End If

            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()


            While dr.Read
                Me.drpRegion.Items.Add(Trim(dr("region_desc").ToString))
            End While


            UpdatePanel1.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Area()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim sqlQuery As String

            Me.drpArea.Items.Clear()

            If accesslevel >= 7 Then
                sqlQuery = "Select area_desc from area where region_code = (select region_code from region where region_desc = '" + drpRegion.Text + "')"
                Me.drpArea.Items.Insert(0, "All")
            Else
                sqlQuery = "Select area_desc from area where area_code= '" + areacode + "'"
            End If

            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()


            While dr.Read
                Me.drpArea.Items.Add(Trim(dr("area_desc").ToString))
            End While


            UpdatePanel1.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Branch()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim sqlQuery As String

            Me.drpBranch.Items.Clear()

            If accesslevel >= 6 Then
                sqlQuery = "Select branch_desc from branch where area_code = (select area_code from area where area_desc = '" + drpArea.Text + "')"
                Me.drpBranch.Items.Insert(0, "All")
            Else
                sqlQuery = "Select branch_desc from branch where branch_code = '" + branchcode + "'"
            End If

            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()


            While dr.Read
                Me.drpBranch.Items.Add(Trim(dr("branch_desc").ToString))
            End While


            UpdatePanel1.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Unit()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim currentuser As String = Trim(Me.lblcurrentuser.Text)
            Dim sqlQuery As String

            Me.drpUnit.Items.Clear()

            If accesslevel = 2 Then
                sqlQuery = "Select unit_code from employee where emp_code = '" + currentuser + "'"

                Dim cmd As New SqlCommand(sqlQuery, con)
                Dim dr As SqlDataReader
                dr = cmd.ExecuteReader()


                While dr.Read
                    Me.drpUnit.Items.Add(Trim(dr("unit_code").ToString))
                End While

            Else
                If drpBranch.Text <> "All" Then
                    Me.drpUnit.Items.Add("All")
                    Me.drpUnit.Items.Add("A")
                    Me.drpUnit.Items.Add("B")
                    Me.drpUnit.Items.Add("C")
                Else
                    Me.drpUnit.Items.Add("All")
                End If

            End If

            UpdatePanel1.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub BIND_PO()
        If drpUnit.Text <> "All" Then
            Me.drpPO.Items.Clear()
            Me.drpPO.Items.Add("All")
            Me.drpPO.Items.Add("PO1")
            Me.drpPO.Items.Add("PO2")
            Me.drpPO.Items.Add("PO3")
            Me.drpPO.Items.Add("PO4")
            Me.drpPO.Items.Add("PO5")
            Me.drpPO.Items.Add("PO6")
            Me.drpPO.Items.Add("PO7")
        Else
            Me.drpPO.Items.Clear()
            Me.drpPO.Items.Add("All")
        End If
    End Sub

    Private Sub BIND_EVALUATORS()
        Try
            Dim datefrom As Date = CDate(Me.txtDateFrom.Text)
            Dim dateto As Date = CDate(Me.txtDateTo.Text)

            Dim dv As New DataView(TryCast(GetDocument("ops_personnel '" & datefrom & "', '" & dateto & "', '" & drpRegion.Text & "', '" & drpArea.Text & "', '" & drpBranch.Text & "', '" & drpUnit.Text & "', '" & drpPO.Text & "'").Tables(0), DataTable))

            dv.Sort = "branch_desc ASC"
            Session("Default") = dv
            grdEvaluatorList.DataSource = dv
            grdEvaluatorList.DataBind()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub drpRegion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpRegion.SelectedIndexChanged
        BIND_Area()
        BIND_Branch()
        drpUnit.Items.Clear()
        Me.drpUnit.Items.Add("All")
        drpUnit.SelectedIndex = 0
        BIND_PO()
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "closereport();", True)
    End Sub

    Protected Sub drpArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpArea.SelectedIndexChanged
        BIND_Branch()
        drpUnit.Items.Clear()
        Me.drpUnit.Items.Add("All")
        drpUnit.SelectedIndex = 0
        BIND_PO()
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "closereport();", True)
    End Sub

    Protected Sub drpBranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpBranch.SelectedIndexChanged
        drpUnit.SelectedIndex = 0
        Me.drpUnit.Items.Clear()

        If drpBranch.Text = "All" Then
            Me.drpUnit.Items.Add("All")

        Else
            Me.drpUnit.Items.Add("All")
            Me.drpUnit.Items.Add("A")
            Me.drpUnit.Items.Add("B")
            Me.drpUnit.Items.Add("C")
        End If
        BIND_PO()
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "closereport();", True)
    End Sub

    Protected Sub drpUnit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpUnit.SelectedIndexChanged
        BIND_PO()
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "closereport();", True)
    End Sub

    Private Sub Score()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "exec OPS_filter '" + txtDateFrom.Text + "','" + txtDateTo.Text + "','" + drpRegion.Text + "','" + drpArea.Text + "','" + drpBranch.Text + "','" + drpUnit.Text + "','" + drpPO.Text + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            If dr.Read Then
                If dr("count") = 0 Then
                    Me.lblNoRecords.Text = "NO RECORDS FOUND!"
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "closetable();", True)
                    Me.pnlSummary.Style.Add("display", "none")
                    Me.grdEvaluatorList.Visible = False
                Else
                    Me.lblNoRecords.Text = ""
                    Me.pnlSummary.Style.Add("display", "block")
                    Me.grdEvaluatorList.Visible = True
                    Me.lblCount.Text = dr("count").ToString()
                    Me.lblTotalMember.Text = dr("attmax").ToString()
                    Me.lblActualAtt.Text = dr("attactual").ToString()
                    Me.lblPercentAtt.Text = dr("attpercent").ToString() + "%"
                    Me.lblScore.Text = dr("avg_score").ToString()
                    hdnRating.Value = dr("rating")
                    Me.lblRating.Text = dr("rating").ToString() + "%"

                    Me.lblpa_arrived.Text = dr("pa_arrived").ToString() + " (" + Math.Round(dr("pa_arrived") * 100 / dr("count")).ToString() + "%)"
                    Me.lblpa_delinquent.Text = dr("pa_delinquent").ToString() + " (" + Math.Round(dr("pa_delinquent") * 100 / dr("count")).ToString() + "%)"
                    Me.lblpa_discuss.Text = dr("pa_discuss").ToString() + " (" + Math.Round(dr("pa_discuss") * 100 / dr("count")).ToString() + "%)"
                    Me.lblctr_follow.Text = dr("ctr_follow").ToString() + " (" + Math.Round(dr("ctr_follow") * 100 / dr("count")).ToString() + "%)"
                    Me.lblctr_start.Text = dr("ctr_start").ToString() + " (" + Math.Round(dr("ctr_start") * 100 / dr("count")).ToString() + "%)"
                    Me.lblctr_provide.Text = dr("ctr_provide").ToString() + " (" + Math.Round(dr("ctr_provide") * 100 / dr("count")).ToString() + "%)"
                    Me.lblctr_trans.Text = dr("ctr_trans").ToString() + " (" + Math.Round(dr("ctr_trans") * 100 / dr("count")).ToString() + "%)"
                    Me.lblctr_visual.Text = dr("ctr_visual").ToString() + " (" + Math.Round(dr("ctr_visual") * 100 / dr("count")).ToString() + "%)"
                    Me.lblctr_minutes.Text = dr("ctr_minutes").ToString() + " (" + Math.Round(dr("ctr_minutes") * 100 / dr("count")).ToString() + "%)"
                    Me.lblatt_min.Text = dr("att_min").ToString() + " (" + Math.Round(dr("att_min") * 100 / dr("count")).ToString() + "%)"
                    Me.lblatt_pm.Text = dr("att_pm").ToString() + " (" + Math.Round(dr("att_pm") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_house.Text = dr("mat_house").ToString() + " (" + Math.Round(dr("mat_house") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_sign.Text = dr("mat_sign").ToString() + " (" + Math.Round(dr("mat_sign") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_pitong.Text = dr("mat_pitong").ToString() + " (" + Math.Round(dr("mat_pitong") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_pang.Text = dr("mat_pang").ToString() + " (" + Math.Round(dr("mat_pang") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_org.Text = dr("mat_org").ToString() + " (" + Math.Round(dr("mat_org") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_column.Text = dr("mat_column").ToString() + " (" + Math.Round(dr("mat_column") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_black.Text = dr("mat_black").ToString() + " (" + Math.Round(dr("mat_black") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_wall.Text = dr("mat_wall").ToString() + " (" + Math.Round(dr("mat_wall") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_center.Text = dr("mat_center").ToString() + " (" + Math.Round(dr("mat_center") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_attendance.Text = dr("mat_attendance").ToString() + " (" + Math.Round(dr("mat_attendance") * 100 / dr("count")).ToString() + "%)"
                    Me.lblmat_weekly.Text = dr("mat_weekly").ToString() + " (" + Math.Round(dr("mat_weekly") * 100 / dr("count")).ToString() + "%)"
                    Me.lblpay_weekly.Text = dr("pay_weekly").ToString() + " (" + Math.Round(dr("pay_weekly") * 100 / dr("count")).ToString() + "%)"
                    Me.lblpay_center.Text = dr("pay_center").ToString() + " (" + Math.Round(dr("pay_center") * 100 / dr("count")).ToString() + "%)"
                    Me.lblpay_indiv.Text = dr("pay_indiv").ToString() + " (" + Math.Round(dr("pay_indiv") * 100 / dr("count")).ToString() + "%)"
                    Me.lblpay_pm.Text = dr("pay_pm").ToString() + " (" + Math.Round(dr("pay_pm") * 100 / dr("count")).ToString() + "%)"

                    Equivalent()

                    pnlSummary.Focus()

                End If

            End If

            UpdatePanel1.Update()


        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub Equivalent()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim rating As Double
            Dim equivalent As String

            rating = hdnRating.Value

            If rating >= 90 Then
                equivalent = "VERY GOOD"
                lblEquivalent.Font.Bold = True
                lblEquivalent.ForeColor = Drawing.Color.Green
            ElseIf rating >= 80 And rating <= 89 Then
                equivalent = "GOOD"
                lblEquivalent.Font.Bold = True
                lblEquivalent.ForeColor = Drawing.Color.Green
            ElseIf rating >= 70 And rating <= 79 Then
                equivalent = "POOR"
                lblEquivalent.Font.Bold = True
                lblEquivalent.ForeColor = Drawing.Color.Red
            Else
                equivalent = "TOTALLY INEFFECTIVE"
                lblEquivalent.Font.Bold = True
                lblEquivalent.ForeColor = Drawing.Color.Red
            End If

            Me.lblEquivalent.Text = equivalent

            UpdatePanel1.Update()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Protected Sub btnFilter_Click(sender As Object, e As EventArgs) Handles btnFilter.Click
        BIND_EVALUATORS()
        Score()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Script", "circlechart();", True)
    End Sub
    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim cryRpt As New ReportDocument
            cryRpt.Load(Server.MapPath("report/cves_summary_report.rpt"))

            Dim sqlQuery As String = "OPS_filter"
            Dim cmd As New SqlCommand(sqlQuery, con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Clear()
            cmd.Parameters.AddWithValue("dateFrom", txtDateFrom.Text)
            cmd.Parameters.AddWithValue("dateTo", txtDateTo.Text)
            cmd.Parameters.AddWithValue("region", drpRegion.Text)
            cmd.Parameters.AddWithValue("area", drpArea.Text)
            cmd.Parameters.AddWithValue("branch", drpBranch.Text)
            cmd.Parameters.AddWithValue("unit", drpUnit.Text)
            cmd.Parameters.AddWithValue("po", drpPO.Text)
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            'MsgBox(Server.MapPath("reports/cvef_revised.rpt"))
            Dim dt As New DataTable
            da.Fill(dt)

            cryRpt.SetDataSource(dt)
            'CRV.DataBind()

            Me.CRV.ReportSource = cryRpt

            Dim Filename As String = "report/SummaryReport" & Replace(CStr(FormatDateTime(txtDateFrom.Text, DateFormat.ShortDate)), "/", "") & Replace(CStr(FormatDateTime(txtDateTo.Text, DateFormat.ShortDate)), "/", "") & ".pdf"
            If System.IO.File.Exists(Server.MapPath(Filename)) = True Then
                System.IO.File.Delete(Server.MapPath(Filename))
            End If
            Dim oDiskOpts As New DiskFileDestinationOptions
            cryRpt.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
            cryRpt.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat
            oDiskOpts.DiskFileName = Server.MapPath(Filename)
            cryRpt.ExportOptions.DestinationOptions = oDiskOpts
            cryRpt.Export()

            ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '" & Filename & "', target='_blank' );", True)
        Catch EX As Exception
            Prompt("ERROR: " & EX.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            CRV.Dispose()
        End Try
    End Sub

    Protected Sub drpPO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpPO.SelectedIndexChanged
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "closereport();", True)
    End Sub

    Private Sub grdEvaluatorList_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdEvaluatorList.PageIndexChanging
        grdEvaluatorList.PageIndex = e.NewPageIndex
        grdEvaluatorList.DataSource = Session("Default")
        grdEvaluatorList.DataBind()
    End Sub

    Private Sub grdEvaluatorList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdEvaluatorList.RowCommand
        Dim conductedby As String = e.CommandArgument
        Try
            If e.CommandName = "Evaluator" Then
                Session("employeeid") = conductedby
                UpdatePanel1.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub grdEvaluatorList_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdEvaluatorList.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.AscendingOrder Then
            direction = SortDirection.DescendingOrder
            sortingDirection = "Desc"
        Else
            direction = SortDirection.AscendingOrder
            sortingDirection = "Asc"
        End If
        grdEvaluatorList.SetPageIndex(0)

        Dim dv As DataView = Session("Default")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("RM") = dv
        grdEvaluatorList.DataSource = dv
        grdEvaluatorList.DataBind()
    End Sub

    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SORTING AND PAGING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    Public Function GetDocument(ByVal query As String) As DataSet

        If con.State = ConnectionState.Open Or con.State = 16 Or con.State = 2 Or con.State = ConnectionState.Fetching Or con.State = ConnectionState.Executing Then con.Close()
        Dim cmd As New SqlCommand(query, con)
        con.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        con.Close()
        Return ds
    End Function

    Public Property direction() As SortDirection
        Get
            If ViewState("directionState") Is Nothing Then
                ViewState("directionState") = SortDirection.AscendingOrder
            End If
            Return DirectCast(ViewState("directionState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("directionState") = value
        End Set
    End Property

End Class