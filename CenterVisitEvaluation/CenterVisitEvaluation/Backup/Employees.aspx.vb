﻿Imports System.Data.SqlClient

Public Class Employees
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                If lblaccesslevel.Text < 8 Then
                    RD.Visible = False
                    If lblaccesslevel.Text < 7 Then
                        AM.Visible = False
                    End If
                End If
                BIND_RegionalDirectors()
                BIND_AreaManagers()
                BIND_BranchManagers()
                BIND_PUH()

            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub BIND_RegionalDirectors()
        Try

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim emp As String = Trim(Me.lblcurrentuser.Text)

            Dim dv As New DataView(TryCast(GetDocument("OPS_list_employee '" & lblbranchcode.Text & "', '" & lblareacode.Text & "', 'RM','" & lblaccesslevel.Text & "', '" & lblcurrentuser.Text & "'").Tables(0), DataTable))

            dv.Sort = "region_desc ASC"
            Session("RM") = dv
            grdRegionalDirectors.DataSource = dv
            grdRegionalDirectors.DataBind()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub BIND_AreaManagers()
        Try

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim emp As String = Trim(Me.lblcurrentuser.Text)
            
            Dim dv As New DataView(TryCast(GetDocument("OPS_list_employee '" & lblbranchcode.Text & "', '" & lblareacode.Text & "', 'AM','" & lblaccesslevel.Text & "', '" & lblcurrentuser.Text & "'").Tables(0), DataTable))

            dv.Sort = "name ASC"
            Session("AM") = dv
            grdAreaManagers.DataSource = dv
            grdAreaManagers.DataBind()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub BIND_BranchManagers()
        Try

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim emp As String = Trim(Me.lblcurrentuser.Text)
            
            Dim dv As New DataView(TryCast(GetDocument("OPS_list_employee '" & lblbranchcode.Text & "', '" & lblareacode.Text & "', 'BM','" & lblaccesslevel.Text & "', '" & lblcurrentuser.Text & "'").Tables(0), DataTable))

            dv.Sort = "name ASC"
            Session("BM") = dv
            grdBranchManagers.DataSource = dv
            grdBranchManagers.DataBind()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub BIND_PUH()
        Try

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim emp As String = Trim(Me.lblcurrentuser.Text)

            Dim dv As New DataView(TryCast(GetDocument("OPS_list_employee '" & lblbranchcode.Text & "', '" & lblareacode.Text & "', 'PUS','" & lblaccesslevel.Text & "', '" & lblcurrentuser.Text & "'").Tables(0), DataTable))

            dv.Sort = "branch_desc ASC, name ASC"
            Session("PUH") = dv
            grdPUH.DataSource = dv
            grdPUH.DataBind()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub grdRegionalDirectors_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRegionalDirectors.PageIndexChanging
        grdRegionalDirectors.PageIndex = e.NewPageIndex
        grdRegionalDirectors.DataSource = Session("RM")
        grdRegionalDirectors.DataBind()
    End Sub

    Protected Sub grdRegionalDirectors_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdRegionalDirectors.RowCommand
        Dim conductedby As String = e.CommandArgument
        Try
            If e.CommandName = "Employee" Then
                Session("employeeid") = conductedby
                UpdatePanel1.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub grdRegionalDirectors_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdRegionalDirectors.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.Ascending Then
            direction = SortDirection.Descending
            sortingDirection = "Desc"
        Else
            direction = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        grdRegionalDirectors.SetPageIndex(0)

        Dim dv As DataView = Session("RM")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("RM") = dv
        grdRegionalDirectors.DataSource = dv
        grdRegionalDirectors.DataBind()
    End Sub

    Protected Sub grdAreaManagers_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAreaManagers.PageIndexChanging
        grdAreaManagers.PageIndex = e.NewPageIndex
        grdAreaManagers.DataSource = Session("AM")
        grdAreaManagers.DataBind()
    End Sub

    Protected Sub grdAreaManagers_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAreaManagers.RowCommand
        Dim conductedby As String = e.CommandArgument
        Try
            If e.CommandName = "Employee" Then
                Session("employeeid") = conductedby
                UpdatePanel2.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub grdAreaManagers_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAreaManagers.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.Ascending Then
            direction = SortDirection.Descending
            sortingDirection = "Desc"
        Else
            direction = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        grdAreaManagers.SetPageIndex(0)

        Dim dv As DataView = Session("AM")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("AM") = dv
        grdAreaManagers.DataSource = dv
        grdAreaManagers.DataBind()
    End Sub

    Protected Sub grdBranchManagers_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBranchManagers.PageIndexChanging
        grdBranchManagers.PageIndex = e.NewPageIndex
        grdBranchManagers.DataSource = Session("BM")
        grdBranchManagers.DataBind()
    End Sub

    Protected Sub grdBranchManagers_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdBranchManagers.RowCommand
        Dim conductedby As String = e.CommandArgument
        Try
            If e.CommandName = "Employee" Then
                Session("employeeid") = conductedby
                UpdatePanel3.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub grdBranchManagers_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdBranchManagers.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.Ascending Then
            direction = SortDirection.Descending
            sortingDirection = "Desc"
        Else
            direction = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        grdBranchManagers.SetPageIndex(0)

        Dim dv As DataView = Session("BM")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("BM") = dv
        grdBranchManagers.DataSource = dv
        grdBranchManagers.DataBind()
    End Sub

    Protected Sub grdPUH_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdPUH.PageIndexChanging
        grdPUH.PageIndex = e.NewPageIndex
        grdPUH.DataSource = Session("PUH")
        grdPUH.DataBind()
    End Sub

    Protected Sub grdPUH_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPUH.RowCommand
        Dim conductedby As String = e.CommandArgument
        Try
            If e.CommandName = "Employee" Then
                Session("employeeid") = conductedby
                UpdatePanel4.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub grdPUH_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPUH.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.Ascending Then
            direction = SortDirection.Descending
            sortingDirection = "Desc"
        Else
            direction = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        grdPUH.SetPageIndex(0)

        Dim dv As DataView = Session("PUH")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("PUH") = dv
        grdPUH.DataSource = dv
        grdPUH.DataBind()

    End Sub

    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SORTING AND PAGING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    Public Function GetDocument(ByVal query As String) As DataSet

        If con.State = ConnectionState.Open Or con.State = 16 Or con.State = 2 Or con.State = ConnectionState.Fetching Or con.State = ConnectionState.Executing Then con.Close()
        Dim cmd As New SqlCommand(query, con)
        con.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        con.Close()
        Return ds
    End Function

    Public Property direction() As SortDirection
        Get
            If ViewState("directionState") Is Nothing Then
                ViewState("directionState") = SortDirection.Ascending
            End If
            Return DirectCast(ViewState("directionState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("directionState") = value
        End Set
    End Property

End Class