﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Employees.aspx.vb" Inherits="CenterVisitEvaluation.Employees" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/jquery-ui.js" type="text/javascript"></script>
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">            
            <div class="container-fluid">
                <div class="panel panel-default">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    EMPLOYEES
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div id="RD" class="panel panel-default" runat="server">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                              Regional Directors
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>                         
                                                <div class="panel-body">
                                                    <asp:GridView ID="grdRegionalDirectors" runat="server" AllowPaging="True" 
                                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                                    GridLines="None" CssClass="gridview" Width="75%" PageSize="25">
                                                        <Columns>                           
                                                            <asp:TemplateField HeaderText="Name" SortExpression="name">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEmp1" runat="server" CausesValidation="False"
                                                                            CommandArgument="<%#Bind('emp_code') %>" CommandName="Employee" 
                                                                            Text="<%#Bind('name') %>" >
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>                    
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="region_desc" SortExpression="region_desc" HeaderText="Region"/>
                                                        </Columns> 
                                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />                                               
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div id="AM" class="panel panel-default" runat="server">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Area Managers
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="panel-body">
                                                    <asp:GridView ID="grdAreaManagers" runat="server" AllowPaging="True" 
                                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                                    GridLines="None" CssClass="gridview" Width="75%" PageSize="25">
                                                        <Columns>                           
                                                            <asp:TemplateField HeaderText="Name" SortExpression="name">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEmp2" runat="server" CausesValidation="False"
                                                                            CommandArgument="<%#Bind('emp_code') %>" CommandName="Employee" 
                                                                            Text="<%#Bind('name') %>" >
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>                    
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="area_desc" SortExpression="area_desc" HeaderText="Area"/>
                                                        </Columns> 
                                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />                                               
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div id="Branch" class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Branch Managers
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="panel-body">
                                                    <asp:GridView ID="grdBranchManagers" runat="server" AllowPaging="True" 
                                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                                    GridLines="None" CssClass="gridview" Width="75%" PageSize="25">
                                                        <Columns>                           
                                                            <asp:TemplateField HeaderText="Name" SortExpression="name">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEmp3" runat="server" CausesValidation="False"
                                                                            CommandArgument="<%#Bind('emp_code') %>" CommandName="Employee" 
                                                                            Text="<%#Bind('name') %>" >
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>                    
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="branch_desc" SortExpression="branch_desc" HeaderText="Branch"/>
                                                        </Columns> 
                                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />                                               
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div id="PUH" class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                Program Unit Heads
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="panel-body">
                                                    <asp:GridView ID="grdPUH" runat="server" AllowPaging="True" 
                                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                                                    GridLines="None" CssClass="gridview" Width="75%" PageSize="25">
                                                        <Columns>                           
                                                            <asp:TemplateField HeaderText="Name" SortExpression="name">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEmp4" runat="server" CausesValidation="False"
                                                                            CommandArgument="<%#Bind('emp_code') %>" CommandName="Employee" 
                                                                            Text="<%#Bind('name') %>" >
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>                    
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="branch_desc" SortExpression="branch_desc" HeaderText="Branch"/>
                                                        </Columns> 
                                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />                                               
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       
    

    <div class="modal fade"  tabindex="-1" id="modMessageBox" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<!--SESSIONS-->
    <div> 
        <asp:Label ID="lblfullname" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblposition" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchcode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchdesc" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblsearch" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Visible="False"></asp:Label>
    </div>

</asp:Content>
