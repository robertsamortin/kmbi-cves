﻿Imports System.Data.SqlClient

Public Class MyProfile
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblcurrentuser.Text = Session("currentuser")

            If Not IsPostBack Then
                BIND_centerlist()
                Me.txtDateFrom.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
                Me.txtDateTo.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub grdCenterList_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCenterList.PageIndexChanging
        grdCenterList.PageIndex = e.NewPageIndex
        grdCenterList.DataSource = Session("MyProfile")
        grdCenterList.DataBind()
    End Sub

    Protected Sub grdCenterList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCenterList.RowCommand
        Dim evalid As String = e.CommandArgument
        Try
            If e.CommandName = "Centers" Then
                Session("evalid") = evalid
                UpdatePanel1.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Details.aspx?id=" + evalid + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub grdEvaluatorList_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCenterList.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.Ascending Then
            direction = SortDirection.Descending
            sortingDirection = "Desc"
        Else
            direction = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        grdCenterList.SetPageIndex(0)

        Dim dv As DataView = Session("MyProfile")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("MyProfile") = dv
        grdCenterList.DataSource = dv
        grdCenterList.DataBind()
    End Sub

    Private Sub BIND_centerlist()
        Try

            Dim dv As New DataView(TryCast(GetDocument("ops_evaluator_center_list '" & Trim(Me.lblcurrentuser.Text) & "', '', ''").Tables(0), DataTable))

            dv.Sort = "conducted_date DESC"
            Session("MyProfile") = dv
            grdCenterList.DataSource = dv
            grdCenterList.DataBind()

            lblCount.Text = "Evaluations Conducted: " + grdCenterList.Rows.Count.ToString
            If grdCenterList.Rows.Count = 0 Then
                Me.lblNoRecords.Text = "NO RECORDS FOUND!"
                lblCount.Visible = False
            End If

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Try

            Dim dv As New DataView(TryCast(GetDocument("ops_evaluator_center_list '" & Trim(Me.lblcurrentuser.Text) & "', '" + txtDateFrom.Text + "', '" + txtDateTo.Text + "'").Tables(0), DataTable))

            dv.Sort = "conducted_date DESC"
            Session("MyProfile") = dv
            grdCenterList.DataSource = dv
            grdCenterList.DataBind()

            If grdCenterList.Rows.Count = 0 Then
                Me.lblNoRecords.Text = "NO RECORDS FOUND!"
                lblCount.Visible = False
            Else
                Me.lblNoRecords.Text = ""
                lblCount.Visible = True
            End If

            lblCount.Text = "Evaluations Done: " + grdCenterList.Rows.Count.ToString
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SORTING AND PAGING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    Public Function GetDocument(ByVal query As String) As DataSet

        If con.State = ConnectionState.Open Or con.State = 16 Or con.State = 2 Or con.State = ConnectionState.Fetching Or con.State = ConnectionState.Executing Then con.Close()
        Dim cmd As New SqlCommand(query, con)
        con.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        con.Close()
        Return ds
    End Function

    Public Property direction() As SortDirection
        Get
            If ViewState("directionState") Is Nothing Then
                ViewState("directionState") = SortDirection.Ascending
            End If
            Return DirectCast(ViewState("directionState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("directionState") = value
        End Set
    End Property

End Class