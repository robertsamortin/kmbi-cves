﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Details.aspx.vb" Inherits="CenterVisitEvaluation.Details" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/jquery-ui.js" type="text/javascript"></script>
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upPnlEval" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <nav id="myNav" class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    CENTER VISIT EVALUATION FORM
                                </div>                            
                            </div>                                                     
                        </div>                        
                    </nav>
                    <div>
                        <asp:Label ID="lblEdited" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="panel-body">                       
                        <div class="form-group">
                            <div class="row">
        	                    <div class="col-sm-2">                                   
                	                <label>Evaluation Date:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                	                    <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" ClientIDMode="Static" autofocus required></asp:TextBox>
                                    </div>
                                </div>
       	                    </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Branch:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>
                                        <asp:TextBox ID="txtbranch" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Center:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-road"></i></span>
                                        <asp:TextBox ID="txtCenter" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Center Address:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                        <asp:TextBox ID="txtaddress" runat="server" CssClass="form-control" ReadOnly="true" ></asp:TextBox>
                                    </div> 
                                </div>                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Program Officer:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <asp:TextBox ID="txtProgramOfficer" runat="server" CssClass="form-control" ReadOnly="true" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <%--PROGRAM OFFICER--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno1" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            Program Officer
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Arrived ON or BEFORE the meeting time</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_po1_q1" runat="server" GroupName="po_q1" Text="Yes"/>
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_po1_q2" runat="server" GroupName="po_q1" Text="No" />
                                                </label>
                                                <asp:TextBox ID="txtpo1" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Program Officer visits delinquent client/s  (attendance and payment)</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_po2_q1" runat="server" GroupName="po_q2" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_po2_q2" runat="server" GroupName="po_q2" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtpo2" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Program Officer makes the discussion lively and interesting </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_po3_q1" runat="server" GroupName="po_q3" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_po3_q2" runat="server" GroupName="po_q3" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtpo3" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--CENTER MEETING--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno2" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            Center Meeting
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Follow the standard process</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_c1_q1" runat="server" GroupName="cen_q1" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_c1_q2" runat="server" GroupName="cen_q1" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtc1" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Start on time</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_c2_q1" runat="server" GroupName="cen_q2" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_c2_q2" runat="server" GroupName="cen_q2" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtc2" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Facilitator provide warm-up game (ice breaker) to the program Member</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_c3_q1" runat="server" GroupName="cen_q3" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_c3_q2" runat="server" GroupName="cen_q3" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtc3" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Transformational topic (discussion) was facilitated</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_c4_q1" runat="server" GroupName="cen_q4" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_c4_q2" runat="server" GroupName="cen_q4" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtc4" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Facilitator use visual aid during the presentation of topic</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_c5_q1" runat="server" GroupName="cen_q5" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_c5_q2" runat="server" GroupName="cen_q5" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtc5" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Minutes of the meeting is recorded in the logbook</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_c6_q1" runat="server" GroupName="cen_q6" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_c6_q2" runat="server" GroupName="cen_q6" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtc6" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--ATTENDANCE--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno3" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            Attendance
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-4">                                         
                                            <label>Total Active Clients in Center:</label>
                                            <asp:TextBox ID="txtMax" runat="server" class="form-control" Width="100" Text="0" onkeydown = "return (!((event.keyCode>=65 && event.keyCode <= 95) || event.keyCode >= 106) && event.keyCode!=32);"></asp:TextBox>                                            
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Number of Clients Present DURING the Evaluation:</label>
                                            <asp:TextBox ID="txtActual" runat="server" class="form-control" Width="100" Text="0" onkeydown = "return (!((event.keyCode>=65 && event.keyCode <= 95) || event.keyCode >= 106) && event.keyCode!=32);"></asp:TextBox>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>With a minimum of 80% attendance</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_att1_q1" runat="server" GroupName="att_q1" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_att1_q2" runat="server" GroupName="att_q1" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtatt1" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>80% of Program Members arrive in the center on-time</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_att2_q1" runat="server" GroupName="att_q2" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_att2_q2" runat="server" GroupName="att_q2" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtatt2" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--CENTER MATERIALS--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno4" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            Center Materials <i>(Available and follow the standard format)</i>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Center House or comfortable meeting venue</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm1_q1" runat="server" GroupName="cm_q1" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm1_q2" runat="server" GroupName="cm_q1" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm1" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Center Signage</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm2_q1" runat="server" GroupName="cm_q2" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm2_q2" runat="server" GroupName="cm_q2" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm2" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Pitong (7) Layunin</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm3_q1" runat="server" GroupName="cm_q3" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm3_q2" runat="server" GroupName="cm_q3" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm3" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Pangako ng Kaanib</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm4_q1" runat="server" GroupName="cm_q4" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm4_q2" runat="server" GroupName="cm_q4" Text="No"  />
                                                </label>
                                                <asp:TextBox ID="txtcm4" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Organizational Chart</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm5_q1" runat="server" GroupName="cm_q5" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm5_q2" runat="server" GroupName="cm_q5" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm5" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Columnar Book or Financial Record</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm6_q1" runat="server" GroupName="cm_q6" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm6_q2" runat="server" GroupName="cm_q6" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm6" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Black Board / white board</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm7_q1" runat="server" GroupName="cm_q7" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm7_q2" runat="server" GroupName="cm_q7" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm7" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Wall Clock</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm8_q1" runat="server" GroupName="cm_q8" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm8_q2" runat="server" GroupName="cm_q8" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm8" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Center Internal Policy (folder)</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm9_q1" runat="server" GroupName="cm_q9" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm9_q2" runat="server" GroupName="cm_q9" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm9" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Attendance Logbook with summary</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm10_q1" runat="server" GroupName="cm_q10" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm10_q2" runat="server" GroupName="cm_q10" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm10" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Weekly Center Discussion tool (Life series book 2)</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_cm11_q1" runat="server" GroupName="cm_q11" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_cm11_q2" runat="server" GroupName="cm_q11" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtcm11" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--PAYMENT--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno5" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            Payment
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>100% weekly  collection before the meeting ended</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_pay1_q1" runat="server" GroupName="pay_q1" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_pay1_q2" runat="server" GroupName="pay_q1" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtpay1" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>The center kept the second copy of ECR in the center, attached with the OR issued by the office</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_pay2_q1" runat="server" GroupName="pay_q2" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_pay2_q2" runat="server" GroupName="pay_q2" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtpay2" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Individual Passbook is presented during payment and updated</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_pay3_q1" runat="server" GroupName="pay_q3" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_pay3_q2" runat="server" GroupName="pay_q3" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtpay3" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Program member/s deposited the collection  </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="rd_pay4_q1" runat="server" GroupName="pay_q4" Text="Yes"   />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="rd_pay4_q2" runat="server" GroupName="pay_q4" Text="No"   />
                                                </label>
                                                <asp:TextBox ID="txtpay4" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <%--COMMENTS--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Nav1" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            Comments/Notes (Optional)
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group col-sm-8">
                                    <asp:TextBox ID="txtComments" TextMode="MultiLine" style="resize:none" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--RATINGS EQUIVALENT--%>
                     <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno6" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            <span class="glyphicon glyphicon-list-alt"></span>
                                            EQUIVALENT RATING
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <asp:Button ID="btnViewScore" runat="server" Text="View Score" CssClass ="btn btn-success" Font-Size="Large" />
                                          
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                           <button class="btn btn-primary" type="button">
                                              Score    <span class="badge"><h5><asp:Label ID="lblTotalScore" runat="server" ></asp:Label></h5></span>
                                            </button>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                           <button class="btn btn-primary" type="button">
                                              Rating   <span class="badge"><h5><asp:Label ID="lblRating" runat="server" ></asp:Label></h5></span>
                                            </button>
                                        </div>
                                        <div class="col-sm-3">
                                              <h3><asp:Label ID="lblEquivalent" runat="server" cssclass="label label-danger"></asp:Label></h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="panel-footer clearfix ">
                        <div class="btn-group btn-group-justified" role="group" >
                            <div class="btn-group" role="group">
                                <asp:Button ID="cmdsave" runat="server" Text="Save Changes" CssClass="btn btn-success" />
                                <asp:ConfirmButtonExtender ID="confirmExtender" runat="server" ConfirmText="Are you sure you want to save?" TargetControlID="cmdSave" >
                                </asp:ConfirmButtonExtender>
                            </div>
                        </div>
                        <CR:CrystalReportViewer ID="CRV" runat="server" AutoDataBind="true" Visible="false" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade"  tabindex="-1" id="modMessageBox" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<asp:UpdateProgress ID="pnlUpdateProgress" runat="server">
        <ProgressTemplate>
            <div style="background-color: Gray; 
                filter:alpha(opacity=60); 
                opacity:0.60; 
                width: 100%; 
                top: 0; 
                left: 0; 
                position: fixed; 
                height: 100%; 
                z-index: 100001">
            </div>
            <div style="filter: alpha(opacity=100);
                opacity: 1;
                position:  fixed;
                top: 50%;
                left: 50%;
                margin: -50px 0px 0px -25px;
                z-index: 100001">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

<!--SESSIONS-->
    <div> 
        <asp:Label ID="lblfullname" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblposition" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchcode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchdesc" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblsearch" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblevalid" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblconductedby" runat="server" Visible="False"></asp:Label>
    </div>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            jQuery(function ($) {
                $("#txtdate").datepicker({ maxDate: '0' });
            });

        });

        jQuery(function ($) {
            $("#txtdate").datepicker({ maxDate: '0' });
        });

    </script>
</asp:Content>
