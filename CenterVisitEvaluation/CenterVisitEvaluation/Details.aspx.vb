﻿Imports System.Data.SqlClient
Public Class Details
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")
            Me.lblevalid.Text = Session("evalid")

            If Not IsPostBack Then
                RetrieveEval()
                GEt_score()
                ViewOrEdit()
            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub ViewOrEdit()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select conducted_by from evaluation where eval_id = '" + lblevalid.Text + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            While dr.Read
                Me.lblconductedby.Text = dr("conducted_by").ToString
            End While



            If lblconductedby.Text <> lblcurrentuser.Text Then
                txtdate.ReadOnly = True
                txtMax.ReadOnly = True
                txtActual.ReadOnly = True
                rd_att1_q1.Enabled = False
                rd_att1_q2.Enabled = False
                rd_att2_q1.Enabled = False
                rd_att2_q2.Enabled = False
                rd_c1_q1.Enabled = False
                rd_c1_q2.Enabled = False
                rd_c2_q1.Enabled = False
                rd_c2_q2.Enabled = False
                rd_c3_q1.Enabled = False
                rd_c3_q2.Enabled = False
                rd_c4_q1.Enabled = False
                rd_c4_q2.Enabled = False
                rd_c5_q1.Enabled = False
                rd_c5_q2.Enabled = False
                rd_c6_q1.Enabled = False
                rd_c6_q2.Enabled = False
                rd_cm1_q1.Enabled = False
                rd_cm1_q2.Enabled = False
                rd_cm2_q1.Enabled = False
                rd_cm2_q2.Enabled = False
                rd_cm3_q1.Enabled = False
                rd_cm3_q2.Enabled = False
                rd_cm4_q1.Enabled = False
                rd_cm4_q2.Enabled = False
                rd_cm5_q1.Enabled = False
                rd_cm5_q2.Enabled = False
                rd_cm6_q1.Enabled = False
                rd_cm6_q2.Enabled = False
                rd_cm7_q1.Enabled = False
                rd_cm7_q2.Enabled = False
                rd_cm8_q1.Enabled = False
                rd_cm8_q2.Enabled = False
                rd_cm9_q1.Enabled = False
                rd_cm9_q2.Enabled = False
                rd_cm10_q1.Enabled = False
                rd_cm10_q2.Enabled = False
                rd_cm11_q1.Enabled = False
                rd_cm11_q2.Enabled = False
                rd_pay1_q1.Enabled = False
                rd_pay1_q2.Enabled = False
                rd_pay2_q1.Enabled = False
                rd_pay2_q2.Enabled = False
                rd_pay3_q1.Enabled = False
                rd_pay3_q2.Enabled = False
                rd_pay4_q1.Enabled = False
                rd_pay4_q2.Enabled = False
                rd_po1_q1.Enabled = False
                rd_po1_q2.Enabled = False
                rd_po2_q1.Enabled = False
                rd_po2_q2.Enabled = False
                rd_po3_q1.Enabled = False
                rd_po3_q2.Enabled = False
                txtComments.ReadOnly = True
                cmdsave.Visible = False
                btnViewScore.Visible = False

            End If

            upPnlEval.Update()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub RetrieveEval()
        Try

            If con.State = 1 Then con.Close()
            con.Open()
            Dim sqlQuery As String = "Select *, isnull(att_max,0) as max, isnull(att_actual,0) as actual from evaluation where eval_id = '" + lblevalid.Text + "'"

            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader = cmd.ExecuteReader()

            If dr.Read() Then
                If dr("edited_date").Equals(DBNull.Value) Then
                    Me.lblEdited.Visible = False
                Else
                    Me.lblEdited.Visible = True
                    Me.lblEdited.Text = "Last Edited: " + dr("edited_date").ToString
                End If

                Me.txtdate.Text = CDate(dr("conducted_date").ToString)
                Me.txtbranch.Text = Trim(dr("branch_name").ToString)
                Me.txtCenter.Text = Trim(dr("center_code").ToString)
                Me.txtaddress.Text = Trim(dr("center_address").ToString)
                Me.txtProgramOfficer.Text = Trim(dr("pa").ToString)
                Me.txtMax.Text = CInt(dr("max"))
                Me.txtActual.Text = CInt(dr("actual"))

                Me.txtpo1.Text = CInt(dr("pa_arrived"))
                If txtpo1.Text = 0 Then rd_po1_q1.Checked = False Else rd_po1_q1.Checked = True
                If txtpo1.Text = 0 Then rd_po1_q2.Checked = True Else rd_po1_q2.Checked = False

                Me.txtpo2.Text = CInt(dr("pa_deliquent"))
                If txtpo2.Text = 0 Then rd_po2_q1.Checked = False Else rd_po2_q1.Checked = True
                If txtpo2.Text = 0 Then rd_po2_q2.Checked = True Else rd_po2_q2.Checked = False

                Me.txtpo3.Text = CInt(dr("pa_discuss"))
                If txtpo3.Text = 0 Then rd_po3_q1.Checked = False Else rd_po3_q1.Checked = True
                If txtpo3.Text = 0 Then rd_po3_q2.Checked = True Else rd_po3_q2.Checked = False

                Me.txtc1.Text = CInt(dr("ctr_follow"))
                If txtc1.Text = 0 Then rd_c1_q1.Checked = False Else rd_c1_q1.Checked = True
                If txtc1.Text = 0 Then rd_c1_q2.Checked = True Else rd_c1_q2.Checked = False

                Me.txtc2.Text = CInt(dr("ctr_start"))
                If txtc2.Text = 0 Then rd_c2_q1.Checked = False Else rd_c2_q1.Checked = True
                If txtc2.Text = 0 Then rd_c2_q2.Checked = True Else rd_c2_q2.Checked = False

                Me.txtc3.Text = CInt(dr("ctr_provide"))
                If txtc3.Text = 0 Then rd_c3_q1.Checked = False Else rd_c3_q1.Checked = True
                If txtc3.Text = 0 Then rd_c3_q2.Checked = True Else rd_c3_q2.Checked = False

                Me.txtc4.Text = CInt(dr("ctr_trans"))
                If txtc4.Text = 0 Then rd_c4_q1.Checked = False Else rd_c4_q1.Checked = True
                If txtc4.Text = 0 Then rd_c4_q2.Checked = True Else rd_c4_q2.Checked = False

                Me.txtc5.Text = CInt(dr("ctr_visual"))
                If txtc5.Text = 0 Then rd_c5_q1.Checked = False Else rd_c5_q1.Checked = True
                If txtc5.Text = 0 Then rd_c5_q2.Checked = True Else rd_c5_q2.Checked = False

                Me.txtc6.Text = CInt(dr("ctr_minutes"))
                If txtc6.Text = 0 Then rd_c6_q1.Checked = False Else rd_c6_q1.Checked = True
                If txtc6.Text = 0 Then rd_c6_q2.Checked = True Else rd_c6_q2.Checked = False

                Me.txtatt1.Text = CInt(dr("att_min"))
                If txtatt1.Text = 0 Then rd_att1_q1.Checked = False Else rd_att1_q1.Checked = True
                If txtatt1.Text = 0 Then rd_att1_q2.Checked = True Else rd_att1_q2.Checked = False

                Me.txtatt2.Text = CInt(dr("att_pm"))
                If txtatt2.Text = 0 Then rd_att2_q1.Checked = False Else rd_att2_q1.Checked = True
                If txtatt2.Text = 0 Then rd_att2_q2.Checked = True Else rd_att2_q2.Checked = False

                Me.txtcm1.Text = CInt(dr("mat_house"))
                If txtcm1.Text = 0 Then rd_cm1_q1.Checked = False Else rd_cm1_q1.Checked = True
                If txtcm1.Text = 0 Then rd_cm1_q2.Checked = True Else rd_cm1_q2.Checked = False

                Me.txtcm2.Text = CInt(dr("mat_sign"))
                If txtcm2.Text = 0 Then rd_cm2_q1.Checked = False Else rd_cm2_q1.Checked = True
                If txtcm2.Text = 0 Then rd_cm2_q2.Checked = True Else rd_cm2_q2.Checked = False

                Me.txtcm3.Text = CInt(dr("mat_pitong"))
                If txtcm3.Text = 0 Then rd_cm3_q1.Checked = False Else rd_cm3_q1.Checked = True
                If txtcm3.Text = 0 Then rd_cm3_q2.Checked = True Else rd_cm3_q2.Checked = False

                Me.txtcm4.Text = CInt(dr("mat_pang"))
                If txtcm4.Text = 0 Then rd_cm4_q1.Checked = False Else rd_cm4_q1.Checked = True
                If txtcm4.Text = 0 Then rd_cm4_q2.Checked = True Else rd_cm4_q2.Checked = False

                Me.txtcm5.Text = CInt(dr("mat_org"))
                If txtcm5.Text = 0 Then rd_cm5_q1.Checked = False Else rd_cm5_q1.Checked = True
                If txtcm5.Text = 0 Then rd_cm5_q2.Checked = True Else rd_cm5_q2.Checked = False

                Me.txtcm6.Text = CInt(dr("mat_column"))
                If txtcm6.Text = 0 Then rd_cm6_q1.Checked = False Else rd_cm6_q1.Checked = True
                If txtcm6.Text = 0 Then rd_cm6_q2.Checked = True Else rd_cm6_q2.Checked = False

                Me.txtcm7.Text = CInt(dr("mat_black"))
                If txtcm7.Text = 0 Then rd_cm7_q1.Checked = False Else rd_cm7_q1.Checked = True
                If txtcm7.Text = 0 Then rd_cm7_q2.Checked = True Else rd_cm7_q2.Checked = False

                Me.txtcm8.Text = CInt(dr("mat_wall"))
                If txtcm8.Text = 0 Then rd_cm8_q1.Checked = False Else rd_cm8_q1.Checked = True
                If txtcm8.Text = 0 Then rd_cm8_q2.Checked = True Else rd_cm8_q2.Checked = False

                Me.txtcm9.Text = CInt(dr("mat_center"))
                If txtcm9.Text = 0 Then rd_cm9_q1.Checked = False Else rd_cm9_q1.Checked = True
                If txtcm9.Text = 0 Then rd_cm9_q2.Checked = True Else rd_cm9_q2.Checked = False

                Me.txtcm10.Text = CInt(dr("mat_attendance"))
                If txtcm10.Text = 0 Then rd_cm10_q1.Checked = False Else rd_cm10_q1.Checked = True
                If txtcm10.Text = 0 Then rd_cm10_q2.Checked = True Else rd_cm10_q2.Checked = False

                Me.txtcm11.Text = CInt(dr("mat_weekly"))
                If txtcm11.Text = 0 Then rd_cm11_q1.Checked = False Else rd_cm11_q1.Checked = True
                If txtcm11.Text = 0 Then rd_cm11_q2.Checked = True Else rd_cm11_q2.Checked = False

                Me.txtpay1.Text = CInt(dr("pay_weekly"))
                If txtpay1.Text = 0 Then rd_pay1_q1.Checked = False Else rd_pay1_q1.Checked = True
                If txtpay1.Text = 0 Then rd_pay1_q2.Checked = True Else rd_pay1_q2.Checked = False

                Me.txtpay2.Text = CInt(dr("pay_center"))
                If txtpay2.Text = 0 Then rd_pay2_q1.Checked = False Else rd_pay2_q1.Checked = True
                If txtpay2.Text = 0 Then rd_pay2_q2.Checked = True Else rd_pay2_q2.Checked = False

                Me.txtpay3.Text = CInt(dr("pay_indiv"))
                If txtpay3.Text = 0 Then rd_pay3_q1.Checked = False Else rd_pay3_q1.Checked = True
                If txtpay3.Text = 0 Then rd_pay3_q2.Checked = True Else rd_pay3_q2.Checked = False

                Me.txtpay4.Text = CInt(dr("pay_pm"))
                If txtpay4.Text = 0 Then rd_pay4_q1.Checked = False Else rd_pay4_q1.Checked = True
                If txtpay4.Text = 0 Then rd_pay4_q2.Checked = True Else rd_pay4_q2.Checked = False

                Me.txtComments.Text = dr("comments").ToString()

                Me.lblTotalScore.Text = CInt(dr("total_score"))
                Me.lblRating.Text = CDbl(dr("compliance"))
                Me.lblEquivalent.Text = Trim(dr("equivalent_rating"))
            End If

                dr.Close()
                cmd.Dispose()
                upPnlEval.Update()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub GEt_score()
        Dim totalscore As Integer
        totalscore = Val(Me.txtpo1.Text) + Val(Me.txtpo2.Text) + Val(Me.txtpo3.Text) _
            + Val(Me.txtc1.Text) + Val(Me.txtc2.Text) + Val(Me.txtc3.Text) + Val(Me.txtc4.Text) + Val(Me.txtc5.Text) + Val(Me.txtc6.Text) _
            + Val(Me.txtatt1.Text) + Val(Me.txtatt2.Text) _
            + Val(Me.txtcm1.Text) + Val(Me.txtcm2.Text) + Val(Me.txtcm3.Text) + Val(Me.txtcm4.Text) + Val(Me.txtcm5.Text) + Val(Me.txtcm6.Text) + Val(Me.txtcm7.Text) + Val(Me.txtcm8.Text) + Val(Me.txtcm9.Text) + Val(Me.txtcm10.Text) + Val(Me.txtcm11.Text) _
            + Val(Me.txtpay1.Text) + Val(Me.txtpay2.Text) + Val(Me.txtpay3.Text) + Val(Me.txtpay4.Text)

        Me.lblTotalScore.Text = totalscore

        Dim rating As Double
        Dim equivalent As String

        rating = FormatNumber((totalscore / 30) * 100, 2)
        'equivalent = rating & "%"
        If rating >= 90 Then
            equivalent = "VERY GOOD"
        ElseIf rating >= 80 And rating <= 89 Then
            equivalent = "GOOD"
        ElseIf rating >= 70 And rating <= 79 Then
            equivalent = "POOR"
        Else
            equivalent = "TOTALLY INEFFECTIVE"
        End If

        Me.lblRating.Text = rating
        Me.lblEquivalent.Text = equivalent
        upPnlEval.Update()
    End Sub

    Protected Sub rd_po1_q1_CheckedChanged(sender As Object, e As EventArgs) Handles rd_po1_q1.CheckedChanged
        If Me.rd_po1_q1.Checked = True Then Me.txtpo1.Text = "1" Else Me.txtpo1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po1_q2.CheckedChanged
        If Me.rd_po1_q2.Checked = True Then Me.txtpo1.Text = "0" Else Me.txtpo1.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po2_q1.CheckedChanged
        If Me.rd_po2_q1.Checked = True Then Me.txtpo2.Text = "1" Else Me.txtpo2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po2_q2.CheckedChanged
        If Me.rd_po2_q2.Checked = True Then Me.txtpo2.Text = "0" Else Me.txtpo2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po3_q1.CheckedChanged
        If Me.rd_po3_q1.Checked = True Then Me.txtpo3.Text = "1" Else Me.txtpo3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_po3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_po3_q2.CheckedChanged
        If Me.rd_po3_q2.Checked = True Then Me.txtpo3.Text = "0" Else Me.txtpo3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c1_q1.CheckedChanged
        If Me.rd_c1_q1.Checked = True Then Me.txtc1.Text = "1" Else Me.txtc1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c1_q2.CheckedChanged
        If Me.rd_c1_q2.Checked = True Then Me.txtc1.Text = "0" Else Me.txtc1.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c2_q1.CheckedChanged
        If Me.rd_c2_q1.Checked = True Then Me.txtc2.Text = "1" Else Me.txtc2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c2_q2.CheckedChanged
        If Me.rd_c2_q2.Checked = True Then Me.txtc2.Text = "0" Else Me.txtc2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c3_q1.CheckedChanged
        If Me.rd_c3_q1.Checked = True Then Me.txtc3.Text = "1" Else Me.txtc3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c3_q2.CheckedChanged
        If Me.rd_c3_q2.Checked = True Then Me.txtc3.Text = "0" Else Me.txtc3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c4_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c4_q1.CheckedChanged
        If Me.rd_c4_q1.Checked = True Then Me.txtc4.Text = "2" Else Me.txtc4.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c4_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c4_q2.CheckedChanged
        If Me.rd_c4_q2.Checked = True Then Me.txtc4.Text = "0" Else Me.txtc4.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c5_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c5_q1.CheckedChanged
        If Me.rd_c5_q1.Checked = True Then Me.txtc5.Text = "1" Else Me.txtc5.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c5_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c5_q2.CheckedChanged
        If Me.rd_c5_q2.Checked = True Then Me.txtc5.Text = "0" Else Me.txtc5.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c6_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c6_q1.CheckedChanged
        If Me.rd_c6_q1.Checked = True Then Me.txtc6.Text = "1" Else Me.txtc6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_c6_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_c6_q2.CheckedChanged
        If Me.rd_c6_q2.Checked = True Then Me.txtc6.Text = "0" Else Me.txtc6.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att1_q1.CheckedChanged
        If Me.rd_att1_q1.Checked = True Then Me.txtatt1.Text = "2" Else Me.txtc6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att1_q2.CheckedChanged
        If Me.rd_att1_q2.Checked = True Then Me.txtatt1.Text = "0" Else Me.txtatt1.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att2_q1.CheckedChanged
        If Me.rd_att2_q1.Checked = True Then Me.txtatt2.Text = "1" Else Me.txtatt2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_att2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_att2_q2.CheckedChanged
        If Me.rd_att2_q2.Checked = True Then Me.txtatt2.Text = "0" Else Me.txtatt2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm1_q1.CheckedChanged
        If Me.rd_cm1_q1.Checked = True Then Me.txtcm1.Text = "1" Else Me.txtcm1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm1_q2.CheckedChanged
        If Me.rd_cm1_q2.Checked = True Then Me.txtcm1.Text = "0" Else Me.txtcm1.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm2_q1.CheckedChanged
        If Me.rd_cm2_q1.Checked = True Then Me.txtcm2.Text = "1" Else Me.txtcm2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm2_q2.CheckedChanged
        If Me.rd_cm2_q2.Checked = True Then Me.txtcm2.Text = "0" Else Me.txtcm2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm3_q1.CheckedChanged
        If Me.rd_cm3_q1.Checked = True Then Me.txtcm3.Text = "1" Else Me.txtcm3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm3_q2.CheckedChanged
        If Me.rd_cm3_q2.Checked = True Then Me.txtcm3.Text = "0" Else Me.txtcm3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm4_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm4_q1.CheckedChanged
        If Me.rd_cm4_q1.Checked = True Then Me.txtcm4.Text = "1" Else Me.txtcm4.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm4_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm4_q2.CheckedChanged
        If Me.rd_cm4_q2.Checked = True Then Me.txtcm4.Text = "0" Else Me.txtcm4.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm5_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm5_q1.CheckedChanged
        If Me.rd_cm5_q1.Checked = True Then Me.txtcm5.Text = "1" Else Me.txtcm5.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm5_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm5_q2.CheckedChanged
        If Me.rd_cm5_q2.Checked = True Then Me.txtcm5.Text = "0" Else Me.txtcm5.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm6_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm6_q1.CheckedChanged
        If Me.rd_cm6_q1.Checked = True Then Me.txtcm6.Text = "1" Else Me.txtcm6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm6_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm6_q2.CheckedChanged
        If Me.rd_cm6_q2.Checked = True Then Me.txtcm6.Text = "0" Else Me.txtcm6.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm7_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm7_q1.CheckedChanged
        If Me.rd_cm7_q1.Checked = True Then Me.txtcm7.Text = "1" Else Me.txtcm6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm7_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm7_q2.CheckedChanged
        If Me.rd_cm7_q2.Checked = True Then Me.txtcm7.Text = "0" Else Me.txtcm7.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm8_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm8_q1.CheckedChanged
        If Me.rd_cm8_q1.Checked = True Then Me.txtcm8.Text = "1" Else Me.txtcm8.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm8_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm8_q2.CheckedChanged
        If Me.rd_cm8_q2.Checked = True Then Me.txtcm8.Text = "0" Else Me.txtcm8.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm9_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm9_q1.CheckedChanged
        If Me.rd_cm9_q1.Checked = True Then Me.txtcm9.Text = "1" Else Me.txtcm9.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm9_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm9_q2.CheckedChanged
        If Me.rd_cm9_q2.Checked = True Then Me.txtcm9.Text = "0" Else Me.txtcm9.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm10_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm10_q1.CheckedChanged
        If Me.rd_cm10_q1.Checked = True Then Me.txtcm10.Text = "2" Else Me.txtcm10.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm10_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm10_q2.CheckedChanged
        If Me.rd_cm10_q2.Checked = True Then Me.txtcm10.Text = "0" Else Me.txtcm10.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm11_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm11_q1.CheckedChanged
        If Me.rd_cm11_q1.Checked = True Then Me.txtcm11.Text = "1" Else Me.txtcm11.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_cm11_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_cm11_q2.CheckedChanged
        If Me.rd_cm11_q2.Checked = True Then Me.txtcm11.Text = "0" Else Me.txtcm11.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay1_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay1_q1.CheckedChanged
        If Me.rd_pay1_q1.Checked = True Then Me.txtpay1.Text = "2" Else Me.txtpay1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay1_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay1_q2.CheckedChanged
        If Me.rd_pay1_q2.Checked = True Then Me.txtpay1.Text = "0" Else Me.txtpay1.Text = "2"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay2_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay2_q1.CheckedChanged
        If Me.rd_pay2_q1.Checked = True Then Me.txtpay2.Text = "1" Else Me.txtpay2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay2_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay2_q2.CheckedChanged
        If Me.rd_pay2_q2.Checked = True Then Me.txtpay2.Text = "0" Else Me.txtpay2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay3_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay3_q1.CheckedChanged
        If Me.rd_pay3_q1.Checked = True Then Me.txtpay3.Text = "1" Else Me.txtpay3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay3_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay3_q2.CheckedChanged
        If Me.rd_pay3_q2.Checked = True Then Me.txtpay3.Text = "0" Else Me.txtpay3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay4_q1_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay4_q1.CheckedChanged
        If Me.rd_pay4_q1.Checked = True Then Me.txtpay4.Text = "1" Else Me.txtpay4.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub rd_pay4_q2_CheckedChanged(sender As Object, e As System.EventArgs) Handles rd_pay4_q2.CheckedChanged
        If Me.rd_pay4_q2.Checked = True Then Me.txtpay4.Text = "0" Else Me.txtpay4.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub


    Private Sub btnViewScore_Click(sender As Object, e As System.EventArgs) Handles btnViewScore.Click
        GEt_score()
        upPnlEval.Update()
    End Sub

    Protected Sub cmdsave_Click(sender As Object, e As EventArgs) Handles cmdsave.Click
        If txtActual.Text = "" Or txtMax.Text = "" Then
            Prompt("Please input number of members.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        ElseIf CInt(Me.txtMax.Text) < CInt(Me.txtActual.Text) Then
            Prompt("Number of clients present must not be less than total active members.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Else

            Try
                GEt_score()

                Dim evalid As Integer = CInt(Me.lblevalid.Text)
                Dim conducteddate As Date = CDate(Me.txtdate.Text)
                Dim paarrived As Integer = CInt(Me.txtpo1.Text)
                Dim padeliquent As Integer = CInt(Me.txtpo2.Text)
                Dim padiscuss As Integer = CInt(Me.txtpo3.Text)
                Dim ctrfollow As Integer = CInt(Me.txtc1.Text)
                Dim ctrstart As Integer = CInt(Me.txtc2.Text)
                Dim ctrprovide As Integer = CInt(Me.txtc3.Text)
                Dim ctrtrans As Integer = CInt(Me.txtc4.Text)
                Dim ctrisual As Integer = CInt(Me.txtc5.Text)
                Dim ctrminutes As Integer = CInt(Me.txtc6.Text)
                Dim attmax As Integer = CInt(Me.txtMax.Text)
                Dim attactual As Integer = CInt(Me.txtActual.Text)
                Dim attmin As Integer = CInt(Me.txtatt1.Text)
                Dim attpm As Integer = CInt(Me.txtatt2.Text)
                Dim mathouse As Integer = CInt(Me.txtcm1.Text)
                Dim matsign As Integer = CInt(Me.txtcm2.Text)
                Dim matpitong As Integer = CInt(Me.txtcm3.Text)
                Dim matpang As Integer = CInt(Me.txtcm4.Text)
                Dim matorg As Integer = CInt(Me.txtcm5.Text)
                Dim matcolumn As Integer = CInt(Me.txtcm6.Text)
                Dim matblack As Integer = CInt(Me.txtcm7.Text)
                Dim matwall As Integer = CInt(Me.txtcm8.Text)
                Dim matcenter As Integer = CInt(Me.txtcm9.Text)
                Dim matattendance As Integer = CInt(Me.txtcm10.Text)
                Dim matweekly As Integer = CInt(Me.txtcm11.Text)
                Dim payweekly As Integer = CInt(Me.txtpay1.Text)
                Dim paycenter As Integer = CInt(Me.txtpay2.Text)
                Dim payindiv As Integer = CInt(Me.txtpay3.Text)
                Dim paypm As Integer = CInt(Me.txtpay4.Text)
                Dim comments As String = Me.txtComments.Text.Replace("'", "''")
                Dim totalscore As Integer = CInt(Me.lblTotalScore.Text)
                Dim compliance As Decimal = CDbl(Me.lblRating.Text)
                Dim rating As String = Trim(Me.lblEquivalent.Text)

                If con.State = 1 Then con.Close()
                con.Open()
                Dim sqlQuery As String = ""


                sqlQuery = "OPS_update_eval " & _
                                    "" & evalid & ", " & _
                                    "'" & conducteddate.ToString & "', " & _
                                    "'" & paarrived & "'," & _
                                    "'" & padeliquent & "'," & _
                                    "'" & padiscuss & "'," & _
                                    "'" & ctrfollow & "'," & _
                                    "'" & ctrstart & "'," & _
                                    "'" & ctrprovide & "'," & _
                                    "'" & ctrtrans & "'," & _
                                    "'" & ctrisual & "'," & _
                                    "'" & ctrminutes & "'," & _
                                    "'" & attmax & "'," & _
                                    "'" & attactual & "'," & _
                                    "'" & attmin & "'," & _
                                    "'" & attpm & "'," & _
                                    "'" & mathouse & "'," & _
                                    "'" & matsign & "'," & _
                                    "'" & matpitong & "'," & _
                                    "'" & matpang & "'," & _
                                    "'" & matorg & "'," & _
                                    "'" & matcolumn & "'," & _
                                    "'" & matblack & "'," & _
                                    "'" & matwall & "'," & _
                                    "'" & matcenter & "'," & _
                                    "'" & matattendance & "'," & _
                                    "'" & matweekly & "'," & _
                                    "'" & payweekly & "'," & _
                                    "'" & paycenter & "'," & _
                                    "'" & payindiv & "'," & _
                                    "'" & paypm & "'," & _
                                    "'" & comments & "'," & _
                                    "'" & totalscore & "'," & _
                                    "'" & compliance & "'," & _
                                    "'" & rating & "'"

                Dim cmd As New SqlCommand(sqlQuery, con)
                cmd.ExecuteNonQuery()
                Prompt("Successfully Updated", Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
                upPnlEval.Update()

            Catch ex As Exception
                Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            Finally
                con.Close()
            End Try

        End If

    End Sub
End Class