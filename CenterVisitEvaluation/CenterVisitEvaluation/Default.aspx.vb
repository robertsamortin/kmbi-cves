﻿Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                'BIND_Region()
                'BIND_Area()
                'BIND_Branch()
                'BIND_Unit()
                'BIND_PO()
                'Me.txtDateFrom.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
                'Me.txtDateTo.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    'Private Sub lnkCves_Click(sender As Object, e As EventArgs) Handles lnkCves.Click
    '    Session("branchcode") = Me.lblbranchcode.Text
    '    Session("currentuser") = Me.lblcurrentuser.Text
    '    Session("accesslevel") = Me.lblaccesslevel.Text
    '    Session("areacode") = Me.lblareacode.Text
    '    Me.lnkCves.PostBackUrl = "cves.aspx"
    'End Sub

    'Private Sub lnkAras_Click(sender As Object, e As EventArgs) Handles lnkAras.Click
    '    Session("branchcode") = Me.lblbranchcode.Text
    '    Session("currentuser") = Me.lblcurrentuser.Text
    '    Session("accesslevel") = Me.lblaccesslevel.Text
    '    Session("areacode") = Me.lblareacode.Text
    '    Me.lnkAras.PostBackUrl = "aras/pages/Default.aspx"
    'End Sub
End Class