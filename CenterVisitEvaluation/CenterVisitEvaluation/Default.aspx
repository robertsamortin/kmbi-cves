﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="CenterVisitEvaluation._Default" %>

<!DOCTYPE html>

<html lang="en">
  <head>
  <meta charset="UTF-8">
  <title>Document</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    .split {
      height: 100%;
      width: 50%;
      position: fixed;
      z-index: 1;
      top: 0;
      overflow: hidden;
      padding-top: 20px;
      color: white;
    }

    .left {
      left: 0;
      background-color: #775ba4;
    }

    .right {
      right: 0;
      background-color: #467763;
    }

    .centered {
      position: relative;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -40%);
      text-align: center;
    }

    a.topnav-icons.fa-menu {
      display: none;
    }

    .myLink {
      text-decoration:none;
      color:white !important;
      opacity:0.8;
    }
    .myLink:hover {
      opacity: 1 
    }

    .myLink:hover h1 {
      font-size: 41px;
    }

    .mySpan {
      border-radius: 7%;
      border: 2px solid white;
      padding: 20px 35px;
      display: inline-block;
      font-size: 105px;
    }

    .myh1 {
      line-height: 90px;
      font-size: 20px;
    }
    .myP {
      font-size: 15px;
      max-width: 370px;
      padding-left: 20px;
      padding-right: 20px;
      margin: auto;
    }

    @media only screen and (max-width:993px) {
      .centered {
        margin-top: 40px;
      }
      .myLink .mySpan {
        font-size: 70px;
        padding: 10px 25px;
      }
      .myLink .myh1 {
        font-size: 25px;
      }
      .myLink:hover h1 {
        font-size: 26px;
      }
    }

    @media only screen and (max-width: 700px) {
      .centered {
        margin-top: 50px;
      }
      .myLink .mySpan {
        font-size: 40px;
        padding: 10px 25px;
      }
      .myLink .myh1 {
        font-size: 16px;
      }
      .myP {
        display: none;
      }
      .myLink:hover h1 {
        font-size: 17px;
      }
    }

    @media only screen and (max-height: 620px) {
      .centered {
        margin-top: 45px;
      }
      .myLink .mySpan {
        font-size: 25px;
        padding: 10px 25px;
      }
      .myLink .myh1 {
        font-size: 16px;
      }
      .myP {
        display: none
      }
      .myLink:hover h1 {
        font-size: 17px;
      }
    }

    @media only screen and (max-width: 550px) {
      .centered {
        margin-top: 40px;
      }
      .myLink .mySpan {
        font-size: 25px;
        padding: 10px 25px;
      }
      .myLink .myh1 {
        font-size: 16px;
      }
      .myP {
        display: none
      }
      .myLink:hover h1 {
        font-size: 17px;
      }
    }
  </style>
</head>
<body>
<form id="Form1" runat="server">
    <div class="split left">
    <div class="centered">
        <asp:LinkButton ID="lnkCves" runat="server" CssClass="myLink" PostBackUrl ="~/Cves.aspx">
            <div style="display:inline-block">
          <%--<span class="mySpan">CVES</span>--%>
            <img src="aras/img/evals.png" alt="" width="200px">
          <h4 class="myh1">Center Visit Evaluation System</h4>
        </div>
        </asp:LinkButton>
      
    </div>
  </div>
    
  <div class="split right">
    <div class="centered">
        <asp:LinkButton ID="lnkAras" CssClass="myLink" runat="server" PostBackUrl ="~/aras/pages/Default.aspx">
            <div style="display:inline-block">
          <%--<span class="mySpan">ARAS</span>--%>
            <img src="aras/img/accountability.png" alt="" width ="200px">
          <h4 class="myh1">Accountability and Responsibility Assessment System</h4>
        </div>
        </asp:LinkButton>
      
    </div>
  </div>
    <div>
        <asp:Label ID="lblbranchcode" runat="server" Text="Label" Visible="false"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Text="Label" Visible="false"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Text="Label" Visible="false"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Text="Label" Visible="false"></asp:Label>
    </div> 
</form>
  
</body>
</html>
