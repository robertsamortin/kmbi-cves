﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class CenterDetails
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblCenterName.Text = Session("centercode")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                BIND_centerhistory()
                Me.txtDateFrom.Text = FormatDateTime("1/1/2016", DateFormat.ShortDate)
                Me.txtDateTo.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub grdCenterList_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCenterList.PageIndexChanging
        grdCenterList.PageIndex = e.NewPageIndex
        grdCenterList.DataSource = Session("CenterDetails")
        grdCenterList.DataBind()
    End Sub

    Protected Sub grdCenterList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCenterList.RowCommand
        Dim evalid As String = e.CommandArgument
        Try
            If e.CommandName = "Centers" Then
                Session("evalid") = evalid
                UpdatePanel1.Update()
                ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Details.aspx?id=" + evalid + "', target='_blank' );", True)


            End If
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub grdEvaluatorList_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCenterList.Sorting
        Dim sortingDirection As String = String.Empty
        If direction = SortDirection.AscendingOrder Then
            direction = SortDirection.DescendingOrder
            sortingDirection = "Desc"
        Else
            direction = SortDirection.AscendingOrder
            sortingDirection = "Asc"
        End If
        grdCenterList.SetPageIndex(0)

        Dim dv As DataView = Session("CenterDetails")
        dv.Sort = e.SortExpression + " " + sortingDirection
        Session("CenterDetails") = dv
        grdCenterList.DataSource = dv
        grdCenterList.DataBind()
    End Sub

    Private Sub BIND_centerhistory()
        Try

            Dim dv As New DataView(TryCast(GetDocument("ops_center_history '" & Trim(Me.lblCenterName.Text) & "', '',''").Tables(0), DataTable))

            dv.Sort = "conducted_date ASC"
            Session("CenterDetails") = dv
            grdCenterList.DataSource = dv
            grdCenterList.DataBind()

            lblCount.Text = "Times Evaluated: " + grdCenterList.Rows.Count.ToString
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Try

            Dim dv As New DataView(TryCast(GetDocument("ops_center_history '" & Trim(Me.lblCenterName.Text) & "', '" + txtDateFrom.Text + "','" + txtDateTo.Text + "'").Tables(0), DataTable))

            dv.Sort = "conducted_date ASC"
            Session("CenterDetails") = dv
            grdCenterList.DataSource = dv
            grdCenterList.DataBind()

            If grdCenterList.Rows.Count = 0 Then
                Me.lblNoRecords.Text = "NO RECORDS FOUND!"
                lblCount.Visible = False
            Else
                Me.lblNoRecords.Text = ""
                lblCount.Text = "Times Evaluated: " + grdCenterList.Rows.Count.ToString
                lblCount.Visible = True
            End If

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim cryRpt As New ReportDocument
            cryRpt.Load(Server.MapPath("report/cves_center_history.rpt"))

            Dim sqlQuery As String = "OPS_center_history"
            Dim cmd As New SqlCommand(sqlQuery, con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Clear()
            cmd.Parameters.AddWithValue("center", lblCenterName.Text)
            cmd.Parameters.AddWithValue("datefrom", txtDateFrom.Text)
            cmd.Parameters.AddWithValue("dateto", txtDateTo.Text)
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            'MsgBox(Server.MapPath("reports/cvef_revised.rpt"))
            Dim dt As New DataTable
            da.Fill(dt)

            cryRpt.SetDataSource(dt)
            'CRV.DataBind()

            Me.CRV.ReportSource = cryRpt

            Dim Filename As String = "report/Centerhistory" & lblCenterName.Text & ".pdf"
            If System.IO.File.Exists(Server.MapPath(Filename)) = True Then
                System.IO.File.Delete(Server.MapPath(Filename))
            End If
            Dim oDiskOpts As New DiskFileDestinationOptions
            cryRpt.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
            cryRpt.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat
            oDiskOpts.DiskFileName = Server.MapPath(Filename)
            cryRpt.ExportOptions.DestinationOptions = oDiskOpts
            cryRpt.Export()

            ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '" & Filename & "', target='_blank' );", True)
        Catch EX As Exception
            Prompt("ERROR: " & EX.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            CRV.Dispose()
        End Try
    End Sub

    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SORTING AND PAGING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    Public Function GetDocument(ByVal query As String) As DataSet

        If con.State = ConnectionState.Open Or con.State = 16 Or con.State = 2 Or con.State = ConnectionState.Fetching Or con.State = ConnectionState.Executing Then con.Close()
        Dim cmd As New SqlCommand(query, con)
        con.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        con.Close()
        Return ds
    End Function

    Public Property direction() As SortDirection
        Get
            If ViewState("directionState") Is Nothing Then
                ViewState("directionState") = SortDirection.AscendingOrder
            End If
            Return DirectCast(ViewState("directionState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("directionState") = value
        End Set
    End Property

End Class