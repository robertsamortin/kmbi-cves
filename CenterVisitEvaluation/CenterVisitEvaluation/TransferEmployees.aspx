﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TransferEmployees.aspx.vb" Inherits="CenterVisitEvaluation.TransferEmployees" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:UpdatePanel ID="upPnlTransfer" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    FROM
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="panel-body">
                       <div class="form-group">
                           <div class="row">
                               <div class="col-sm-2">Branch</div>
                               <div class="col-sm-10">
                                  <asp:DropDownList ID="cboFrombranch" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                        </asp:DropDownList>
                                   <asp:TextBox ID="txtFromBranchCode" runat="server" Visible ="false"></asp:TextBox>
                                   <asp:TextBox ID="txtFromRegCode" runat="server" Visible ="false"></asp:TextBox>
                                   <asp:TextBox ID="txtFromAreaCode" runat="server" Visible ="false"></asp:TextBox>
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="row">
                               <div class="col-sm-2">Employee</div>
                               <div class="col-sm-10">
                                  <asp:DropDownList ID="cboEmployee" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                        </asp:DropDownList><asp:TextBox ID="txtEmployeeCode" runat="server" Visible ="false"></asp:TextBox>
                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    TO
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="panel-body">
                       <div class="form-group">
                           <div class="row">
                               <div class="col-sm-2">Branch</div>
                               <div class="col-sm-10">
                                 
                                  <asp:DropDownList ID="cboToBranch" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                        </asp:DropDownList>
                                   <asp:TextBox ID="txtToBranchCode" runat="server" Visible ="false"></asp:TextBox>
                                   <asp:TextBox ID="txtToRegCode" runat="server" Visible ="false"></asp:TextBox>
                                   <asp:TextBox ID="txtToAreaCode" runat="server" Visible ="false"></asp:TextBox>
                              
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="row">
                               <div class="col-sm-2">Position</div>
                               <div class="col-sm-10">
                                  <asp:DropDownList ID="cboPosition" runat="server" CssClass="form-control" AutoPostBack="True" required>

                                      <asp:ListItem>Select Position</asp:ListItem>
                                        <asp:ListItem>RD</asp:ListItem>
                                        <asp:ListItem>AM</asp:ListItem>
                                        <asp:ListItem>BM</asp:ListItem>
                                        <asp:ListItem>BA</asp:ListItem>
                                      <asp:ListItem>BAA</asp:ListItem>
                                      <asp:ListItem>PUH</asp:ListItem>

                                        </asp:DropDownList>
                                   <asp:TextBox ID="txtPosCode" runat="server" Visible ="false"></asp:TextBox>
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="row">
                               <div class="col-sm-2">Unit</div>
                               <div class="col-sm-10">
                                  <asp:DropDownList ID="cboUnit" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                       <asp:ListItem>Select Unit</asp:ListItem>
                                        <asp:ListItem>A</asp:ListItem>
                                        <asp:ListItem>B</asp:ListItem>
                                        <asp:ListItem>C</asp:ListItem>
                                        </asp:DropDownList>
                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-right">
                <div class="btn-group" role="group">
                    <asp:Button ID="cmdSave" runat="server" Text="Save" CssClass="btn btn-success"/>
                    
                                <asp:ConfirmButtonExtender ID="confirmExtender" runat="server" ConfirmText="Are you sure you want to submit?" TargetControlID="cmdSave" >
                                </asp:ConfirmButtonExtender>
                            </div>
            </div>
        </div>
    </div>
    
            </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade"  tabindex="-1" id="modMessageBox" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<!--SESSIONS-->
    <div> 
        <asp:Label ID="lblfullname" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblposition" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchcode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchdesc" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblsearch" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Visible="False"></asp:Label>
    </div>
</asp:Content>
