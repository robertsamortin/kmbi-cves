﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Cves.aspx.vb" Inherits="CenterVisitEvaluation.Cves" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/jquery-ui.js" type="text/javascript"></script>

    <script src="assets/percircle/percircle.js"></script>
    <link href="assets/percircle/percircle.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />

    
    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#report').addClass("hidden");
            $('#cnt_').addClass("hidden");

            $('btnFilter').click(function () {
                var $this = $(this);

                if ($this.hasClass("hidden")) {
                    $(this).removeClass("hidden").addClass("visible");
                    
                } else {
                    $(this).removeClass("visible").addClass("hidden");
                }
            });            
        });
    </script>

    <script type="text/javascript">
        function closereport() {
            $('#report').addClass("hidden");
            if ($('#report').hasClass("visible")) {
                $('#report').removeClass("visible").addClass("hidden");
            }
            return false
        };
    </script>

    <script type="text/javascript">
        function closetable() {
            $('#tblSummary').addClass("hidden");
            if ($('#tblSummary').hasClass("visible")) {
                $('#tblSummary').removeClass("visible").addClass("hidden");
            }
            return false
        };
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <nav id="myNav" class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand"> 
                                    <span class="glyphicon glyphicon-list"></span>
                                    DASHBOARD
                                </div>                          
                            </div>
                        </div>
                    </nav>
                    <div class="report-filter form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-2">
                                <label class="control-label">From</label>                                                                                       
                                <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">To</label>                           
                                <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Region</label>
                            <div class="col-sm-6"> 
                                <asp:DropDownList ID="drpRegion" runat="server" class="form-control" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Area</label>
                            <div class="col-sm-6">                  
                                <asp:DropDownList ID="drpArea" runat="server" class="form-control" AutoPostBack="True">
                                </asp:DropDownList> 
                            </div>
                        </div>          
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Branch</label>
                            <div class="col-sm-6">                 
                                <asp:DropDownList ID="drpBranch" runat="server" class="form-control" AutoPostBack="True">
                                </asp:DropDownList> 
                            </div>
                        </div>            
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Unit</label>
                            <div class="col-sm-6">                 
                                <asp:DropDownList ID="drpUnit" runat="server" class="form-control" AutoPostBack="True">
                                </asp:DropDownList> 
                            </div>
                        </div>            
                        <div class="form-group">
                            <label class="col-sm-2 control-label">PO</label>
                            <div class="col-sm-6">                 
                                <asp:DropDownList ID="drpPO" runat="server" class="form-control" AutoPostBack="True">
                                </asp:DropDownList> 
                            </div>
                        </div>                         
                        <div class="form-group">
                            <div class="form-inline col-sm-offset-5 col-sm-9">
                            
                                <div class="form-group col-sm-3">                                                         
                                    <asp:Button ID="btnReport" runat="server" Text="Generate Report PDF" class="btn btn-primary btn-lg" Font-Bold="True"/>
                                </div> 
                                <div class="form-group">                                                         
                                    <asp:Button ID="btnFilter" runat="server" Text="Filter" class="btn btn-primary btn-lg" Font-Bold="True" data-toggle="collapse" data-target="#report"/>
                                </div>
                                <CR:CrystalReportViewer ID="CRV" runat="server" AutoDataBind="true" Visible="false" />  
                            </div>                                                     
                        </div>
                    </div>
                    
                </div>

                <%--<div id="cnt_" class="panel panel-default" style="font-size:14px">
                    <div class="panel-heading" style="font-weight:bold">
                        <span class="glyphicon glyphicon-list"></span>
                        LIST OF EVALUATORS
                    </div>
                    
                </div>--%>


                <div id="report" class="panel panel-default" style="font-size:14px">
                    <div class="panel-heading" style="font-weight:bold">
                        <span class="glyphicon glyphicon-list"></span>
                        REPORT
                    </div>
                    <div class="panel-body">
                        <div class="text-danger" style="font-weight:bold; font-size:large">                           
                            <asp:Label ID="lblNoRecords" runat="server"></asp:Label>
                        </div>
                    <table id="tblSummary" class="table-bordered" style="margin: 0 auto; text-align:center;">
                        <tr>
                            <td style="padding:15px;">
                                <div>
                                    Number of Evaluations
                                </div>
                                <div style="font-size:20px; padding-top:5px;">
                                    <asp:Label ID="lblCount" runat="server"></asp:Label>
                                </div>
                            </td>
                            <td style="padding:15px;">                            
                                <div>
                                    Active Program Members
                                </div>
                                <div style="font-size:20px; padding-top:5px;">
                                    <asp:Label ID="lblTotalMember" runat="server"></asp:Label>
                                </div>
                            </td>
                            <td style="padding:15px;">                               
                                <div>
                                    Members Present During Evaluation
                                </div>
                                <div style="font-size:20px; padding-top:5px;">
                                    <asp:Label ID="lblActualAtt" runat="server"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:15px;">
                                <div>
                                    Center Meeting Attendance Rate
                                </div>
                                <div style="font-size:16px; padding-top:15px;">
                                    <asp:Label ID="lblPercentAtt" runat="server"></asp:Label>
                                </div>                               
                                <div id="attendance" class="center"></div>                                                                                         
                            </td>
                            <td style="padding:15px;">
                                <div>
                                    Average Score Per Center
                                </div>
                                <div style="font-size:16px; padding-top:15px;">
                                    <asp:Label ID="lblScore" runat="server"></asp:Label>
                                </div>
                                <div id="score" class="center"></div> 
                            </td>
                            <td style="padding:15px;">
                                <div>
                                    CVES Rating
                                </div>
                                <div style="font-size:16px; padding-top:15px;">
                                    <asp:Label ID="lblRating" runat="server"></asp:Label>
                                    <asp:HiddenField ID="hdnRating" runat="server" />
                                </div>
                                <div id="rating" class="center"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-size:16px; padding:15px;">
                                <asp:Label ID="lblEquivalent" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>                      
                        <br />
                        <br />

                        <div class="panel-body">
                           <asp:GridView ID="grdEvaluatorList" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                            GridLines="None" CssClass="gridview" Width="100%" PageSize="25" Caption="LIST OF EVALUATORS">
                            <Columns>
                            
                                <asp:TemplateField HeaderText="Evaluator" SortExpression="position_code">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEvaluator" runat="server" CausesValidation="False" 
                                                CommandArgument="<%#Bind('conducted_by') %>" CommandName="Evaluator" 
                                                Text="<%#Bind('position_code') %>" >
                                        </asp:LinkButton>
                                    </ItemTemplate>                    
                                </asp:TemplateField>
                           
                            
                                <asp:BoundField DataField="region_desc" SortExpression="region_desc" HeaderText="Region" />
                                <asp:BoundField DataField="area_desc" SortExpression="area_desc" HeaderText="Area" />
                                <asp:BoundField DataField="branch_desc" SortExpression="branch_desc" HeaderText="Branch" />
                                <asp:BoundField DataField="unit_code" SortExpression="unit_code" HeaderText="Unit" />
                                <asp:BoundField DataField="no_centers_evaluated" SortExpression="no_centers_evaluated" HeaderText="Count" />
                            </Columns> 
                            <PagerStyle CssClass="GridPager" HorizontalAlign="Center" />
                            </asp:GridView>
                        </div>
                        <br />
                        <br />

                        <asp:Panel ID="pnlSummary" runat="server">
                            <div class="table-responsive">
                                <table class="table table-striped"><caption>SUMMARY</caption>
                                    <thead class="thead-default">
                                    <tr>
                                        <th>Item</th>
                                        <th>Centers Complied (Percentage)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Program Officer</th>
                                    </tr>
                                    <tr>
                                        <td>Arrived ON or BEFORE the meeting time</th>
                                        <td><asp:Label ID="lblpa_arrived" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Program Officer visits delinquent client/s (attendance and payment)</th>
                                        <td><asp:Label ID="lblpa_delinquent" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Program Officer makes the discussion lively and interesting</th>
                                        <td><asp:Label ID="lblpa_discuss" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Center Meeting</th>
                                    </tr>
                                    <tr>
                                        <td>Follow the standard process</th>
                                        <td><asp:Label ID="lblctr_follow" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Start on time</th>
                                        <td><asp:Label ID="lblctr_start" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Facilitator provide warm-up game (ice breaker) to the program member</th>
                                        <td><asp:Label ID="lblctr_provide" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Transformational topic (discussion) was facilitated</th>
                                        <td><asp:Label ID="lblctr_trans" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Facilitator use visual aid during the presentation of topic</th>
                                        <td><asp:Label ID="lblctr_visual" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Minutes of the meeting is recorded in the logbook</th>
                                        <td><asp:Label ID="lblctr_minutes" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <th scope="row">Attendance</th>
                                    </tr>
                                    <tr>
                                        <td>With a minimum of 80% attendance</th>
                                        <td><asp:Label ID="lblatt_min" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>80% of Program Members arrive in the center on-time</th>
                                        <td><asp:Label ID="lblatt_pm" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <th scope="row">Center Materials (Available and follow the standard format)</th>
                                    </tr>
                                    <tr>
                                        <td>Center House or comfortable meeting venue</th>
                                        <td><asp:Label ID="lblmat_house" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Center Signage</th>
                                        <td><asp:Label ID="lblmat_sign" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Pitong (7) Layunin</th>
                                        <td><asp:Label ID="lblmat_pitong" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Pangako ng Kaanib</th>
                                        <td><asp:Label ID="lblmat_pang" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Organizational Chart</th>
                                        <td><asp:Label ID="lblmat_org" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Columnar Book or Financial Record</th>
                                        <td><asp:Label ID="lblmat_column" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Black Board / white board</th>
                                        <td><asp:Label ID="lblmat_black" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Wall Clock</th>
                                        <td><asp:Label ID="lblmat_wall" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Center Internal Policy (folder)</th>
                                        <td><asp:Label ID="lblmat_center" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Attendance Logbook with summary</th>
                                        <td><asp:Label ID="lblmat_attendance" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Weekly Center Discussion tool (Life series book 2)</th>
                                        <td><asp:Label ID="lblmat_weekly" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <th scope="row">Payment</th>
                                    </tr>
                                    <tr>
                                        <td>100% weekly collection before the meeting ended</th>
                                        <td><asp:Label ID="lblpay_weekly" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>The center kept the second copy of ECR in the center, attached with the OR issued by the office</th>
                                        <td><asp:Label ID="lblpay_center" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Individual Passbook is presented during payment and updated</th>
                                        <td><asp:Label ID="lblpay_indiv" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Program member/s deposited the collection</th>
                                        <td><asp:Label ID="lblpay_pm" runat="server" Text="Label"></asp:Label></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <%--<dl>                           
                                <dt>...</dt>
                                <dd>...</dd>
                            </dl>--%>
                        </asp:Panel>                        
                    </div>
                </div>                                       
            </div>  
            <%--<script>
                var filter = $('.report-filter');
                filter.find('select').change(function () {
                    var select = $(this);
                    var lvl = select.data('level');

                    filter.find('select').each(function () {
                        var select2 = $(this);
                        if (select2.data('level') > lvl) {
                            $(this).val('').attr('disabled', 'disabled');
                        }
                    });

                    if (select.val() != '') {
                        var nextLvl = lvl + 1;
                        filter.find('select[data-level="' + nextLvl + '"]').removeAttr('disabled');
                    }
                })
            </script>--%>

        </ContentTemplate>    
    </asp:UpdatePanel>

<div class="modal fade"  tabindex="-1" id="modMessageBox" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
 
    <script type="text/javascript">
     var prm = Sys.WebForms.PageRequestManager.getInstance();
     prm.add_endRequest(function () {
         jQuery(function ($) {
             $("#txtDateFrom").datepicker();
             $("#txtDateTo").datepicker();
         });

     });

     jQuery(function ($) {
         $("#txtDateFrom").datepicker();
         $("#txtDateTo").datepicker();
     });

    </script>

    <script>
        function circlechart() {
            var attendance = $('#MainContent_lblPercentAtt').html()
            attendance = parseFloat(attendance.substring(0, attendance.length - 1));         
            var score = $('#MainContent_lblScore').html()
            var rating = $('#MainContent_lblRating').html()
            rating = parseFloat(rating.substring(0, rating.length - 1));

            $('#MainContent_lblPercentAtt').hide();
            $('#MainContent_lblScore').hide();
            $('#MainContent_lblRating').hide();

            $("#attendance").percircle({
                percent: attendance
            });

            $("#score").percircle({
                text: score + "/30",
                percent: rating
            });

            $("#rating").percircle({
                percent: rating
            });
        }
        
    </script>

    <div>
        <asp:Label ID="lblbranchcode" runat="server" Text="Label" Visible="false"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Text="Label" Visible="false"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Text="Label" Visible="false"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Text="Label" Visible="false"></asp:Label>
    </div> 
</asp:Content>
