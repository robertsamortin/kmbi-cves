﻿Imports System.Data.SqlClient

Public Class arasSite
    Inherits System.Web.UI.MasterPage

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                'PUH
                If lblaccesslevel.Text = 2 Then
                    lnkAm.Visible = False
                    lnkBm.Visible = False
                    lnkPuh.Visible = False
                    lnkPo.Visible = True
                End If

                'BM
                If lblaccesslevel.Text = 5 Then
                    lnkAm.Visible = False
                    lnkBm.Visible = False
                    lnkPuh.Visible = True
                    lnkPo.Visible = True
                End If

                'AM
                If lblaccesslevel.Text = 6 Then
                    lnkAm.Visible = False
                    lnkBm.Visible = True
                    lnkPuh.Visible = True
                    lnkPo.Visible = True
                End If

                'RD
                If lblaccesslevel.Text = 7 Or lblaccesslevel.Text = 8 Then
                    lnkAm.Visible = True
                    lnkBm.Visible = True
                    lnkPuh.Visible = True
                    lnkPo.Visible = True
                End If

                If lblaccesslevel.Text = 8 Then
                    sysSettings.Visible = True
                Else
                    sysSettings.Visible = False
                End If
            End If
        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub lnkCpassword_Click(sender As Object, e As System.EventArgs) Handles lnkCpassword.Click

        Me.txtusername.Text = HttpContext.Current.User.Identity.Name
        Me.txtusername.Focus()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openChangePassword();", True)
        'upPnlCPassword.Update()

    End Sub


    Private Sub cmdOkCPassword_Click(sender As Object, e As System.EventArgs) Handles cmdOkCPassword.Click

        Try
            Dim empcode As String = Me.lblcurrentuser.Text

            '@@@@@@@ HASH PASSWORD @@@@@@@@@
            Dim newpassword As String = PasswordHash.CreateHash(Trim(Me.txtNewPassword.Text))
            Dim delimiter As Char() = {":"c}
            Dim split As String() = newpassword.Split(delimiter)
            Dim salt As String = split(2)
            Dim hash As String = split(3)
            Dim username As String = Trim(Me.txtusername.Text)

            If Me.txtusername.Text = Trim("") Then
                Prompt("Please enter your Username", Me.lblMessage, Me.pnlUpdateFormMessageSite)
                ' MesBox("Please enter your Username", Me.lblMessage, Me.pnlUpdateFormMessage, Me)
                Me.txtusername.Focus()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBoxSite();", True)
                lnkCpassword_Click(Me, Nothing)
            ElseIf Trim(Me.txtNewPassword.Text) <> Trim(Me.txtRetypePassword.Text) Then
                Prompt("Please re-type your new password correctly.", Me.lblMessage, Me.pnlUpdateFormMessageSite)
                ' MesBox("Please re-type your new password correctly", Me.lblMessage, Me.pnlUpdateFormMessage, Me)
                Me.txtRetypePassword.Focus()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBoxSite();", True)
                lnkCpassword_Click(Me, Nothing)
            ElseIf Me.txtNewPassword.Text = "" Then
                Prompt("Please enter new password", Me.lblMessage, Me.pnlUpdateFormMessageSite)
                ' MesBox("Please enter new password", Me.lblMessage, Me.pnlUpdateFormMessage, Me)
                Me.txtNewPassword.Focus()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBoxSite();", True)
                lnkCpassword_Click(Me, Nothing)
            Else
                If con.State = 1 Then con.Close()
                con.Open()
                Dim sqlQuery As String = "MF_Update_Password '" & empcode & "', " &
                                        "'" & hash & "'," &
                                        "'" & salt & "'," &
                                        "'" & username & "'"


                Dim cmd As New SqlCommand(sqlQuery, con)
                cmd.ExecuteNonQuery()
                'upPnlCPassword.Update()
                'Prompt("Successfully Saved! Please login using your new credential", Me.lblMessage, Me.pnlUpdateFormMessageSite)
                'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBoxSite();", True)


                Session.Abandon()
                Session.Clear()
                FormsAuthentication.RedirectToLoginPage()
            End If

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessageSite)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBoxSite();", True)
        Finally
            con.Close()
        End Try
    End Sub
End Class