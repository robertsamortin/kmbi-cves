﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class poEvaluation
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Dim cryRpt As New ReportDocument

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                Me.txtdate.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
                BIND_Branch()
                BIND_Unit()
                GEt_score()
            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub


    Private Sub BIND_Branch()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "OPS_list_branch '" & branchcode & "', '" & areacode & "', '" & accesslevel & "','" & pa & "', '" & pus & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.cbobranch.Items.Clear()
            While dr.Read
                Me.cbobranch.Items.Add(Trim(dr("branch_desc").ToString))
            End While


            upPnlEval.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub



    Private Sub GEt_score()
        Dim totalscore As Integer
        totalscore = Val(Me.txtq1.Text) + Val(Me.txtq2.Text) + Val(Me.txtq3.Text) _
            + Val(Me.txtq4.Text) + Val(Me.txtq5.Text) + Val(Me.txtq6.Text) + Val(Me.txtq7.Text) _
            + Val(Me.txtq8.Text) + Val(Me.txtq9.Text) _
            + Val(Me.txtq10.Text) + Val(Me.txtq11.Text) _
            + Val(Me.txtq12.Text) + Val(Me.txtq13.Text)

        Me.lblTotalScore.Text = totalscore

        Dim rating As Double
        Dim equivalent As String

        rating = FormatNumber((totalscore / 13) * 100, 2)
        'equivalent = rating & "%"
        If rating >= 90 Then
            equivalent = "VERY GOOD"
        ElseIf rating >= 80 And rating <= 89 Then
            equivalent = "GOOD"
        ElseIf rating >= 70 And rating <= 79 Then
            equivalent = "POOR"
        Else
            equivalent = "TOTALLY INEFFECTIVE"
        End If

        Me.lblRating.Text = rating
        Me.lblEquivalent.Text = equivalent
        upPnlEval.Update()
    End Sub

    Private Sub Get_RecordData()
        Try

            Dim conducteddate As Date = CDate(Me.txtdate.Text)
            Dim conductedbay As String = Trim(Me.lblcurrentuser.Text)
            Dim branch As String = Trim(Me.cbobranch.Text)

            Dim po As String = Trim(Me.txtProgramOfficer.Text)

            'MsgBox(conducteddate)
            'MsgBox(conductedbay)
            'MsgBox(branch)
            'MsgBox(center)
            'MsgBox(pa)

            If con.State = 1 Then con.Close()
            con.Open()
            Dim sqlQuery As String = "aras_get_eval_po '" & conducteddate & "'," &
                "'" & conductedbay & "'," &
                "'" & branch & "'," &
                "'" & po & "'"

            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader = cmd.ExecuteReader()

            If dr.Read() Then
                Me.txtq1.Text = CInt(dr("meet_minimum_clients"))
                If txtq1.Text = 0 Then q1_rd1.Checked = False Else q1_rd1.Checked = True
                If txtq1.Text = 0 Then q1_rd2.Checked = True Else q1_rd2.Checked = False

                Me.txtq2.Text = CInt(dr("meet_required_centers"))
                If txtq2.Text = 0 Then q2_rd1.Checked = False Else q2_rd1.Checked = True
                If txtq2.Text = 0 Then q2_rd2.Checked = True Else q2_rd2.Checked = False

                Me.txtq3.Text = CInt(dr("cbu_withdrawal"))
                If txtq3.Text = 0 Then q3_rd1.Checked = False Else q3_rd1.Checked = True
                If txtq3.Text = 0 Then q3_rd2.Checked = True Else q3_rd2.Checked = False

                Me.txtq4.Text = CInt(dr("efficient_ontime"))
                If txtq4.Text = 0 Then q4_rd1.Checked = False Else q4_rd1.Checked = True
                If txtq4.Text = 0 Then q4_rd2.Checked = True Else q4_rd2.Checked = False

                Me.txtq5.Text = CInt(dr("collection_performance"))
                If txtq5.Text = 0 Then q5_rd1.Checked = False Else q5_rd1.Checked = True
                If txtq5.Text = 0 Then q5_rd2.Checked = True Else q5_rd2.Checked = False

                Me.txtq6.Text = CInt(dr("meet_minimum_attendance"))
                If txtq6.Text = 0 Then q6_rd1.Checked = False Else q6_rd1.Checked = True
                If txtq6.Text = 0 Then q6_rd2.Checked = True Else q6_rd2.Checked = False

                Me.txtq7.Text = CInt(dr("make_followup"))
                If txtq7.Text = 0 Then q7_rd1.Checked = False Else q7_rd1.Checked = True
                If txtq7.Text = 0 Then q7_rd2.Checked = True Else q7_rd2.Checked = False

                Me.txtq8.Text = CInt(dr("keep_maintain"))
                If txtq8.Text = 0 Then q8_rd1.Checked = False Else q8_rd1.Checked = True
                If txtq8.Text = 0 Then q8_rd2.Checked = True Else q8_rd2.Checked = False

                Me.txtq9.Text = CInt(dr("no_advance"))
                If txtq9.Text = 0 Then q9_rd1.Checked = False Else q9_rd1.Checked = True
                If txtq9.Text = 0 Then q9_rd2.Checked = True Else q9_rd2.Checked = False

                Me.txtq10.Text = CInt(dr("implement_change"))
                If txtq10.Text = 0 Then q10_rd1.Checked = False Else q10_rd1.Checked = True
                If txtq10.Text = 0 Then q10_rd2.Checked = True Else q10_rd2.Checked = False

                Me.txtq11.Text = CInt(dr("prepare_visual"))
                If txtq11.Text = 0 Then q11_rd1.Checked = False Else q11_rd1.Checked = True
                If txtq11.Text = 0 Then q11_rd2.Checked = True Else q11_rd2.Checked = False

                Me.txtq12.Text = CInt(dr("conduct_center"))
                If txtq12.Text = 0 Then q12_rd1.Checked = False Else q12_rd1.Checked = True
                If txtq12.Text = 0 Then q12_rd2.Checked = True Else q12_rd2.Checked = False

                Me.txtq13.Text = CInt(dr("resolve_all"))
                If txtq13.Text = 0 Then q13_rd1.Checked = False Else q13_rd1.Checked = True
                If txtq13.Text = 0 Then q13_rd2.Checked = True Else q13_rd2.Checked = False



                Me.lblTotalScore.Text = CInt(dr("total_score"))
                Me.lblRating.Text = CDbl(dr("ratings"))
                Me.lblEquivalent.Text = Trim(dr("equivalent_ratings"))
                Me.txtComments.Text = Trim(dr("remarks"))
            End If

            dr.Close()
            cmd.Dispose()
            upPnlEval.Update()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub cmdsave_Click(sender As Object, e As System.EventArgs) Handles cmdsave.Click


        Try
            GEt_score()


            Dim branch As String = Trim(Me.cbobranch.Text)
            Dim po As String = Trim(Me.txtProgramOfficer.Text)
            Dim unitcode As String = Trim(Me.cboUnit.Text)
            Dim conducteddate As Date = CDate(Me.txtdate.Text)
            Dim conductedbay As String = Trim(Me.lblcurrentuser.Text)
            Dim q1 As Integer = CInt(Me.txtq1.Text)
            Dim q2 As Integer = CInt(Me.txtq2.Text)
            Dim q3 As Integer = CInt(Me.txtq3.Text)
            Dim q4 As Integer = CInt(Me.txtq4.Text)
            Dim q5 As Integer = CInt(Me.txtq5.Text)
            Dim q6 As Integer = CInt(Me.txtq6.Text)
            Dim q7 As Integer = CInt(Me.txtq7.Text)
            Dim q8 As Integer = CInt(Me.txtq8.Text)
            Dim q9 As Integer = CInt(Me.txtq9.Text)
            Dim q10 As Integer = CInt(Me.txtq10.Text)
            Dim q11 As Integer = CInt(Me.txtq11.Text)
            Dim q12 As Integer = CInt(Me.txtq12.Text)
            Dim q13 As Integer = CInt(Me.txtq13.Text)

            Dim remarks As String = Me.txtComments.Text.Replace("'", "''")
            Dim totalscore As Integer = CInt(Me.lblTotalScore.Text)
            Dim rating As Decimal = CDbl(Me.lblRating.Text)
            Dim equirating As String = Trim(Me.lblEquivalent.Text)

            If con.State = 1 Then con.Close()
            con.Open()
            Dim sqlQuery As String = ""


            sqlQuery = "aras_insert_eval_po " &
                                    "'" & branch & "', " &
                                    "'" & po & "', " &
                                    "'" & unitcode & "', " &
                                    "'" & conducteddate & "', " &
                                    "'" & conductedbay & "'," &
                                    "'" & q1 & "'," &
                                    "'" & q2 & "'," &
                                    "'" & q3 & "'," &
                                    "'" & q4 & "'," &
                                    "'" & q5 & "'," &
                                    "'" & q6 & "'," &
                                    "'" & q7 & "'," &
                                    "'" & q8 & "'," &
                                    "'" & q9 & "'," &
                                    "'" & q10 & "'," &
                                    "'" & q11 & "'," &
                                    "'" & q12 & "'," &
                                    "'" & q13 & "'," &
                                    "'" & totalscore & "'," &
                                    "'" & rating & "'," &
                                    "'" & equirating & "'," &
                                    "'" & remarks & "'"

            Dim cmd As New SqlCommand(sqlQuery, con)
            cmd.ExecuteNonQuery()
            Prompt("Successfully Submitted", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            upPnlEval.Update()

        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try


    End Sub

    Protected Sub q1_rd1_CheckedChanged(sender As Object, e As EventArgs) Handles q1_rd1.CheckedChanged
        If Me.q1_rd1.Checked = True Then Me.txtq1.Text = "1" Else Me.txtq1.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q1_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q1_rd2.CheckedChanged
        If Me.q1_rd2.Checked = True Then Me.txtq1.Text = "0" Else Me.txtq1.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q2_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q2_rd1.CheckedChanged
        If Me.q2_rd1.Checked = True Then Me.txtq2.Text = "1" Else Me.txtq2.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q2_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q2_rd2.CheckedChanged
        If Me.q2_rd2.Checked = True Then Me.txtq2.Text = "0" Else Me.txtq2.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q3_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q3_rd1.CheckedChanged
        If Me.q3_rd1.Checked = True Then Me.txtq3.Text = "1" Else Me.txtq3.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q3_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q3_rd2.CheckedChanged
        If Me.q3_rd2.Checked = True Then Me.txtq3.Text = "0" Else Me.txtq3.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q4_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q4_rd1.CheckedChanged
        If Me.q4_rd1.Checked = True Then Me.txtq4.Text = "1" Else Me.txtq4.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q4_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q4_rd2.CheckedChanged
        If Me.q4_rd2.Checked = True Then Me.txtq4.Text = "0" Else Me.txtq4.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q5_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q5_rd1.CheckedChanged
        If Me.q5_rd1.Checked = True Then Me.txtq5.Text = "1" Else Me.txtq5.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q5_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q5_rd2.CheckedChanged
        If Me.q5_rd2.Checked = True Then Me.txtq5.Text = "0" Else Me.txtq5.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q6_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q6_rd1.CheckedChanged
        If Me.q6_rd1.Checked = True Then Me.txtq6.Text = "1" Else Me.txtq6.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q6_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q6_rd2.CheckedChanged
        If Me.q6_rd2.Checked = True Then Me.txtq6.Text = "0" Else Me.txtq6.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q7_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q7_rd1.CheckedChanged
        If Me.q7_rd1.Checked = True Then Me.txtq7.Text = "1" Else Me.txtq7.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q7_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q7_rd2.CheckedChanged
        If Me.q7_rd2.Checked = True Then Me.txtq7.Text = "0" Else Me.txtq7.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q8_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q8_rd1.CheckedChanged
        If Me.q8_rd1.Checked = True Then Me.txtq8.Text = "1" Else Me.txtq8.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q8_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q8_rd2.CheckedChanged
        If Me.q8_rd2.Checked = True Then Me.txtq8.Text = "0" Else Me.txtq8.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q9_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q9_rd1.CheckedChanged
        If Me.q9_rd1.Checked = True Then Me.txtq9.Text = "1" Else Me.txtq9.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q9_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q9_rd2.CheckedChanged
        If Me.q9_rd2.Checked = True Then Me.txtq9.Text = "0" Else Me.txtq9.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q10_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q10_rd1.CheckedChanged
        If Me.q10_rd1.Checked = True Then Me.txtq10.Text = "1" Else Me.txtq9.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q10_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q10_rd2.CheckedChanged
        If Me.q10_rd2.Checked = True Then Me.txtq10.Text = "0" Else Me.txtq10.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q11_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q11_rd1.CheckedChanged
        If Me.q11_rd1.Checked = True Then Me.txtq11.Text = "1" Else Me.txtq11.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q11_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q11_rd2.CheckedChanged
        If Me.q11_rd2.Checked = True Then Me.txtq11.Text = "0" Else Me.txtq11.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q12_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q12_rd1.CheckedChanged
        If Me.q12_rd1.Checked = True Then Me.txtq12.Text = "1" Else Me.txtq12.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q12_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q12_rd2.CheckedChanged
        If Me.q12_rd2.Checked = True Then Me.txtq12.Text = "0" Else Me.txtq12.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q13_rd1_CheckedChanged(sender As Object, e As System.EventArgs) Handles q13_rd1.CheckedChanged
        If Me.q13_rd1.Checked = True Then Me.txtq13.Text = "1" Else Me.txtq13.Text = "0"
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub q13_rd2_CheckedChanged(sender As Object, e As System.EventArgs) Handles q13_rd2.CheckedChanged
        If Me.q13_rd2.Checked = True Then Me.txtq13.Text = "0" Else Me.txtq13.Text = "1"
        GEt_score()
        upPnlEval.Update()
    End Sub


    Private Sub btnViewScore_Click(sender As Object, e As System.EventArgs) Handles btnViewScore.Click
        GEt_score()
        upPnlEval.Update()
    End Sub

    Private Sub BIND_Unit()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim currentuser As String = Trim(Me.lblcurrentuser.Text)
            Dim sqlQuery As String

            Me.cboUnit.Items.Clear()

            If accesslevel = 2 Then
                sqlQuery = "Select unit_code from employee where emp_code = '" + currentuser + "'"

                Dim cmd As New SqlCommand(sqlQuery, con)
                Dim dr As SqlDataReader
                dr = cmd.ExecuteReader()


                While dr.Read
                    Me.cboUnit.Items.Add(Trim(dr("unit_code").ToString))
                End While

            Else
                'If cbobranch.Text <> "All" Then
                '    Me.cboUnit.Items.Add("All")
                Me.cboUnit.Items.Add("A")
                Me.cboUnit.Items.Add("B")
                Me.cboUnit.Items.Add("C")
                'Else
                '    Me.cboUnit.Items.Add("All")
                'End If

            End If

            upPnlEval.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub cmdReport_Click(sender As Object, e As System.EventArgs) Handles cmdReport.Click
        '' MsgBox("")
        'Try

        '    Dim PA As String = Trim(Me.txtProgramOfficer.Text)
        '    Dim BRANCH As String = Trim(Me.cbobranch.Text)
        '    Dim cmd As New SqlCommand
        '    cmd.Connection = con
        '    cmd.CommandText = "OPS_cves_form"
        '    cmd.CommandType = CommandType.StoredProcedure
        '    cmd.Parameters.AddWithValue("pa", PA)
        '    cmd.Parameters.AddWithValue("branch", BRANCH)
        '    cmd.Parameters.AddWithValue("center_code", CENTER)

        '    Dim da As New SqlDataAdapter
        '    da.SelectCommand = cmd
        '    'MsgBox(Server.MapPath("reports/cvef_revised.rpt"))
        '    Dim dt As New DataTable
        '    da.Fill(dt)
        '    cryRpt.Load(Server.MapPath("report/cvef_revised.rpt"))
        '    cryRpt.SetDataSource(dt)

        '    Me.CRV.ReportSource = cryRpt
        '    'Me.crViewer.DataBind()

        '    Dim Filename As String = "report/CVEF" & BRANCH & CENTER & Replace(CStr(FormatDateTime(Date.Now, DateFormat.ShortDate)), "/", "") & ".pdf"
        '    If System.IO.File.Exists(Server.MapPath(Filename)) = True Then
        '        System.IO.File.Delete(Server.MapPath(Filename))
        '    End If
        '    Dim oDiskOpts As New DiskFileDestinationOptions
        '    cryRpt.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
        '    cryRpt.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat
        '    oDiskOpts.DiskFileName = Server.MapPath(Filename)
        '    cryRpt.ExportOptions.DestinationOptions = oDiskOpts
        '    cryRpt.Export()

        '    ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '" & Filename & "', target='_blank' );", True)
        'Catch EX As Exception
        '    Prompt("ERROR: " & EX.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        'Finally
        '    cryRpt.Close()
        '    cryRpt.Dispose()
        'End Try
    End Sub

    Private Sub txtdate_TextChanged(sender As Object, e As System.EventArgs) Handles txtdate.TextChanged
        Get_RecordData()
    End Sub



    Private Sub RetrieveProvince()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select province_desc from province order by province_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.drpProvince.Items.Clear()
            While dr.Read
                Me.drpProvince.Items.Add(Trim(dr("province_desc").ToString))
            End While
            BIND_Province()
            RetrieveMunicipality()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub RetrieveMunicipality()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select municipality_desc from municipality where province_code = (select province_code from province where province_desc = '" + drpProvince.SelectedItem.Text + "') order by municipality_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.drpMunicipality.Items.Clear()
            While dr.Read
                Me.drpMunicipality.Items.Add(Trim(dr("municipality_desc").ToString))
            End While
            BIND_Municipality()
            RetrieveBarangay()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub RetrieveBarangay()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select barangay_desc from barangay where municipality_code = (select municipality_code from municipality where municipality_desc = '" + drpMunicipality.SelectedItem.Text + "' and province_code = '" + hdnProvince.Value.ToString() + "') order by barangay_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.drpBarangay.Items.Clear()
            While dr.Read
                Me.drpBarangay.Items.Add(Trim(dr("barangay_desc").ToString))
            End While
            BIND_Barangay()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Province()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select province_code from province where province_desc = '" + drpProvince.SelectedItem.Text + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            dr.Read()
            Me.hdnProvince.Value = dr("province_code").ToString()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Municipality()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select municipality_code from municipality where municipality_desc = '" + drpMunicipality.SelectedItem.Text + "' and province_code = '" + hdnProvince.Value.ToString() + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            dr.Read()
            Me.hdnMunicipality.Value = dr("municipality_code").ToString()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub BIND_Barangay()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim sqlQuery As String = "Select barangay_code from barangay where barangay_desc = '" + drpBarangay.SelectedItem.Text + "' and municipality_code = '" + hdnMunicipality.Value.ToString() + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            dr.Read()
            Me.hdnBarangay.Value = dr("barangay_code").ToString()
            upPnlCAddress.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Protected Sub drpProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProvince.SelectedIndexChanged
        BIND_Province()
        RetrieveMunicipality()
    End Sub

    Protected Sub drpMunicipality_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpMunicipality.SelectedIndexChanged
        BIND_Municipality()
        RetrieveBarangay()
    End Sub

    Protected Sub drpBarangay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpBarangay.SelectedIndexChanged
        BIND_Barangay()
        upPnlCAddress.Update()
    End Sub


End Class