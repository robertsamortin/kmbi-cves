﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="arasSite.Master" CodeBehind="poEvaluation.aspx.vb" Inherits="CenterVisitEvaluation.poEvaluation" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../assets/js/jquery-ui.js" type="text/javascript"></script>
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function openMessageBox() {
            $('#modMessageBox').modal({ backdrop: 'static', keyboard: false })
        }
        function openChangeAddress() {
            $('#modChangeAddress').modal({ backdrop: 'static', keyboard: false })
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upPnlEval" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <nav id="myNav" class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    <b>PROGRAM OFFICERS</b>
                                </div>                              
                            </div>                                                     
                        </div>                        
                    </nav>
                    <div class="panel-body">
                        <div class="form-group">
	                        <div class="row">
        	                    <div class="col-sm-2">
                	                <label>Evaluation Date:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                	                    <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" ClientIDMode="Static" autofocus required></asp:TextBox>
                                    </div>
                                </div>
       	                    </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Branch:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>
                                        <asp:DropDownList ID="cbobranch" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Program Officer:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <asp:TextBox ID="txtProgramOfficer" runat="server" CssClass="form-control" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Unit Code:</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-road"></i></span>
                                        <asp:DropDownList ID="cboUnit" runat="server" CssClass="form-control" AutoPostBack="True" required>
                                            
                                        </asp:DropDownList>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <%--PROGRAM OFFICER--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno1" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            <small style="font-size:12px; font-weight:700"><em>Information below served as an evaluation of the Program Officers accountability, "Yes" indicate compliance, however "No" means un-observance or non compliance.</em></small> 
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Meet the minimum required number of clients.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q1_rd1" runat="server" GroupName="q1" Text="Yes"  AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q1_rd2" runat="server" GroupName="q1" Text="No"    AutoPostBack="true"/>
                                                </label>
                                                <asp:TextBox ID="txtq1" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Meet the required number of centers.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q2_rd1" runat="server" GroupName="q2" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q2_rd2" runat="server" GroupName="q2" Text="No"   AutoPostBack="true" />
                                                </label>
                                                <asp:TextBox ID="txtq2" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>CBU withdrawal release on or before the center renewal.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q3_rd1" runat="server" GroupName="q3" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q3_rd2" runat="server" GroupName="q3" Text="No"   AutoPostBack="true" />
                                                </label>
                                                <asp:TextBox ID="txtq3" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Efficient and ontime schedule of loan disbursement.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q4_rd1" runat="server" GroupName="q4" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q4_rd2" runat="server" GroupName="q4" Text="No"   AutoPostBack="true" />
                                                </label>
                                                <asp:TextBox ID="txtq4" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Collection performance is with in the allowable PAR rate of 2%.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q5_rd1" runat="server" GroupName="q5" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q5_rd2" runat="server" GroupName="q5" Text="No"    AutoPostBack="true"/>
                                                </label>
                                                <asp:TextBox ID="txtq5" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Meet the minimum required attendance rate of 80%.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q6_rd1" runat="server" GroupName="q6" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q6_rd2" runat="server" GroupName="q6" Text="No"   AutoPostBack="true" />
                                                </label>
                                                <asp:TextBox ID="txtq6" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Make regular follow up  of all  delinquent clients as evidence by collection, logbook and official receipt issued. </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q7_rd1" runat="server" GroupName="q7" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q7_rd2" runat="server" GroupName="q7" Text="No"   AutoPostBack="true" />
                                                </label>
                                                <asp:TextBox ID="txtq7" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Keep or maintain a copy of PAR inventory with reason of delinquency </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q8_rd1" runat="server" GroupName="q8" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q8_rd2" runat="server" GroupName="q8" Text="No"    AutoPostBack="true"/>
                                                </label>
                                                <asp:TextBox ID="txtq8" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>No advance payment, all collection is done during center meeting.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q9_rd1" runat="server" GroupName="q9" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q9_rd2" runat="server" GroupName="q9" Text="No"    AutoPostBack="true"/>
                                                </label>
                                                <asp:TextBox ID="txtq9" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Implement change of center leadership as required in the policy atleast once a year.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q10_rd1" runat="server" GroupName="q10" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q10_rd2" runat="server" GroupName="q10" Text="No"    AutoPostBack="true"/>
                                                </label>
                                                <asp:TextBox ID="txtq10" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Prepare visual aid for center meeting.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q11_rd1" runat="server" GroupName="q11" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q11_rd2" runat="server" GroupName="q11" Text="No"    AutoPostBack="true"/>
                                                </label>
                                                <asp:TextBox ID="txtq11" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Conduct center leaders meeting.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q12_rd1" runat="server" GroupName="q12" Text="Yes"  AutoPostBack="true"  />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q12_rd2" runat="server" GroupName="q12" Text="No"   AutoPostBack="true" />
                                                </label>
                                                <asp:TextBox ID="txtq12" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Resolve all center conflict</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="radio">
                                                <label class="btn btn-success">
                                                    <asp:RadioButton ID="q13_rd1" runat="server" GroupName="q13" Text="Yes"   AutoPostBack="true" />
                                                </label>
                                                <label class="btn btn-danger">
                                                    <asp:RadioButton ID="q13_rd2" runat="server" GroupName="q13" Text="No"    AutoPostBack="true"/>
                                                </label>
                                                <asp:TextBox ID="txtq13" runat="server" text="0" style="display:none;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                   

                    

                    <%--COMMENTS--%>
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Nav1" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            Remarks
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group col-sm-8">
                                    <asp:TextBox ID="txtComments" TextMode="MultiLine" style="resize:none" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--RATINGS EQUIVALENT--%>
                     <div class="container-fluid">
                        <div class="panel panel-default">
                            <nav id="Qno6" class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">
                                            <span class="glyphicon glyphicon-list-alt"></span>
                                            EQUIVALENT RATING
                                        </div>
                                    </div>
                                </div>
                            </nav>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <asp:Button ID="btnViewScore" runat="server" Text="View Score" CssClass ="btn btn-success" Font-Size="Large" />
                                          
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                           <button class="btn btn-primary" type="button">
                                              Score    <span class="badge"><h5><asp:Label ID="lblTotalScore" runat="server" ></asp:Label></h5></span>
                                            </button>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                           <button class="btn btn-primary" type="button">
                                              Rating   <span class="badge"><h5><asp:Label ID="lblRating" runat="server" ></asp:Label></h5></span>
                                            </button>
                                        </div>
                                        <div class="col-sm-3">
                                              <h3><asp:Label ID="lblEquivalent" runat="server" cssclass="label label-danger"></asp:Label></h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="panel-footer clearfix ">
                        <div class="btn-group btn-group-justified" role="group" >
                            <div class="btn-group" role="group">
                                <asp:Button ID="cmdsave" runat="server" Text="Submit" CssClass="btn btn-success" />
                                <asp:ConfirmButtonExtender ID="confirmExtender" runat="server" ConfirmText="Are you sure you want to submit?" TargetControlID="cmdSave" >
                                </asp:ConfirmButtonExtender>
                            </div>
                            <div class="btn-group" role="group">
                                <asp:Button ID="cmdReport" runat="server" Text="Generate Report" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <CR:CrystalReportViewer ID="CRV" runat="server" AutoDataBind="true" Visible="false" />
                    </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade"  tabindex="-1" id="modMessageBox" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">SYSTEM NOTIFICATION</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="pnlUpdateFormMessage" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modChangeAddress" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div  class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">CHANGE CENTER ADDRESS</h4>
                </div>
                <div  class="modal-body">
                    <asp:UpdatePanel ID="upPnlCAddress" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
                        <ContentTemplate>
                            <div class="form-group">
                                <label>Province</label>
                                <asp:DropDownList ID="drpProvince" runat="server" CssClass="form-control" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnProvince" runat="server" />
                            </div>
                            <div class="form-group">
                                <label>Municipality</label>
                                <asp:DropDownList ID="drpMunicipality" runat="server" CssClass="form-control" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnMunicipality" runat="server" />
                            </div>
                            <div class="form-group">
                                <label>Barangay</label>
                                <asp:DropDownList ID="drpBarangay" runat="server" CssClass="form-control" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnBarangay" runat="server" />
                             </div>  
                        </ContentTemplate> 
                    </asp:UpdatePanel> 
                    
                </div>
                <div  class="modal-footer">
                    <asp:Button ID="cmdOkCAddress" runat="server" Text="Save" class = "btn btn-success" /> 
                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" 
                                    TargetControlID="cmdOkCAddress" ConfirmText= "Are you sure?"/>
                </div>
            </div>
        </div>
    </div>  

<asp:UpdateProgress ID="pnlUpdateProgress" runat="server">
        <ProgressTemplate>
            <div style="background-color: Gray; 
                filter:alpha(opacity=60); 
                opacity:0.60; 
                width: 100%; 
                top: 0; 
                left: 0; 
                position: fixed; 
                height: 100%; 
                z-index: 100001">
            </div>
            <div style="filter: alpha(opacity=100);
                opacity: 1;
                position:  fixed;
                top: 50%;
                left: 50%;
                margin: -50px 0px 0px -25px;
                z-index: 100001">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

<!--SESSIONS-->
    <div> 
        <asp:Label ID="lblfullname" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblposition" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchcode" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblbranchdesc" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblcurrentuser" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblaccesslevel" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblsearch" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblareacode" runat="server" Visible="False"></asp:Label>
    </div>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            jQuery(function ($) {
                $("#txtdate").datepicker({ maxDate: '0' });
            });

        });

        jQuery(function ($) {
            $("#txtdate").datepicker({ maxDate: '0' });
        });

    </script>
</asp:Content>
