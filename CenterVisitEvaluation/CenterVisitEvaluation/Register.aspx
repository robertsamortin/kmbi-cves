﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Register.aspx.vb" Inherits="CenterVisitEvaluation.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
Last Name:  <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
First Name:  <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
Middle Name:  <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
Email Address:  <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
Position:  
    <asp:DropDownList ID="DropDownList1" runat="server">
        <asp:ListItem>Deputy Executive Director</asp:ListItem>
        <asp:ListItem>Regional Director</asp:ListItem>
        <asp:ListItem>Area Manager</asp:ListItem>
        <asp:ListItem>Branch Manager</asp:ListItem>
        <asp:ListItem>Program Unit Head</asp:ListItem>
        <asp:ListItem>Program Officer</asp:ListItem>
    </asp:DropDownList>
User Name:  <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
Password:  <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
</asp:Content>
