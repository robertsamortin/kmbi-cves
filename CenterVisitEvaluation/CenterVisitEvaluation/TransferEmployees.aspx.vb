﻿Imports System.Data.SqlClient
Public Class TransferEmployees
    Inherits System.Web.UI.Page

    Dim st As String = ConfigurationManager.ConnectionStrings("con").ToString
    Dim con As New SqlConnection(st)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.IsAuthenticated And Session("currentuser") <> "" Then
            Me.lblfullname.Text = Session("fullname")
            Me.lblposition.Text = Session("position")
            Me.lblbranchcode.Text = Session("branchcode")
            Me.lblbranchdesc.Text = Session("branchdesc")
            Me.lblcurrentuser.Text = Session("currentuser")
            Me.lblaccesslevel.Text = Session("accesslevel")
            Me.lblareacode.Text = Session("areacode")

            If Not IsPostBack Then
                BIND_Branch()

            End If

        Else
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Private Sub BIND_Branch()
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchcode As String = Trim(Me.lblbranchcode.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "Select * from branch order by branch_desc"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.cboFrombranch.Items.Clear()
            Me.cboToBranch.Items.Clear()
            Me.cboFrombranch.Items.Add("Select Branch")
            Me.cboToBranch.Items.Add("Select Branch")
            While dr.Read
                Me.cboFrombranch.Items.Add(Trim(dr("branch_desc").ToString))
                Me.cboToBranch.Items.Add(Trim(dr("branch_desc").ToString))
            End While

            upPnlTransfer.Update()
            dr.Close()
            cmd.Dispose()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub cboFrombranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFrombranch.SelectedIndexChanged

        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchdesc As String = Trim(Me.cboFrombranch.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "Select a.branch_code, b.area_code, c.region_code from branch a inner join area b on a.area_code = b.area_code inner join region c on b.region_code = c.region_code where a.branch_desc  = '" & branchdesc & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            Me.txtEmployeeCode.Text = ""
            If dr.Read Then
                Me.txtFromBranchCode.Text = dr("branch_code").ToString
                Me.txtFromAreaCode.Text = dr("area_code").ToString
                Me.txtFromRegCode.Text = dr("region_code").ToString
            End If

            dr.Close()
            cmd.Dispose()

            Get_Employee()
            upPnlTransfer.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub cboToBranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboToBranch.SelectedIndexChanged
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchdesc As String = Trim(Me.cboToBranch.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "Select a.branch_code, b.area_code, c.region_code from branch a inner join area b on a.area_code = b.area_code inner join region c on b.region_code = c.region_code where a.branch_desc  = '" & branchdesc & "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            If dr.Read Then
                Me.txtToBranchCode.Text = dr("branch_code").ToString
                Me.txtToAreaCode.Text = dr("area_code").ToString
                Me.txtToRegCode.Text = dr("region_code").ToString
            End If

            dr.Close()
            cmd.Dispose()

            upPnlTransfer.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub Get_Employee()
        Try
            Dim branchcode As String = Trim(Me.txtFromBranchCode.Text)

            Dim sqlQuery As String = "Select * from employee where branch_code  = '" & branchcode & "' order by last_name, first_name, middle_name"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()

            Me.cboEmployee.Items.Clear()
            Me.cboEmployee.Items.Add("Select Employee")
            While dr.Read
                Me.cboEmployee.Items.Add(Trim(dr("last_name").ToString) + ", " + Trim(dr("first_name").ToString) + " " + Trim(dr("middle_name").ToString))
            End While

            dr.Close()
            cmd.Dispose()

            upPnlTransfer.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If con.State = 1 Then con.Close()
            con.Open()

            Dim branchdesc As String = Trim(Me.cboFrombranch.Text)
            Dim areacode As String = Trim(Me.lblareacode.Text)
            Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
            Dim pa As String = Trim(Me.lblcurrentuser.Text)
            Dim pus As String = Trim(Me.lblcurrentuser.Text)

            Dim sqlQuery As String = "Select emp_code from employee where last_name + ', ' + first_name + ' ' + middle_name = '" + Trim(Me.cboEmployee.Text) + "'"
            Dim cmd As New SqlCommand(sqlQuery, con)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            If dr.Read Then
                Me.txtEmployeeCode.Text = dr("emp_code").ToString
            End If

            dr.Close()
            cmd.Dispose()

            upPnlTransfer.Update()
        Catch ex As Exception
            Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub cboPosition_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPosition.SelectedIndexChanged
        If cboPosition.Text = "PUH" Then
            txtPosCode.Text = "PUS"
        ElseIf cboPosition.Text = "RD" Then
            txtPosCode.Text = "RM"
        Else
            txtPosCode.Text = cboPosition.Text
        End If
    End Sub

    

    'Private Sub BIND_AreaManagers()
    '    Try

    '        Dim branchcode As String = Trim(Me.lblbranchcode.Text)
    '        Dim areacode As String = Trim(Me.lblareacode.Text)
    '        Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
    '        Dim emp As String = Trim(Me.lblcurrentuser.Text)

    '        Dim dv As New DataView(TryCast(GetDocument("OPS_list_employee '" & lblbranchcode.Text & "', '" & lblareacode.Text & "', 'AM','" & lblaccesslevel.Text & "', '" & lblcurrentuser.Text & "'").Tables(0), DataTable))

    '        dv.Sort = "name ASC"
    '        Session("AM") = dv
    '        grdAreaManagers.DataSource = dv
    '        grdAreaManagers.DataBind()

    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    End Try
    'End Sub

    'Private Sub BIND_BranchManagers()
    '    Try

    '        Dim branchcode As String = Trim(Me.lblbranchcode.Text)
    '        Dim areacode As String = Trim(Me.lblareacode.Text)
    '        Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
    '        Dim emp As String = Trim(Me.lblcurrentuser.Text)

    '        Dim dv As New DataView(TryCast(GetDocument("OPS_list_employee '" & lblbranchcode.Text & "', '" & lblareacode.Text & "', 'BM','" & lblaccesslevel.Text & "', '" & lblcurrentuser.Text & "'").Tables(0), DataTable))

    '        dv.Sort = "name ASC"
    '        Session("BM") = dv
    '        grdBranchManagers.DataSource = dv
    '        grdBranchManagers.DataBind()

    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    End Try
    'End Sub

    'Private Sub BIND_PUH()
    '    Try

    '        Dim branchcode As String = Trim(Me.lblbranchcode.Text)
    '        Dim areacode As String = Trim(Me.lblareacode.Text)
    '        Dim accesslevel As String = CInt(Me.lblaccesslevel.Text)
    '        Dim emp As String = Trim(Me.lblcurrentuser.Text)

    '        Dim dv As New DataView(TryCast(GetDocument("OPS_list_employee '" & lblbranchcode.Text & "', '" & lblareacode.Text & "', 'PUS','" & lblaccesslevel.Text & "', '" & lblcurrentuser.Text & "'").Tables(0), DataTable))

    '        dv.Sort = "branch_desc ASC, name ASC"
    '        Session("PUH") = dv
    '        grdPUH.DataSource = dv
    '        grdPUH.DataBind()
    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    End Try
    'End Sub

    'Protected Sub grdRegionalDirectors_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRegionalDirectors.PageIndexChanging
    '    grdRegionalDirectors.PageIndex = e.NewPageIndex
    '    grdRegionalDirectors.DataSource = Session("RM")
    '    grdRegionalDirectors.DataBind()
    'End Sub

    'Protected Sub grdRegionalDirectors_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdRegionalDirectors.RowCommand
    '    Dim conductedby As String = e.CommandArgument
    '    Try
    '        If e.CommandName = "Employee" Then
    '            Session("employeeid") = conductedby
    '            UpdatePanel1.Update()
    '            ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


    '        End If
    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    End Try
    'End Sub

    'Protected Sub grdRegionalDirectors_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdRegionalDirectors.Sorting
    '    Dim sortingDirection As String = String.Empty
    '    If direction = SortDirection.Ascending Then
    '        direction = SortDirection.Descending
    '        sortingDirection = "Desc"
    '    Else
    '        direction = SortDirection.Ascending
    '        sortingDirection = "Asc"
    '    End If
    '    grdRegionalDirectors.SetPageIndex(0)

    '    Dim dv As DataView = Session("RM")
    '    dv.Sort = e.SortExpression + " " + sortingDirection
    '    Session("RM") = dv
    '    grdRegionalDirectors.DataSource = dv
    '    grdRegionalDirectors.DataBind()
    'End Sub

    'Protected Sub grdAreaManagers_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAreaManagers.PageIndexChanging
    '    grdAreaManagers.PageIndex = e.NewPageIndex
    '    grdAreaManagers.DataSource = Session("AM")
    '    grdAreaManagers.DataBind()
    'End Sub

    'Protected Sub grdAreaManagers_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAreaManagers.RowCommand
    '    Dim conductedby As String = e.CommandArgument
    '    Try
    '        If e.CommandName = "Employee" Then
    '            Session("employeeid") = conductedby
    '            UpdatePanel2.Update()
    '            ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


    '        End If
    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    End Try
    'End Sub

    'Protected Sub grdAreaManagers_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAreaManagers.Sorting
    '    Dim sortingDirection As String = String.Empty
    '    If direction = SortDirection.Ascending Then
    '        direction = SortDirection.Descending
    '        sortingDirection = "Desc"
    '    Else
    '        direction = SortDirection.Ascending
    '        sortingDirection = "Asc"
    '    End If
    '    grdAreaManagers.SetPageIndex(0)

    '    Dim dv As DataView = Session("AM")
    '    dv.Sort = e.SortExpression + " " + sortingDirection
    '    Session("AM") = dv
    '    grdAreaManagers.DataSource = dv
    '    grdAreaManagers.DataBind()
    'End Sub

    'Protected Sub grdBranchManagers_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBranchManagers.PageIndexChanging
    '    grdBranchManagers.PageIndex = e.NewPageIndex
    '    grdBranchManagers.DataSource = Session("BM")
    '    grdBranchManagers.DataBind()
    'End Sub

    'Protected Sub grdBranchManagers_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdBranchManagers.RowCommand
    '    Dim conductedby As String = e.CommandArgument
    '    Try
    '        If e.CommandName = "Employee" Then
    '            Session("employeeid") = conductedby
    '            UpdatePanel3.Update()
    '            ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


    '        End If
    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    End Try
    'End Sub

    'Protected Sub grdBranchManagers_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdBranchManagers.Sorting
    '    Dim sortingDirection As String = String.Empty
    '    If direction = SortDirection.Ascending Then
    '        direction = SortDirection.Descending
    '        sortingDirection = "Desc"
    '    Else
    '        direction = SortDirection.Ascending
    '        sortingDirection = "Asc"
    '    End If
    '    grdBranchManagers.SetPageIndex(0)

    '    Dim dv As DataView = Session("BM")
    '    dv.Sort = e.SortExpression + " " + sortingDirection
    '    Session("BM") = dv
    '    grdBranchManagers.DataSource = dv
    '    grdBranchManagers.DataBind()
    'End Sub

    'Protected Sub grdPUH_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdPUH.PageIndexChanging
    '    grdPUH.PageIndex = e.NewPageIndex
    '    grdPUH.DataSource = Session("PUH")
    '    grdPUH.DataBind()
    'End Sub

    'Protected Sub grdPUH_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPUH.RowCommand
    '    Dim conductedby As String = e.CommandArgument
    '    Try
    '        If e.CommandName = "Employee" Then
    '            Session("employeeid") = conductedby
    '            UpdatePanel4.Update()
    '            ScriptManager.RegisterStartupScript(Me, GetType(String), "New_Window", "window.open( '/CVES/Profile.aspx?id=" + conductedby + "', target='_blank' );", True)


    '        End If
    '    Catch ex As Exception
    '        Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
    '    End Try
    'End Sub

    'Protected Sub grdPUH_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPUH.Sorting
    '    Dim sortingDirection As String = String.Empty
    '    If direction = SortDirection.Ascending Then
    '        direction = SortDirection.Descending
    '        sortingDirection = "Desc"
    '    Else
    '        direction = SortDirection.Ascending
    '        sortingDirection = "Asc"
    '    End If
    '    grdPUH.SetPageIndex(0)

    '    Dim dv As DataView = Session("PUH")
    '    dv.Sort = e.SortExpression + " " + sortingDirection
    '    Session("PUH") = dv
    '    grdPUH.DataSource = dv
    '    grdPUH.DataBind()

    'End Sub

    ''@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SORTING AND PAGING @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ''@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    'Public Function GetDocument(ByVal query As String) As DataSet

    '    If con.State = ConnectionState.Open Or con.State = 16 Or con.State = 2 Or con.State = ConnectionState.Fetching Or con.State = ConnectionState.Executing Then con.Close()
    '    Dim cmd As New SqlCommand(query, con)
    '    con.Open()
    '    Dim da As New SqlDataAdapter(cmd)
    '    Dim ds As New DataSet()
    '    da.Fill(ds)
    '    con.Close()
    '    Return ds
    'End Function

    'Public Property direction() As SortDirection
    '    Get
    '        If ViewState("directionState") Is Nothing Then
    '            ViewState("directionState") = SortDirection.Ascending
    '        End If
    '        Return DirectCast(ViewState("directionState"), SortDirection)
    '    End Get
    '    Set(value As SortDirection)
    '        ViewState("directionState") = value
    '    End Set
    'End Property

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtFromBranchCode.Text = "" Or txtFromAreaCode.Text = "" Or txtFromRegCode.Text = "" Then
            Prompt("Please select From Branch.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboFrombranch.Focus()
        ElseIf txtEmployeeCode.Text = "" Then
            Prompt("Please select Employee.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboEmployee.Focus()
        ElseIf txtToBranchCode.Text = "" Or txtToAreaCode.Text = "" Or txtToRegCode.Text = "" Then
            Prompt("Please select To Branch.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboToBranch.Focus()
        ElseIf txtPosCode.Text = "" Then
            Prompt("Please select Position.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboToBranch.Focus()
        ElseIf txtPosCode.Text = "PUS" And cboUnit.Text = "Select Unit" Then
            Prompt("Please select Unit.", Me.lblMessage, Me.pnlUpdateFormMessage)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            cboToBranch.Focus()
        Else


            Try
                Dim empcode As String = Trim(Me.txtEmployeeCode.Text)
                Dim regcode As String = Trim(Me.txtToRegCode.Text)
                Dim areacode As String = Trim(Me.txtToAreaCode.Text)
                Dim bracode As String = Trim(Me.txtToBranchCode.Text)
                Dim unitcode As String
                If cboUnit.Text = "Select Unit" Then
                    unitcode = "A"
                Else
                    unitcode = Trim(Me.cboUnit.Text)
                End If
                Dim poscode As String = Trim(Me.txtPosCode.Text)

                If con.State = 1 Then con.Close()
                con.Open()
                Dim sqlQuery As String = ""


                sqlQuery = "sp_updateEmployeeUsers " &
                                "'" & empcode & "', " &
                                "'" & regcode & "', " &
                                "'" & areacode & "', " &
                                "'" & bracode & "', " &
                                "'" & unitcode & "', " &
                                "'" & poscode & "'"

                Dim cmd As New SqlCommand(sqlQuery, con)
                cmd.ExecuteNonQuery()
                Prompt("Successfully Submitted", Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
                upPnlTransfer.Update()

            Catch ex As Exception
                Prompt("ERROR: " & ex.Message, Me.lblMessage, Me.pnlUpdateFormMessage)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "openMessageBox();", True)
            Finally
                con.Close()
            End Try
        End If
    End Sub
End Class